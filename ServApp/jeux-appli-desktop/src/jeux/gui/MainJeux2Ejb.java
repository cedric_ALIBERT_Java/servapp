package jeux.gui;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import jeux.commun.service.IContextService;

import jeux.emb.service.clientejb.ClientEjbMock;
import jeux.emb.service.clientejb.ClientEjbStandard;
import jeux.javafx.model.IContextModel;
import jeux.javafx.view.ManagerGuiAbstract;


public class MainJeux2Ejb { 

	public static void main(String[] args) {
	
		try{
			InputStream in = MainJeux2Ejb.class.getResourceAsStream("/META-INF/logging.properties");
			LogManager.getLogManager().readConfiguration(in);
			in.close();
		}catch(IOException e){
			e.printStackTrace();
		}
		
		final Logger logger = Logger.getLogger( MainJeux2Ejb.class.getName() );
		
		try  {
			
			IContextService contextService = new ClientEjbStandard();
			IContextModel contextModel = 
//					new jeux.gui.model.autonome.ContextModel();
					new jeux.javafx.model.standard.ContextModel( contextService );
			ManagerGuiAbstract managerGui = 
					new jeux.javafx.view.ManagerGuiClassic( contextModel );
			
			
			logger.log(Level.CONFIG,"Initialisation effectuée");
			//managerGui = null;
			managerGui.launch();
			
		} catch (Exception e) {
			//e.printStackTrace();
			logger.log(Level.SEVERE,"Echec démarrage",e);			
			
			JOptionPane.showMessageDialog(null, "Impossible de démarrer l'application.", "", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
	}
 
}
