package jeux.javafx.view;

import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.input.InputEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Duration;


public abstract class ManagerGuiAbstract {
	
	
	// Constantes
	private static final KeyCodeCombination KCC_ENTER = new KeyCodeCombination( KeyCode.ENTER );
	private static final KeyCodeCombination KCC_ESCAPE = new KeyCodeCombination( KeyCode.ESCAPE );
	private static final Logger logger = Logger.getLogger( ManagerGuiAbstract.class.getName() );
	
	// Champs
	
	private Stage			stage;
	private Scene			scene;
	private EnumView		viewPrec;


	// Initialisations
	
	public void launch() {
		ApplicationJavaFX.launch(this);
	}
	
	
	private void start( Stage stage ) throws Exception {

		// Configure le stage
		scene = new Scene( new Pane() );
		stage.setScene( scene );
		stage.setMinWidth(280);
		stage.setMinHeight(330);
		stage.setResizable( false );
		stage.setTitle("Jeux");
        this.stage = stage;

		// Choisit la vue à afficher
        showView( EnumView.Menu );
		
		// Affiche le stage
		stage.show();
		
	}

	
	// Actions
	
	public abstract <T> T getModel( Class<T> type );

	
	public void showView( EnumView view ) {
		
		try {
			
			Pane pane = view.getPane();
		
			if( pane == null ) {
				
				// Charge le panneau et l'enregistre dans la vue
				FXMLLoader loader = new FXMLLoader(getClass().getResource( view.getPathn() ));
				pane = loader.load(); 
				view.setPane( pane );

				// Injecte la référence du managerGui
				((IController) loader.getController()).setManagerGui(this);;

			}
			// Affiche la vue
			scene.setRoot(pane);	
			
			// Gère les boutons par défaut
			if ( viewPrec != null ) {
				if ( viewPrec.getRunnableEnter() == null ) {
					viewPrec.setRunnableEnter(scene.getAccelerators().get( KCC_ENTER ) );
				}
				if ( viewPrec.getRunnableEscape() == null ) {
					viewPrec.setRunnableCancel( scene.getAccelerators().get( KCC_ESCAPE ) );
				}
			} 
			if ( view.getRunnableEnter() != null ) {
				scene.getAccelerators().put( KCC_ENTER, view.getRunnableEnter() );
			}
			if ( view.getRunnableEscape() != null ) {
				scene.getAccelerators().put( KCC_ESCAPE, view.getRunnableEscape() );
			}
			viewPrec = view;
			
		} catch (Exception e) {
			afficherErreur( e );
		} 
	}
	
	
	public void close() {
		stage.close();
	}
	
	
	// Méthodes utilitaires
	
	public void execTask( Runnable runnable ) {

		final EventHandler<InputEvent> inputEventConsumer = (event) ->	event.consume() ;  
		stage.addEventFilter(InputEvent.ANY, inputEventConsumer);  
		stage.getScene().setCursor( Cursor.WAIT );  

		Timeline timeline = new Timeline(  new KeyFrame(
	        Duration.ONE,
            new EventHandler<ActionEvent>() {  
	            @Override  
	            public void handle(ActionEvent event) {
					runnable.run();
		            stage.removeEventFilter(InputEvent.ANY, inputEventConsumer);  
		            stage.getScene().setCursor(Cursor.DEFAULT);  
	              }  
	         }));  
		timeline.play();  

	}

	public void afficherErreur( Throwable exception ) {
		afficherErreur( null, exception );
	}

	public void afficherErreur( String message ) {
		afficherErreur( message, null );
	}

	public void afficherErreur( String message, Throwable exception ) {
		if ( exception != null ) {
			//exception.printStackTrace();
			logger.log(Level.SEVERE, exception.getMessage());
			logger.log(Level.FINE, exception.getMessage(),exception);
		}
		if ( message == null ) {
			message = "Echec du traitement demandé.";
		}
		final Alert alert = new Alert(Alert.AlertType.ERROR);  
		alert.initOwner(stage); 
		alert.setHeaderText( message ); 
		alert.showAndWait(); 
	}
	
	
	// Classe auxiliaire
	
	public static class ApplicationJavaFX extends Application {
		private static ManagerGuiAbstract	managerGui;
		public static void launch( ManagerGuiAbstract managerGui ) {
			ApplicationJavaFX.managerGui = managerGui;
			launch();
		}
		@Override
		public void start(Stage stage) throws Exception {
			managerGui.start(stage);
		}
	}	
	
}
