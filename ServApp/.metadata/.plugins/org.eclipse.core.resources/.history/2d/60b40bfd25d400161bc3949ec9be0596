package jeux.ejb.service.mock;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import com.sun.xml.ws.developer.Stateful;

import jeux.commun.dto.DtoPenduConfig;
import jeux.commun.dto.DtoPenduReponse;
import jeux.commun.dto.DtoPenduReponse.EnumResponse;
import jeux.commun.service.IServicePendu;
import jeux.commun.util.ExceptionAppli;
import jeux.ejb.dom.DataPendu;

@Stateful( mappedName="ejb/mock/ServicePendu" )
@Remote( IServicePendu.class )
public class ServicePendu implements IServicePendu {
	
	
	// Champs
	
	private final Map<String, DataPendu> mapData = new HashMap<>();

	private String[]		dictionnaire;
	
	private final Random 	random = new Random();

	
	
	
	
	// Actions
	
	@Override
	public DtoPenduConfig retrouverConfig( String idJoueur ) throws ExceptionAppli {
		return mapConfig( mapData.get( idJoueur ) );
	}

	@Override
	public void enregistrerConfig( String idJoueur, DtoPenduConfig dtoConfig ) throws ExceptionAppli {
		DataPendu dataPendu = mapData.get( idJoueur );
		dataPendu.setFlagModeFacile( dtoConfig.isFlagModeFacile() );
		dataPendu.setNbErreursMaxi( dtoConfig.getNbErreursMaxi() );
	}
	
	@Override
	public DtoPenduReponse retrouverPartie( String idJoueur ) throws ExceptionAppli {
		return mapReponse( mapData.get( idJoueur ) );
	}

	
	@Override
	public DtoPenduReponse nouvellePartie( String idJoueur ) throws ExceptionAppli {

		DataPendu dataPendu = mapData.get( idJoueur );
		if ( dataPendu == null ) {
			dataPendu = initDataNouveauJoueur( idJoueur );
		}
		dataPendu.setNbErreursRestantes( dataPendu.getNbErreursMaxi() );
		dataPendu.setLettresJouees("");
		
		int rang = random.nextInt( dictionnaire.length );
		String motMystere = dictionnaire[rang];
		
		StringBuilder sbResultat = new StringBuilder();
		sbResultat.append('_');
		for ( int i=1; i < motMystere.length() ; i++ ) {
			sbResultat.append(' ').append('_');
		}
		if ( dataPendu.isFlagModeFacile() ) {
			sbResultat.setCharAt( 0, motMystere.charAt(0) );
			sbResultat.setCharAt( sbResultat.length() - 1, motMystere.charAt( motMystere.length() - 1 ) );
		}

		dataPendu.setMotMystere( motMystere );
		dataPendu.setResultat( sbResultat.toString()  );
		return mapReponse(dataPendu);
	}
	
	@Override
	public DtoPenduReponse jouer( String idJoueur, char lettre ) throws ExceptionAppli {

		DataPendu dataPendu = mapData.get( idJoueur );
		String motMystere = dataPendu.getMotMystere();
		StringBuilder sbResultat = new StringBuilder( dataPendu.getResultat() );

		boolean flagErreur = true;
		int pos = -1;
		while (true) {
			pos = motMystere.indexOf( lettre, pos+1 );
			if ( pos < 0 ) 
				break;
			if ( sbResultat.charAt( pos*2 ) == '_' ) {
				sbResultat.setCharAt( pos*2, lettre );
				flagErreur = false;
			}
		}
		if ( flagErreur ) {
			dataPendu.setNbErreursRestantes( dataPendu.getNbErreursRestantes() - 1);
		} else {
			dataPendu.setResultat( sbResultat.toString() );
		}
		dataPendu.setLettresJouees( dataPendu.getLettresJouees() + lettre );
		return mapReponse(dataPendu);
	}

	@Override
	public String tricher( String idJoueur ) throws ExceptionAppli {
		DataPendu dataPendu = mapData.get( idJoueur );
		return dataPendu.getMotMystere();
	}

	
	// Methodes auxiliaires
	
	private DtoPenduConfig mapConfig( DataPendu dataPendu ) {
		if( dataPendu == null ) {
			return null;
		}
		DtoPenduConfig dtoConfig = new DtoPenduConfig();
		dtoConfig.setFlagModeFacile( dataPendu.isFlagModeFacile() );
		dtoConfig.setNbErreursMaxi( dataPendu.getNbErreursMaxi() );
		return dtoConfig;
	}
	
	private DtoPenduReponse mapReponse( DataPendu dataPendu ) {
		if( dataPendu == null ) {
			return null;
		}
		DtoPenduReponse dtoReponse = new DtoPenduReponse();
		if ( dataPendu.getResultat().indexOf('_') < 0 ) {
			dtoReponse.setReponse( EnumResponse.GAGNE );
			dtoReponse.setMotMystere( dataPendu.getMotMystere() );
		} else if ( dataPendu.getNbErreursRestantes() < 0 ) {
			dtoReponse.setReponse( EnumResponse.PERDU );
			dtoReponse.setMotMystere( dataPendu.getMotMystere() );
		} else {
			dtoReponse.setReponse( EnumResponse.EN_COURS );
		}
		dtoReponse.setResultat( dataPendu.getResultat() );
		dtoReponse.setNbErreursRestantes( dataPendu.getNbErreursRestantes() );
		dtoReponse.setLettresJouees( dataPendu.getLettresJouees() );
		return dtoReponse;
	}
	
	
	private DataPendu initDataNouveauJoueur( String idJoueur ) {
		DataPendu dataPendu = new DataPendu();
		dataPendu.setIdJoueur( idJoueur );
		dataPendu.setFlagModeFacile(false);
		dataPendu.setNbErreursMaxi( 7 );
		mapData.put( idJoueur, dataPendu );
		return dataPendu;
	}

	@PostConstruct
	private void initialiserDictionnaire() {
		
		dictionnaire = new String[] {
			"AEROPORT",
			"APPARTEMENT",
			"AUTOROUTE",
			"BIBLIOTHEQUE",
			"BOULANGERIE",
			"BOULEVARD",
			"CARREFOUR",
			"CATHEDRALE",
			"METROPOLE",
			"PREFECTURE",
			"PERIPHERIQUE",
			"PHARMACIE",
			"SUPERMARCHE",
			"TROLLEYBUS",
		};
	}

}
