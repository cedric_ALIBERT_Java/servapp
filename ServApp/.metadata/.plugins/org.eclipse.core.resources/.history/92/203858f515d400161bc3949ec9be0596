package jeux.emb.service.standard;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import jeux.commun.dto.DtoPenduConfig;
import jeux.commun.dto.DtoPenduReponse;
import jeux.commun.dto.DtoPenduReponse.EnumResponse;
import jeux.commun.service.IServicePendu;
import jeux.commun.util.ExceptionAnomalie;
import jeux.commun.util.ExceptionAppli;
import jeux.emb.dao.IDaoPendu;
import jeux.emb.dom.DataPendu;



public class ServicePendu implements IServicePendu {
	
	private static final Logger logger = Logger.getLogger( ServicePendu.class.getName() );
	// Champs

	private String[]		dictionnaire;
	private final Random 	random = new Random();
	
	private IDaoPendu		daoPendu;
	
	
	// Constructeur
	
	public ServicePendu() {
		initialiserDictionnaire();
	}
	
	
	// Injecteurs
	
	public void setDaoPendu(IDaoPendu daoPendu) {
		this.daoPendu = daoPendu;
	}
	
	
	// Actions
	
	@Override
	public DtoPenduConfig retrouverConfig( String idJoueur ) throws ExceptionAppli {
		try {
			return mapConfig( daoPendu.retrouver( idJoueur ) );
		} catch (RuntimeException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			throw new ExceptionAnomalie(e);
		}
	}

	@Override
	public void enregistrerConfig( String idJoueur, DtoPenduConfig dtoConfig ) throws ExceptionAppli {
		DataPendu dataPendu = daoPendu.retrouver( idJoueur );
		dataPendu.setFlagModeFacile( dtoConfig.isFlagModeFacile() );
		dataPendu.setNbErreursMaxi( dtoConfig.getNbErreursMaxi() );
		daoPendu.modifier( dataPendu );
	}
	
	@Override
	public DtoPenduReponse retrouverPartie( String idJoueur) throws ExceptionAppli {
		return mapReponse( daoPendu.retrouver( idJoueur ) );
	}

	
	@Override
	public DtoPenduReponse nouvellePartie( String idJoueur ) throws ExceptionAppli {

		DataPendu dataPendu = daoPendu.retrouver( idJoueur );
		if ( dataPendu == null ) {
			dataPendu = initDataNouveauJoueur( idJoueur );
		}
		dataPendu.setNbErreursRestantes( dataPendu.getNbErreursMaxi() );
		dataPendu.setLettresJouees("");
		
		int rang = random.nextInt( dictionnaire.length );
		String motMystere = dictionnaire[rang];
		
		StringBuilder sbResultat = new StringBuilder();
		sbResultat.append('_');
		for ( int i=1; i < motMystere.length() ; i++ ) {
			sbResultat.append(' ').append('_');
		}
		if ( dataPendu.isFlagModeFacile() ) {
			sbResultat.setCharAt( 0, motMystere.charAt(0) );
			sbResultat.setCharAt( sbResultat.length() - 1, motMystere.charAt( motMystere.length() - 1 ) );
		}

		dataPendu.setMotMystere( motMystere );
		dataPendu.setResultat( sbResultat.toString()  );
		daoPendu.modifier( dataPendu );
		return mapReponse(dataPendu);
	}

	
	@Override
	public DtoPenduReponse jouer( String idJoueur, char lettre ) throws ExceptionAppli {

		DataPendu dataPendu = daoPendu.retrouver( idJoueur );
		String motMystere = dataPendu.getMotMystere();
		StringBuilder sbResultat = new StringBuilder( dataPendu.getResultat() );

		boolean flagErreur = true;
		int pos = -1;
		while (true) {
			pos = motMystere.indexOf( lettre, pos+1 );
			if ( pos < 0 ) 
				break;
			if ( sbResultat.charAt( pos*2 ) == '_' ) {
				sbResultat.setCharAt( pos*2, lettre );
				flagErreur = false;
			}
		}
		if ( flagErreur ) {
			dataPendu.setNbErreursRestantes( dataPendu.getNbErreursRestantes() - 1);
		} else {
			dataPendu.setResultat( sbResultat.toString() );
		}
		dataPendu.setLettresJouees( dataPendu.getLettresJouees() + lettre );
		daoPendu.modifier( dataPendu );
		return mapReponse(dataPendu);
	}

	@Override
	public String tricher( String idJoueur ) throws ExceptionAppli {
		DataPendu dataPendu = daoPendu.retrouver( idJoueur );
		return dataPendu.getMotMystere();
	}

	
	// Methodes auxiliaires
	
	private DtoPenduConfig mapConfig( DataPendu dataPendu ) {
		if( dataPendu == null ) {
			return null;
		}
		DtoPenduConfig dtoConfig = new DtoPenduConfig();
		dtoConfig.setFlagModeFacile( dataPendu.isFlagModeFacile() );
		dtoConfig.setNbErreursMaxi( dataPendu.getNbErreursMaxi() );
		return dtoConfig;
	}
	
	private DtoPenduReponse mapReponse( DataPendu dataPendu ) {
		if( dataPendu == null ) {
			return null;
		}
		DtoPenduReponse dtoReponse = new DtoPenduReponse();
		if ( dataPendu.getResultat().indexOf('_') < 0 ) {
			dtoReponse.setReponse( EnumResponse.GAGNE );
			dtoReponse.setMotMystere( dataPendu.getMotMystere() );
		} else if ( dataPendu.getNbErreursRestantes() < 0 ) {
			dtoReponse.setReponse( EnumResponse.PERDU );
			dtoReponse.setMotMystere( dataPendu.getMotMystere() );
		} else {
			dtoReponse.setReponse( EnumResponse.EN_COURS );
		}
		dtoReponse.setResultat( dataPendu.getResultat() );
		dtoReponse.setNbErreursRestantes( dataPendu.getNbErreursRestantes() );
		dtoReponse.setLettresJouees( dataPendu.getLettresJouees() );
		return dtoReponse;
	}
	
	
	private DataPendu initDataNouveauJoueur( String idJoueur ) {
		DataPendu dataPendu = new DataPendu();
		dataPendu.setIdJoueur( idJoueur );
		dataPendu.setFlagModeFacile(false);
		dataPendu.setNbErreursMaxi( 7 );
		daoPendu.inserer(dataPendu);
		return dataPendu;
	}
	
	private void initialiserDictionnaire() {
		
		dictionnaire = new String[] {
			"AEROPORT",
			"APPARTEMENT",
			"AUTOROUTE",
			"BIBLIOTHEQUE",
			"BOULANGERIE",
			"BOULEVARD",
			"CARREFOUR",
			"CATHEDRALE",
			"METROPOLE",
			"PREFECTURE",
			"PERIPHERIQUE",
			"PHARMACIE",
			"SUPERMARCHE",
			"TROLLEYBUS",
		};
	}

}
