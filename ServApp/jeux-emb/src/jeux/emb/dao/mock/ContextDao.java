package jeux.emb.dao.mock;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import jeux.emb.dao.IContextDao;


public class ContextDao implements IContextDao  {
	
	
	// Champs
	
	@SuppressWarnings("rawtypes")
	private final Map<Class, Object>	mapBeans = new HashMap<>();
	
	
	// Actions
	
	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <T> T getDao( Class<T> type ) {
		
		// Recherche dans la map
		Object bean = mapBeans.get(type);
		
		// Si pas trouvé dans la map
		if ( bean == null ) {
			try {

				// Détermine le type à instancier
				Class<T> typeImpl;
				String nomImpl = type.getSimpleName().substring(1);
				String nomPackage = this.getClass().getPackage().getName();
				nomImpl = nomPackage + "." + nomImpl;
				typeImpl =  (Class<T>) Class.forName( nomImpl );
				Constructor<T> constructor = typeImpl.getConstructor(new Class[] {});

				// Instancie un objet et l'ajoute à la map
				bean = constructor.newInstance( new Object[] {} ) ;
				mapBeans.put( type, bean);

				// Injecte les dépendances
				for( Method method : typeImpl.getDeclaredMethods() ) {
					if ( method.getParameterCount() == 1 ) {
						Class typeProp = method.getParameterTypes()[0];
						if ( method.getName().startsWith( "setDao" ) ) {
							method.invoke( bean, getDao(typeProp) );
						}
					}
				}
				
				// Appel de la méthode init() si elle existe
				try {
					typeImpl.getMethod( "init" ).invoke( bean );
				} catch (Exception e) {
				}
						
			} catch ( RuntimeException e) {
				throw e;
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		return (T) bean;
	}

}
