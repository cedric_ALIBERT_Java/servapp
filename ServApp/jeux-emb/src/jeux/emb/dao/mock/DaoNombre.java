package jeux.emb.dao.mock;

import java.util.HashMap;
import java.util.Map;

import jeux.emb.dao.IDaoNombre;
import jeux.emb.dom.DataNombre;


public class DaoNombre implements IDaoNombre {

	
	// Champs
	
	private Map<String, DataNombre> mapData = new HashMap<>();

	
	
	// Constructeur
	
	public DaoNombre() {
		initialiserDonnees();
	}

	
	// Actions
	
	@Override
	public void inserer( DataNombre dataNombre ) {
		mapData.put( dataNombre.getIdJoueur(), dataNombre);
	}

	
	@Override
	public void modifier( DataNombre dataNombre ) {
		mapData.put( dataNombre.getIdJoueur(), dataNombre);
	}
	
	
	@Override
	public DataNombre retrouver( String idJoueur ) {
		return mapData.get(idJoueur);
	}
	
	
	// Méthodes auxiliaires

	private void initialiserDonnees() {
		
		DataNombre dataNombre;
		
		dataNombre = new DataNombre();
		dataNombre.setIdJoueur( "XX" );
		dataNombre.setValeurMaxi( 32 );
		dataNombre.setNbEssaisMaxi( 5 );
		dataNombre.setNbEssaisRestants( 3 );
		dataNombre.setNombreMystere( 27 );
		inserer(dataNombre);
		
	}

}
