package jeux.emb.dao.mock;

import java.util.HashMap;
import java.util.Map;

import jeux.emb.dao.IDaoPendu;
import jeux.emb.dom.DataPendu;


public class DaoPendu implements IDaoPendu {

	
	// Champs
	
	private Map<String, DataPendu> mapData = new HashMap<>();

	
	
	// Constructeur
	
	public DaoPendu() {
		initialiserDonnees();
	}

	
	// Actions
	
	@Override
	public void inserer( DataPendu dataPendu ) {
		mapData.put( dataPendu.getIdJoueur(), dataPendu);
	}

	
	@Override
	public void modifier( DataPendu dataPendu ) {
		mapData.put( dataPendu.getIdJoueur(), dataPendu);
	}
	
	
	@Override
	public DataPendu retrouver( String idJoueur ) {
		return mapData.get(idJoueur);
	}
	
	
	// Méthodes auxiliaires

	private void initialiserDonnees() {
		
		DataPendu dataPendu;
		
		dataPendu = new DataPendu();
		dataPendu.setIdJoueur( "XX" );
		dataPendu.setFlagModeFacile(false);
		dataPendu.setNbErreursMaxi( 7 );
		dataPendu.setNbErreursRestantes( 5 );
		dataPendu.setMotMystere( "AUTOROUTE" );
		dataPendu.setResultat( "_ _ T O _ O _ T _" );
		dataPendu.setLettresJouees( "OTMP" );
		inserer(dataPendu);
		
	}

}
