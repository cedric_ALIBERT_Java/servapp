package jeux.emb.dao;

import jeux.emb.dom.DataNombre;


public interface IDaoNombre {

	void		inserer( DataNombre dataNombre );

	void 		modifier( DataNombre dataNombre );

	DataNombre 	retrouver( String idJoueur );

}
