package jeux.emb.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import jeux.emb.dom.DataNombre;
import jeux.emb.dao.IDaoNombre;


public class DaoNombre implements IDaoNombre {

	
	// Champs

	private DataSource		dataSource;

	
	// Injecteurs
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	
	
	// Actions
	
	@Override
	public void inserer( DataNombre dataNombre ) {

		Connection			connection = null;
		PreparedStatement	stmt = null;
		
		try {
			try {
				connection = dataSource.getConnection();
				String sql = "INSERT INTO nombre ( ValeurMaxi, NbEssaisMaxi, NbEssaisRestants, NombreMystere, BorneInf, BorneSup, FlagPartieGagnee, IdJoueur ) VALUES ( ?, ?, ?, ?, ?, ?, ?, ? )";
				stmt = connection.prepareStatement( sql );
				stmt.setInt(	1, dataNombre.getValeurMaxi() );
				stmt.setInt(	2, dataNombre.getNbEssaisMaxi() );
				stmt.setInt(	3, dataNombre.getNbEssaisRestants() );
				stmt.setInt(	4, dataNombre.getNombreMystere() );
				stmt.setInt(	5, dataNombre.getBorneInf() );
				stmt.setInt(	6, dataNombre.getBorneSup() );
				stmt.setBoolean(7, dataNombre.isFlagPartieGagnee() );
				stmt.setString(	8, dataNombre.getIdJoueur() );
				stmt.executeUpdate();
				
			} finally {
				if (stmt != null) stmt.close();
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	
	@Override
	public void modifier( DataNombre dataNombre ) {

		Connection			connection = null;
		PreparedStatement	stmt = null;

		try {
			try {
				connection = dataSource.getConnection();
				String sql = "UPDATE nombre SET ValeurMaxi = ?, NbEssaisMaxi = ?, NbEssaisRestants = ?, NombreMystere = ?, BorneInf = ?, BorneSup = ?, FlagPartieGagnee = ? WHERE IdJoueur =  ?";
				stmt = connection.prepareStatement( sql );
				stmt.setInt(	1, dataNombre.getValeurMaxi() );
				stmt.setInt(	2, dataNombre.getNbEssaisMaxi() );
				stmt.setInt(	3, dataNombre.getNbEssaisRestants() );
				stmt.setInt(	4, dataNombre.getNombreMystere() );
				stmt.setInt(	5, dataNombre.getBorneInf() );
				stmt.setInt(	6, dataNombre.getBorneSup() );
				stmt.setBoolean(7, dataNombre.isFlagPartieGagnee() );
				stmt.setString(	8, dataNombre.getIdJoueur() );
				stmt.executeUpdate();
				
			} finally {
				if (stmt != null) stmt.close();
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
	}
	
	
	@Override
	public DataNombre retrouver( String idJoueur ) {

		DataNombre			dataNombre	= null;
		
		Connection			connection 	= null;
		PreparedStatement	stmt 		= null;
		ResultSet 			rs			= null;

		try {
			try {
				connection = dataSource.getConnection();
				String sql = "SELECT * FROM nombre WHERE IdJoueur = ?";
				stmt = connection.prepareStatement( sql );
				stmt.setString( 1, idJoueur );
				rs = stmt.executeQuery();

				if ( rs.next() ) {
					dataNombre = construireDataNombre(rs);
				}
			} finally {
				try {
					if (rs != null) rs.close();
				} finally {
					if (stmt != null) stmt.close();
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

			return dataNombre;
	}
	
	
	// Méthodes auxiliaires
	
	private DataNombre construireDataNombre( ResultSet rs ) throws SQLException {
		DataNombre dataNombre = new DataNombre();
		dataNombre.setIdJoueur( 		rs.getString( "IdJoueur"	) );
		dataNombre.setValeurMaxi( 		rs.getInt( "ValeurMaxi"	) );
		dataNombre.setNbEssaisMaxi(		rs.getInt( "NbEssaisMaxi"	) );
		dataNombre.setNbEssaisRestants(	rs.getInt( "NbEssaisRestants" ) );
		dataNombre.setBorneInf( 		rs.getInt( "BorneInf" ) );
		dataNombre.setBorneSup( 		rs.getInt( "BorneSup" ) );
		dataNombre.setFlagPartieGagnee(	rs.getBoolean( "FlagPartieGagnee" ) );
		dataNombre.setNombreMystere( 	rs.getInt( "NombreMystere" ) );
		return dataNombre;
	}

}
