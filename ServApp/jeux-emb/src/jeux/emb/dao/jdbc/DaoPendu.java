package jeux.emb.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import jeux.emb.dom.DataPendu;
import jeux.emb.dao.IDaoPendu;


public class DaoPendu implements IDaoPendu {

	
	// Champs

	private DataSource		dataSource;

	
	// Injecteurs
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	
	
	// Actions
	
	@Override
	public void inserer( DataPendu dataPendu ) {

		Connection			connection = null;
		PreparedStatement	stmt = null;
		
		try {
			try {
				connection = dataSource.getConnection();
				String sql = "INSERT INTO pendu ( FlagModeFacile, NbErreursMaxi, NbErreursRestantes, MotMystere, Resultat, LettresJouees, IdJoueur ) VALUES ( ?, ?, ?, ?, ?, ?, ? )";
				stmt = connection.prepareStatement( sql );
				stmt.setBoolean(1, dataPendu.isFlagModeFacile() );
				stmt.setInt(	2, dataPendu.getNbErreursMaxi() );
				stmt.setInt(	3, dataPendu.getNbErreursRestantes() );
				stmt.setString(	4, dataPendu.getMotMystere() );
				stmt.setString(	5, dataPendu.getResultat() );
				stmt.setString(	6, dataPendu.getLettresJouees() );
				stmt.setString(	7, dataPendu.getIdJoueur() );
				stmt.executeUpdate();
				
			} finally {
				if (stmt != null) stmt.close();
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	
	@Override
	public void modifier( DataPendu dataPendu ) {

		Connection			connection = null;
		PreparedStatement	stmt = null;

		try {
			try {
				connection = dataSource.getConnection();
				String sql = "UPDATE pendu SET FlagModeFacile = ?, NbErreursMaxi = ?, NbErreursRestantes = ?, MotMystere = ?, Resultat = ?, LettresJouees = ? WHERE IdJoueur =  ?";
				stmt = connection.prepareStatement( sql );
				stmt.setBoolean(1, dataPendu.isFlagModeFacile() );
				stmt.setInt(	2, dataPendu.getNbErreursMaxi() );
				stmt.setInt(	3, dataPendu.getNbErreursRestantes() );
				stmt.setString(	4, dataPendu.getMotMystere() );
				stmt.setString(	5, dataPendu.getResultat() );
				stmt.setString(	6, dataPendu.getLettresJouees() );
				stmt.setString(	7, dataPendu.getIdJoueur() );
				stmt.executeUpdate();
				
			} finally {
				if (stmt != null) stmt.close();
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
	}
	
	
	@Override
	public DataPendu retrouver( String idJoueur ) {

		DataPendu			DATAPendu	= null;
		
		Connection			connection 	= null;
		PreparedStatement	stmt 		= null;
		ResultSet 			rs			= null;

		try {
			try {
				connection = dataSource.getConnection();
				String sql = "SELECT * FROM pendu WHERE IdJoueur = ?";
				stmt = connection.prepareStatement( sql );
				stmt.setString( 1, idJoueur );
				rs = stmt.executeQuery();

				if ( rs.next() ) {
					DATAPendu = construireDataPendu(rs);
				}
			} finally {
				try {
					if (rs != null) rs.close();
				} finally {
					if (stmt != null) stmt.close();
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

			return DATAPendu;
	}
	
	
	// Méthodes auxiliaires
	
	private DataPendu construireDataPendu( ResultSet rs ) throws SQLException {
		DataPendu dataPendu = new DataPendu();
		dataPendu.setIdJoueur( 			rs.getString( "IdJoueur" ) );
		dataPendu.setFlagModeFacile( 	rs.getBoolean( "FlagModeFacile" ) );
		dataPendu.setNbErreursMaxi( 	rs.getInt( "NbErreursMaxi" ) );
		dataPendu.setNbErreursRestantes(rs.getInt( "NbErreursRestantes" ) );
		dataPendu.setMotMystere( 		rs.getString( "MotMystere" ) );
		dataPendu.setResultat( 			rs.getString( "Resultat" ) );
		dataPendu.setLettresJouees(		rs.getString( "LettresJouees" ) );
		return dataPendu;
	}

}
