package jeux.emb.dao;

import jeux.emb.dom.DataPendu;


public interface IDaoPendu {

	void		inserer( DataPendu dataPendu );

	void 		modifier( DataPendu dataPendu );

	DataPendu 	retrouver( String idJoueur );

}
