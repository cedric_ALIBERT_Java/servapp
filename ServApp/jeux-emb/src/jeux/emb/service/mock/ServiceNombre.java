package jeux.emb.service.mock;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import jeux.commun.dto.DtoNombreConfig;
import jeux.commun.dto.DtoNombreReponse;
import jeux.commun.dto.DtoNombreReponse.EnumResponse;
import jeux.commun.service.IServiceNombre;
import jeux.commun.util.ExceptionAppli;
import jeux.emb.dom.DataNombre;


public class ServiceNombre implements IServiceNombre {
	
	
	// Champs
	
	private final Map<String, DataNombre> mapData = new HashMap<>();
	
	private final Random random = new Random();
	
	
	// Actions
	
	@Override
	public DtoNombreConfig retrouverConfig( String idJoueur ) throws ExceptionAppli {
		return mapConfig( mapData.get( idJoueur ) );
	}
	
	@Override
	public void enregistrerConfig( String idJoueur, DtoNombreConfig dtoConfig ) throws ExceptionAppli {
		DataNombre dataNombre = mapData.get( idJoueur );
		dataNombre.setValeurMaxi( dtoConfig.getValeurMaxi() );
		dataNombre.setNbEssaisMaxi( dtoConfig.getNbEssaisMaxi() );
	}
	
	@Override
	public DtoNombreReponse retrouverPartie( String idJoueur ) throws ExceptionAppli {
		return mapReponse( mapData.get( idJoueur ) );
	}
	
	@Override
	public DtoNombreReponse nouvellePartie( String idJoueur ) throws ExceptionAppli {
		DataNombre dataNombre = mapData.get( idJoueur );
		if ( dataNombre == null ) {
			dataNombre = initDataNouveauJoueur( idJoueur );
		}
		dataNombre.setNbEssaisRestants( dataNombre.getNbEssaisMaxi() );
		dataNombre.setNombreMystere( random.nextInt( dataNombre.getValeurMaxi() + 1) );
		dataNombre.setBorneInf(0);
		dataNombre.setBorneSup( dataNombre.getValeurMaxi() );
		dataNombre.setFlagPartieGagnee( false );
		return mapReponse(dataNombre);
	}

	
	@Override
	public DtoNombreReponse jouer( String idJoueur, int proposition ) throws ExceptionAppli {

		DataNombre dataNombre = mapData.get( idJoueur );
		
		// Contrôle de validité
		if ( proposition < 0 || dataNombre.getValeurMaxi() < proposition ) {
			throw new IllegalArgumentException();
		}
		
		dataNombre.setNbEssaisRestants( dataNombre.getNbEssaisRestants() - 1 );

		if ( proposition == dataNombre.getNombreMystere()  ) {
			dataNombre.setFlagPartieGagnee( true );
		} else if ( dataNombre.getNbEssaisRestants() > 0 ) {
			if ( proposition < dataNombre.getNombreMystere()  ) {
				if ( dataNombre.getBorneInf() <= proposition ) {
					dataNombre.setBorneInf( proposition + 1 );
				}
			} else {
				if ( dataNombre.getBorneSup() >= proposition ) {
					dataNombre.setBorneSup( proposition - 1 );
				}
			}
		}
		
		DtoNombreReponse dtoReponse = mapReponse(dataNombre);
		if( dtoReponse.getReponse() == null ) {
			if ( proposition < dataNombre.getNombreMystere()  ) {
				dtoReponse.setReponse( EnumResponse.TROP_PETIT );
			} else {
				dtoReponse.setReponse( EnumResponse.TROP_GRAND );
			}
		}
		return dtoReponse;
	}
	
	
	@Override
	public int tricher( String idJoueur ) throws ExceptionAppli {
		DataNombre dataNombre = mapData.get( idJoueur );
		return dataNombre.getNombreMystere();
	}

	
	// Methodes auxiliaires
	
	private DtoNombreConfig mapConfig( DataNombre dataNombre ) {
		if( dataNombre == null ) {
			return null;
		}
		DtoNombreConfig dtoConfig = new DtoNombreConfig();
		dtoConfig.setValeurMaxi( dataNombre.getValeurMaxi() );
		dtoConfig.setNbEssaisMaxi( dataNombre.getNbEssaisMaxi() );
		return dtoConfig;
	}
	
	private DtoNombreReponse mapReponse( DataNombre dataNombre ) {
		if( dataNombre == null ) {
			return null;
		}
		DtoNombreReponse dtoReponse = new DtoNombreReponse();
		dtoReponse.setNbEssaisRestants( dataNombre.getNbEssaisRestants() );
		dtoReponse.setBorneInf( dataNombre.getBorneInf());
		dtoReponse.setBorneSup( dataNombre.getBorneSup());
		if ( dataNombre.isFlagPartieGagnee() ) {
			dtoReponse.setReponse( EnumResponse.GAGNE );
			dtoReponse.setNombreMystere( dataNombre.getNombreMystere());
		} else if( dataNombre.getNbEssaisRestants() == 0 ) {
			dtoReponse.setReponse( EnumResponse.PERDU );
			dtoReponse.setNombreMystere( dataNombre.getNombreMystere());
		}
		return dtoReponse;
	}
	
	
	private DataNombre initDataNouveauJoueur( String idJoueur ) {
		DataNombre dataNombre = new DataNombre();
		dataNombre.setIdJoueur( idJoueur );
		dataNombre.setValeurMaxi( 32 );
		dataNombre.setNbEssaisMaxi( 5 );
		dataNombre.setBorneSup(-1 );
		mapData.put( idJoueur, dataNombre );
		return dataNombre;
	}
}
