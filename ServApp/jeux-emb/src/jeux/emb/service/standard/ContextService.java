package jeux.emb.service.standard;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import jeux.commun.service.IContextService;
import jeux.emb.dao.IContextDao;


public class ContextService implements IContextService  {
	
	
	// Champs
	
	private final List<Object>		beans = new ArrayList<>();
	
	private final IContextDao		contextDao;
	
	
	// Constructeur
	
	public ContextService(IContextDao contextDao) {
		super();
		this.contextDao = contextDao;
	}


	// Actions
	
	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <T> T getService( Class<T> type ) {
		
		// Recherche dans la liste
		Object bean = null;
		for ( Object obj : beans ) {
			if ( type.isAssignableFrom( obj.getClass() ) ) {
				bean = obj;
				break;
			}
		}
		
		// Si pas trouvé dans la liste
		if ( bean == null ) {
			try {

				// Détermine le type à instancier
				Class<T> typeImpl;
				String nomImpl = type.getSimpleName().substring(1);
				String nomPackage = this.getClass().getPackage().getName();
				nomImpl = nomPackage + "." + nomImpl;
				typeImpl =  (Class<T>) Class.forName( nomImpl );
				Constructor<T> constructor = typeImpl.getConstructor(new Class[] {});

				// Instancie un objet et l'ajoute à la liste
				bean = constructor.newInstance( new Object[] {} ) ;
				beans.add(bean);

				// Injecte les dépendances
				for( Method method : typeImpl.getDeclaredMethods() ) {
					if ( method.getParameterCount() == 1 ) {
						Class typeProp = method.getParameterTypes()[0];
						if ( method.getName().startsWith( "setService" ) ) {
							method.invoke( bean, getService( typeProp ) );
						}
						if ( method.getName().startsWith( "setDao" ) ) {
							method.invoke( bean, contextDao.getDao(typeProp) );
						}
					}
				}
						
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		return (T) bean;
	}

}
