package jeux.emb.service.clientejb;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.InitialContext;

import jeux.commun.service.IContextService;
import jeux.commun.service.IServiceNombre;
import jeux.commun.service.IServicePendu;
import jeux.commun.util.ExceptionAnomalie;
import jeux.commun.util.ExceptionAppli;

public class ClientEjbMock implements IContextService {
	
	//Logger
	private static final Logger logger = 
			Logger.getLogger(ClientEjbMock.class.getName());
	
	//Champs
	private IServiceNombre serviceNombre;
	private IServicePendu servicePendu;
	
	//Actions
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> T getService(Class<T> type) throws ExceptionAppli {
		try {
			//ServiceNombre
			if(type == IServiceNombre.class){
				if(serviceNombre == null){
					InitialContext ic = new InitialContext();
					serviceNombre = (IServiceNombre) ic.lookup( "ejb/mock/ServiceNombre");
					ic.close();
				}
				return (T) serviceNombre;
			}
			//ServicePendu
			if( type == IServicePendu.class) {
				if( servicePendu == null){
					InitialContext ic = new InitialContext();
					servicePendu = (IServicePendu) ic.lookup( "ejb/mock/ServicePendu");
					ic.close();
				}
				return (T) servicePendu;
			}
			//Type inconnu
			throw new IllegalArgumentException(type.getName());
		}catch(Exception e){
			logger.log(Level.SEVERE, e.getMessage(), e);
			throw new ExceptionAnomalie(e);
		}
	}

}
