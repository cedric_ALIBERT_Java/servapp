package jeux.javafx.view;

import javafx.scene.layout.Pane;


public enum EnumView {

	
	// Valeurs
	
	Menu			( "jeux/ViewMenu.fxml" ),
	NombreJeu		( "jeux/ViewNombreJeu.fxml" ),
	NombreConfig	( "jeux/ViewNombreConfig.fxml" ),
	PenduJeu		( "jeux/ViewPenduJeu.fxml" ),
	PenduConfig		( "jeux/ViewPenduConfig.fxml" ),
	;

	
	// Champs
	
	private String		path;
	private Pane		pane;
	private Runnable	runnableEnter;
	private Runnable	runnableEscape;

	
	// Constructeur 
	
	EnumView( String path ) {
		this.path = path;
	}

	
	// Getters & setters

	public String getPathn() {
		return path;
	}

	public Pane getPane() {
		return pane;
	}
	public void setPane(Pane pane) {
		this.pane = pane;
	}
	
	public Runnable getRunnableEnter() {
		return runnableEnter;
	}
	public void setRunnableEnter(Runnable runnableEnter) {
		this.runnableEnter = runnableEnter;
	}
	
	public Runnable getRunnableEscape() {
		return runnableEscape;
	}
	public void setRunnableCancel(Runnable runnableCancel) {
		this.runnableEscape = runnableCancel;
	}
}
