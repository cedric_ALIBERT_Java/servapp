package jeux.javafx.view;

import jeux.javafx.model.IContextModel;

public class ManagerGuiClassic extends ManagerGuiAbstract {

	
	// Champs
    
	private IContextModel	contextModel;
	

	// Constructeur
	
	public ManagerGuiClassic(IContextModel contextModel) {
		super();
		this.contextModel = contextModel;
	}

	
	// Actions
	
	@Override
	public <T> T getModel( Class<T> type ) {
		return contextModel.getModel( type );
	}

}
