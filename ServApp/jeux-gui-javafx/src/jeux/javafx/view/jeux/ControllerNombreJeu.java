package jeux.javafx.view.jeux;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import jeux.javafx.model.IModelNombre;
import jeux.javafx.view.ManagerGuiAbstract;
import jeux.javafx.view.EnumView;
import jeux.javafx.view.IController;

public class ControllerNombreJeu implements IController {
	
	
	// Composants de la vue
	
	@FXML
	private Label		labelMessage;
	
	@FXML
	private Label		labelNbEssaisRestants;

	@FXML
	private TextField	textFieldProposition;
	
	@FXML
	private Button		buttonJouer;
	
	
	// Autre champs
	
	private ManagerGuiAbstract	managerGui;
	private IModelNombre	modelNombre;
	
	
	// Actions
	
	@FXML
	private void doNouvellePartie() {
		try {
			modelNombre.nouvellePartie();
		} catch (Exception e) {
			managerGui.afficherErreur(e);
		}
	}

	
	@FXML
	private void doJouer() {
		try {
			modelNombre.jouer();
		} catch (Exception e) {
			managerGui.afficherErreur(e);
		}
	}
	
	@FXML
	private void doConfig() {
		try {
			modelNombre.preparerconfig();
			managerGui.showView( EnumView.NombreConfig );;;
		} catch (Exception e) {
			managerGui.afficherErreur(e);
		}
	}
	
	@FXML
	private void doMenu() {
		try {
			managerGui.showView( EnumView.Menu );;;
		} catch (Exception e) {
			managerGui.afficherErreur(e);
		}
	}
	
	
	public void doTricher() {
		try {
			modelNombre.tricher();
		} catch (Exception e) {
			managerGui.afficherErreur(e);
		}
	}
	

	// Gestion des évènements
	
	@FXML
	private void onClick( MouseEvent event) {
		try {
			// Test double-clic
			if( event.getClickCount() == 2  && event.getButton() == MouseButton.PRIMARY ) {
				doTricher();
			}
		} catch (Exception e) {
			managerGui.afficherErreur(e);
		}
	}
	
	
	// Initialisaiton
	
	@Override
	public void setManagerGui( ManagerGuiAbstract managerGui ) {

		// Récupère les dépendances
		this.managerGui = managerGui;
		modelNombre = managerGui.getModel( IModelNombre.class);
		
		// Data binding
		labelMessage.textProperty().bind( modelNombre.getMessageVue() );
		labelNbEssaisRestants.textProperty().bind( modelNombre.getNbEssaisRestantsVue() );
		textFieldProposition.textProperty().bindBidirectional( modelNombre.getPropositionVue() );
		buttonJouer.disableProperty().bind( modelNombre.getFlagPartieFinieVue() );
	}

	
}
