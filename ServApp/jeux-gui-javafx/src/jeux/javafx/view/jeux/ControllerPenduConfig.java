package jeux.javafx.view.jeux;

import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import jeux.javafx.model.IModelPendu;
import jeux.javafx.view.ManagerGuiAbstract;
import jeux.javafx.view.EnumView;
import jeux.javafx.view.IController;


public class ControllerPenduConfig  implements IController {
	
	
	// Composants de la vue

	@FXML
	private CheckBox			checkBoxModeFacile;

	@FXML
	private Spinner<Integer>	spinnerNbErreursMaxi;
	
	
	// Autre champs
	
	private ManagerGuiAbstract		managerGui;
	private IModelPendu		modelPendu;
	
	
	// Actions
	
	@FXML
	private void doValider() {
		try {
			modelPendu.enregistrerConfig();
			managerGui.showView( EnumView.PenduJeu );
		} catch (Exception e) {
			managerGui.afficherErreur(e);
		}
	}

	
	@FXML
	private void doAnnuler() {
		try {
			managerGui.showView( EnumView.PenduJeu );
		} catch (Exception e) {
			managerGui.afficherErreur(e);
		}
	}
	
	
	// Initialisaiton
	
	@Override
	public void setManagerGui( ManagerGuiAbstract managerGui ) {

		// Récupère les dépendances
		this.managerGui = managerGui;
		modelPendu = managerGui.getModel( IModelPendu.class );
		
		// Configure le Spinner
		spinnerNbErreursMaxi.setValueFactory( new SpinnerValueFactory.IntegerSpinnerValueFactory( 3, 15) );

		// Data binding
		checkBoxModeFacile.selectedProperty().bindBidirectional( modelPendu.getFlagModeFacileVue() );
		spinnerNbErreursMaxi.getValueFactory().valueProperty().bindBidirectional( modelPendu.getNbErreursMaxiVue()  );
		
	}
	
	
}
