package jeux.javafx.view.jeux;

import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import jeux.javafx.model.IModelPendu;
import jeux.javafx.view.ManagerGuiAbstract;
import jeux.javafx.view.EnumView;
import jeux.javafx.view.IController;


public class ControllerPenduJeu  implements IController {
	
	
	// Composants de la vue
	
	@FXML
	private Label			labelMessage;
	
	@FXML
	private Label			labelResultat;
	
	@FXML
	private ImageView		imageView;
	
	@FXML
	private FlowPane		paneBoutonsLettres;
	
	
	// Autre champs
	
	private ManagerGuiAbstract		managerGui;
	private IModelPendu		modelPendu;
	
	
	// Actions
	
	@FXML
	private void doNouvellePartie() {
		try {
			modelPendu.nouvellePartie();
		} catch (Exception e) {
			managerGui.afficherErreur(e);
		}
	}

	
	public void doTricher() {
		try {
			modelPendu.tricher();
		} catch (Exception e) {
			managerGui.afficherErreur(e);
		}
	}

	
	@FXML
	private void doConfig() {
		try {
			modelPendu.preparerConfig();
			managerGui.showView( EnumView.PenduConfig );
		} catch (Exception e) {
			managerGui.afficherErreur(e);
		}
	}
	
	@FXML
	private void doMenu() {
		try {
			managerGui.showView( EnumView.Menu );
		} catch (Exception e) {
			managerGui.afficherErreur(e);
		}
	}
	

	// Gestion des évènements

	@FXML
	public void onActionBoutonLettre(ActionEvent event ) {
		try {
			if ( modelPendu.getFlagPartieFinieVue().get() ) 
				return;
			Button button =  (Button) event.getSource();
			char lettre = button.getText().charAt(0);
			modelPendu.jouer(lettre);
		} catch (Exception e) {
			managerGui.afficherErreur(e);
		}
	}
	
	@FXML
	public void onKeyPressed( KeyEvent event ) {
		try {
			char lettre = 0;
			if ( ! event.getText().isEmpty() ) {
				lettre = event.getText().toUpperCase().charAt(0);
			}
			if (  ( ! modelPendu.getFlagPartieFinieVue().get() ) && 'A' <= lettre && lettre <= 'Z' ) {
				if ( modelPendu.getLettresJoueesVue().get().indexOf(lettre) < 0 )   {
					modelPendu.jouer(lettre);
				}
			}
		} catch (Exception e) {
			managerGui.afficherErreur(e);
		}
	}
	
	
	@FXML
	private void onClick( MouseEvent event) {
		try {
			// Test double-clic
			if( event.getClickCount() == 2  && event.getButton() == MouseButton.PRIMARY ) {
				modelPendu.tricher();
			}
		} catch (Exception e) {
			managerGui.afficherErreur(e);
		}
	}
	
	
	// Initialisaiton
	
	@Override
	public void setManagerGui( ManagerGuiAbstract managerGui ) {

		// Récupère les dépendances
		this.managerGui = managerGui;
		modelPendu = managerGui.getModel( IModelPendu.class );
		
		
		// Data binding
		
		labelMessage.textProperty().bindBidirectional( modelPendu.getMessageVue() );
		labelResultat.textProperty().bindBidirectional( modelPendu.getResultatVue() );
		imageView.imageProperty().bind( modelPendu.getImageVue() );
		
		ChangeListener<String> listenerLettresJouees =  
				( observable, oldValue, newValue ) -> {
					for ( Node node : paneBoutonsLettres.getChildren() ) {
						if ( node instanceof Button ) {
							node.setDisable( newValue.contains( ((Button) node).getText()) );
						}
					}
			};
		modelPendu.getLettresJoueesVue().addListener( listenerLettresJouees ) ;
		
		
		// Configure le comportement des boutons avec les lettres
		
		// Association de onActionBoutonLettre() + pas de focus
		for ( Node node : paneBoutonsLettres.getChildren() ) {
			if ( node instanceof Button ) {
				((Button)  node).setOnAction( this::onActionBoutonLettre );
				node.setFocusTraversable(false);
			}
		}

	
		// Initialisaiton de l'aaffichage
		listenerLettresJouees.changed( null, null, modelPendu.getLettresJoueesVue().get() );

	}
	
}
