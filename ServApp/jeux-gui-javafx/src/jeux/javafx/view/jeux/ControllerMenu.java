package jeux.javafx.view.jeux;

import javafx.application.Platform;
import javafx.fxml.FXML;
import jeux.javafx.model.IModelNombre;
import jeux.javafx.model.IModelPendu;
import jeux.javafx.view.ManagerGuiAbstract;
import jeux.javafx.view.EnumView;
import jeux.javafx.view.IController;

public class ControllerMenu implements IController {
	
	
	// Champs
	
	private ManagerGuiAbstract		managerGui;
	
	
	// Actions
	
	@FXML
	private void doNombreMystere() {
			managerGui.execTask( () -> {
				try {
					managerGui.getModel( IModelNombre.class ).retrouverPartie();
					managerGui.showView( EnumView.NombreJeu ) ;
				} catch (Exception e) {
					Platform.runLater( () -> managerGui.afficherErreur(e) );
				}
			} );
	}
	
	@FXML
	private void doPendu() {
		managerGui.execTask( () -> {
			try {
				managerGui.getModel( IModelPendu.class ).retrouverPartie();
				managerGui.showView( EnumView.PenduJeu ) ;
			} catch (Exception e) {
				Platform.runLater( () -> managerGui.afficherErreur(e) );
			}
		} );
	}

	@FXML
	private void doSudoku() {
	}
	
	@FXML
	private void doQuitter() {
		try {
			managerGui.close();
		} catch (Exception e) {
			managerGui.afficherErreur(e);
		}
	}
	
	
	// Initialisation
	
	public void setManagerGui(ManagerGuiAbstract managerGui) {
		this.managerGui = managerGui;
	}
	
}
