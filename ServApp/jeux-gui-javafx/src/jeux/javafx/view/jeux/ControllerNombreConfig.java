package jeux.javafx.view.jeux;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import jeux.javafx.model.IModelNombre;
import jeux.javafx.view.ManagerGuiAbstract;
import jeux.javafx.view.EnumView;
import jeux.javafx.view.IController;


public class ControllerNombreConfig implements IController {
	
	
	// Composants de la vue

	@FXML
	private TextField	textFieldValeurMaxi;

	@FXML
	private TextField	textFieldNbEssaisMaxi;
	
	
	// Autre champs
	
	private ManagerGuiAbstract	managerGui;
	private IModelNombre	modelNombre;
	
	
	// Actions
	
	@FXML
	private void doValider() {
		try {
			modelNombre.enregistrerconfig();
			managerGui.showView( EnumView.NombreJeu ) ;
		} catch (Exception e) {
			managerGui.afficherErreur(e);
		}
	}

	
	@FXML
	private void doAnnuler() {
		try {
			managerGui.showView( EnumView.NombreJeu ) ;
		} catch (Exception e) {
			managerGui.afficherErreur(e);
		}
	}
	
	
	// Initialisaiton
	
	@Override
	public void setManagerGui( ManagerGuiAbstract managerGui ) {

		// Récupère les dépendances
		this.managerGui = managerGui;
		modelNombre = managerGui.getModel( IModelNombre.class);
		
		// Data binding
		textFieldValeurMaxi.textProperty().bindBidirectional( modelNombre.getValeurMaxiVue() );
		textFieldNbEssaisMaxi.textProperty().bindBidirectional( modelNombre.getNbEssaisMaxiVue() );
		
	}
	
	
}
