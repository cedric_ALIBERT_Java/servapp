package jeux.javafx.model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.StringProperty;
import jeux.commun.util.ExceptionAppli;

public interface IModelNombre {

	StringProperty getValeurMaxiVue();

	StringProperty getNbEssaisMaxiVue();

	StringProperty getNbEssaisRestantsVue();

	StringProperty getMessageVue();

	StringProperty getPropositionVue();

	BooleanProperty getFlagPartieFinieVue();

	void preparerconfig() throws ExceptionAppli;

	void enregistrerconfig() throws ExceptionAppli;

	void nouvellePartie() throws ExceptionAppli;
	
	void retrouverPartie() throws ExceptionAppli;

	void jouer() throws ExceptionAppli;

	void tricher() throws ExceptionAppli;

}