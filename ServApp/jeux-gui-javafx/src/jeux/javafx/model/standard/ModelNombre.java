package jeux.javafx.model.standard;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.util.Duration;
import jeux.commun.dto.DtoNombreConfig;
import jeux.commun.dto.DtoNombreReponse;
import jeux.commun.dto.DtoNombreReponse.EnumResponse;
import jeux.commun.service.IServiceNombre;
import jeux.commun.util.ExceptionAppli;
import jeux.javafx.model.IModelNombre;


public class ModelNombre implements IModelNombre {
	
	
	// Champs pour la vue Config
	
	private final StringProperty		valeurMaxiVue	= new SimpleStringProperty();
	private final StringProperty		nbEssaisMaxiVue	= new SimpleStringProperty();

	
	// Champs pour la vue Jeu
	
	private final StringProperty		messageVue			= new SimpleStringProperty();
	private final StringProperty		nbEssaisRestantsVue	= new SimpleStringProperty();
	private final StringProperty		propositionVue		= new SimpleStringProperty();
	private final BooleanProperty		flagPartieFinieVue	= new SimpleBooleanProperty();

	
	// Valeurs courantes

	private String		message;
	
	
	// Autres champs
	
	private String			idJoueur = "XX";

	private IServiceNombre	serviceNombre;
	
	
	// Injecteurs
	
	public void setServiceNombre(IServiceNombre serviceNombre) {
		this.serviceNombre = serviceNombre;
	}
	
	
	// Getters

	@Override
	public final StringProperty getValeurMaxiVue() {
		return this.valeurMaxiVue;
	}

	@Override
	public final StringProperty getNbEssaisMaxiVue() {
		return this.nbEssaisMaxiVue;
	}

	@Override
	public final StringProperty getNbEssaisRestantsVue() {
		return this.nbEssaisRestantsVue;
	}

	@Override
	public final StringProperty getMessageVue() {
		return this.messageVue;
	}

	@Override
	public final StringProperty getPropositionVue() {
		return this.propositionVue;
	}
	
	@Override
	public final BooleanProperty getFlagPartieFinieVue() {
		return this.flagPartieFinieVue;
	}
	
	
	// Actions
	
	@Override
	public void preparerconfig() throws ExceptionAppli {
		DtoNombreConfig dtoConfig = serviceNombre.retrouverConfig(idJoueur);
		valeurMaxiVue.set( String.valueOf( dtoConfig.getValeurMaxi() ) );
		nbEssaisMaxiVue.set( String.valueOf( dtoConfig.getNbEssaisMaxi() ) );
	}
	
	@Override
	public void enregistrerconfig() throws ExceptionAppli {
		DtoNombreConfig dtoConfig = new DtoNombreConfig();
		try {
			dtoConfig.setValeurMaxi( Integer.parseInt( valeurMaxiVue.get() ) );
		} catch (NumberFormatException e) {
		}
		try {
			dtoConfig.setNbEssaisMaxi( Integer.parseInt( nbEssaisMaxiVue.get() ) );
		} catch (NumberFormatException e) {
		}
		serviceNombre.enregistrerConfig( idJoueur, dtoConfig );
		nouvellePartie();
	}

	
	@Override
	public void retrouverPartie() throws ExceptionAppli {
		DtoNombreReponse dtoReponse = serviceNombre.retrouverPartie(idJoueur);
		if( dtoReponse == null ) {
			dtoReponse = serviceNombre.nouvellePartie(idJoueur);
		}
		actualiserVueJeu( dtoReponse );		
	}

	
	@Override
	public void nouvellePartie() throws ExceptionAppli {
		DtoNombreReponse dtoReponse = serviceNombre.nouvellePartie(idJoueur);
		actualiserVueJeu( dtoReponse );		
	}

	
	@Override
	public void jouer() throws ExceptionAppli {
		
		int proposition;
		
		try {

			proposition = Integer.parseInt( propositionVue.get() );
			DtoNombreReponse dtoReponse = serviceNombre.jouer(idJoueur, proposition);

			if (dtoReponse.getReponse() == EnumResponse.TROP_PETIT ) {
				message = proposition + " est trop petit.";
			} else if (dtoReponse.getReponse() == EnumResponse.TROP_GRAND ) {
				message = proposition + " est trop grand.";
			}
			actualiserVueJeu( dtoReponse );		

		} catch (IllegalArgumentException e) {
			message = "Valeur incorreecte.\nRecommencez !";
			messageVue.set(message);
		}

	}
	
	
	@Override
	public void tricher() throws ExceptionAppli {
		
		message = messageVue.get();

		// Rétablit l'affichage normal au bout de 500 millisecondes 
		Timeline timeline = new Timeline(new KeyFrame(
		        Duration.millis(500),
		        ae -> messageVue.set( message )) );
		timeline.play();
		
		// Affiche le message de triche
		messageVue.set( "La solution est " + serviceNombre.tricher(idJoueur) ) ;
	}
	
	
	// Méthodes auxiliaires
	
	private void actualiserVueJeu( DtoNombreReponse dtoReponse ) {
		
		if (dtoReponse.getReponse() == null ) {
			message = String.format( "Trouvez un nombre entre %d et %d%nVous avez droit à %d essais.", dtoReponse.getBorneInf(), dtoReponse.getBorneSup(), dtoReponse.getNbEssaisRestants() );
		} else if (dtoReponse.getReponse() == EnumResponse.GAGNE ) {
			message = "Gagné !\nLe nombre mystère était " + dtoReponse.getNombreMystere();
		} else if (dtoReponse.getReponse() == EnumResponse.PERDU ) {
			message = "Perdu !\nLe nombre mystère était " + dtoReponse.getNombreMystere() ;
		}
		
		messageVue.set(message);
		if ( dtoReponse.getNbEssaisRestants() < 2) {
			nbEssaisRestantsVue.set( dtoReponse.getNbEssaisRestants() + " essai restant" );
		} else {
			nbEssaisRestantsVue.set( dtoReponse.getNbEssaisRestants() + " essais restants" );
		}

		if ( dtoReponse.getReponse() == EnumResponse.GAGNE || dtoReponse.getReponse() == EnumResponse.PERDU ) {
			flagPartieFinieVue.set( true );
		} else {
			flagPartieFinieVue.set( false );
			propositionVue.set(null);
		}
		
	}
	
	
}
