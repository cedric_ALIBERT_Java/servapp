package jeux.javafx.model.standard;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.image.Image;
import javafx.util.Duration;
import jeux.commun.dto.DtoPenduConfig;
import jeux.commun.dto.DtoPenduReponse;
import jeux.commun.dto.DtoPenduReponse.EnumResponse;
import jeux.commun.service.IServicePendu;
import jeux.commun.util.ExceptionAppli;
import jeux.javafx.model.IModelPendu;
import jeux.javafx.view.EnumView;


public class ModelPendu implements IModelPendu {
	
	
	// Champs pour la vue Config
	
	private final BooleanProperty	flagModeFacileVue	= new SimpleBooleanProperty();
	private final Property<Integer>	nbErreursMaxiVue			= new SimpleObjectProperty<>(0);
	
	
	// Champs pour la vue Jeu

	private final StringProperty	messageVue		= new SimpleStringProperty();
	private final StringProperty	resultatVue		= new SimpleStringProperty();
	private final Property<Image>	imageVue		= new SimpleObjectProperty<>();
	private final BooleanProperty	flagPartieDebutVue	= new SimpleBooleanProperty();
	private final BooleanProperty	flagPartieFinieVue	= new SimpleBooleanProperty();
	private final StringProperty	lettresJoueesVue	= new SimpleStringProperty();
	
	
	
	// Valeurs courantes
	
	private Image[]		images = new Image[9];
	private int			nbErreursMaxi;

	private String		message;
	private boolean		flagPartieDebut;
	private boolean		flagPartieFinie;
	
	
	// Autres champs
	
	private String			idJoueur = "XX";

	private IServicePendu	servicePendu;

	
	// Injecteurs
	
	public void setServicePendu(IServicePendu servicePendu) {
		this.servicePendu = servicePendu;
	}
	
	// Getters
	
	@Override
	public final BooleanProperty getFlagModeFacileVue() {
		return flagModeFacileVue;
	}


	@Override
	public final Property<Integer> getNbErreursMaxiVue() {
		return nbErreursMaxiVue;
	}


	@Override
	public final StringProperty getMessageVue() {
		return messageVue;
	}


	@Override
	public final StringProperty getResultatVue() {
		return resultatVue;
	}


	@Override
	public final Property<Image> getImageVue() {
		return imageVue;
	}

	@Override
	public final BooleanProperty getFlagPartieDebutVue() {
		return flagPartieDebutVue;
	}

	@Override
	public final BooleanProperty getFlagPartieFinieVue() {
		return flagPartieFinieVue;
	}
	
	@Override
	public final StringProperty getLettresJoueesVue() {
		return lettresJoueesVue;
	}
	
	
	// Constructeur
	
	public ModelPendu() {
		initialiserImages();
	}
	
	
	// Actions

	@Override
	public void preparerConfig() throws ExceptionAppli {
		DtoPenduConfig dtoConfig = servicePendu.retrouverConfig(idJoueur);
		flagModeFacileVue.set( dtoConfig.isFlagModeFacile() );
		nbErreursMaxiVue.setValue( dtoConfig.getNbErreursMaxi() );
	}
	
	@Override
	public void enregistrerConfig() throws ExceptionAppli {
		DtoPenduConfig dtoConfig = new DtoPenduConfig();
		dtoConfig.setFlagModeFacile( flagModeFacileVue.get() );
		dtoConfig.setNbErreursMaxi( nbErreursMaxiVue.getValue() );
		servicePendu.enregistrerConfig( idJoueur, dtoConfig );
		nouvellePartie();
	}

	@Override
	public void retrouverPartie() throws ExceptionAppli {
		DtoPenduReponse dtoReponse = servicePendu.retrouverPartie(idJoueur);
		if( dtoReponse == null ) {
			dtoReponse = servicePendu.nouvellePartie(idJoueur);
		}
		actualiserVueJeu( dtoReponse );		
//		DtoPenduConfig dtoConfig = servicePendu.retrouverConfig( idJoueur );
//		DtoPenduReponse dtoReponse = servicePendu.retrouverPartie( idJoueur );
//		nbErreursMaxi = dtoConfig.getNbErreursMaxi();
//		flagPartieDebut = true;
//		flagPartieFinie = false;
//		actualiserVueJeu( dtoReponse );
	}
	
	
	@Override
	public void nouvellePartie() throws ExceptionAppli {
		DtoPenduReponse dtoReponse = servicePendu.nouvellePartie( idJoueur );
		actualiserVueJeu( dtoReponse );
	}
	
	@Override
	public void jouer( char lettre ) throws ExceptionAppli {

		if ( flagPartieFinie ) {
			return;
		} else {
			flagPartieDebut = false;
		}
		
		DtoPenduReponse dtoReponse = servicePendu.jouer( idJoueur,	lettre );
		actualiserVueJeu( dtoReponse );
	}

	
	@Override
	public void tricher() throws ExceptionAppli {
		
		String sauvegarde = messageVue.get();

		// Rétablit l'affichage normal au bout de 500 millisecondes 
		Timeline timeline = new Timeline(new KeyFrame(
		        Duration.millis(500),
		        ae -> messageVue.set( sauvegarde )) );
		timeline.play();
		
		// Affiche le message de triche
		messageVue.set( "La solution est " + servicePendu.tricher( idJoueur ) ) ;
		
	}
	
	
	// Méthodes auxiliaires
	
	private void actualiserVueJeu( DtoPenduReponse dtoReponse ) throws ExceptionAppli {
		
		if( dtoReponse.getReponse() == EnumResponse.GAGNE ) {
			flagPartieFinie = true;
			message = "Félicitations, vous avez gagné !";
		} else if( dtoReponse.getReponse() == EnumResponse.PERDU ) {
			flagPartieFinie = true;
			message = "Vous avez perdu.\nLa solution était " + dtoReponse.getMotMystere();
		} else {
			message = String.format( "Trouvez le mot caché.%nVous avez droit à %d erreurs.", dtoReponse.getNbErreursRestantes() );
		}
		
		messageVue.set(message);
		resultatVue.set( dtoReponse.getResultat() );
		flagPartieDebutVue.set( flagPartieDebut );
		flagPartieFinieVue.set( flagPartieFinie );
		lettresJoueesVue.set( dtoReponse.getLettresJouees() );
		if ( dtoReponse.getNbErreursRestantes() == nbErreursMaxi && nbErreursMaxi < 7 ) {
			imageVue.setValue( images[1]);
		} else {
			imageVue.setValue( images[ Math.max( 0, 7 - dtoReponse.getNbErreursRestantes() ) ]);
		}
	}
	
	
	private void initialiserImages() {
		for ( int i = 0; i < images.length; i++ ) {
			images[i] = new Image( EnumView.class.getResource( "images/pendu-" + i + ".png").toString() );
		}
	}
}
