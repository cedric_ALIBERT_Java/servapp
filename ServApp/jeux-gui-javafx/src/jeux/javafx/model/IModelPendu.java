package jeux.javafx.model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.Property;
import javafx.beans.property.StringProperty;
import javafx.scene.image.Image;
import jeux.commun.util.ExceptionAppli;

public interface IModelPendu {

	BooleanProperty getFlagModeFacileVue();

	Property<Integer> getNbErreursMaxiVue();

	StringProperty getMessageVue();

	StringProperty getResultatVue();

	Property<Image> getImageVue();

	BooleanProperty getFlagPartieDebutVue();

	BooleanProperty getFlagPartieFinieVue();

	StringProperty getLettresJoueesVue();

	void preparerConfig() throws ExceptionAppli;

	void enregistrerConfig() throws ExceptionAppli;

	void nouvellePartie() throws ExceptionAppli;
	
	void retrouverPartie() throws ExceptionAppli;

	void jouer(char lettre) throws ExceptionAppli;

	void tricher() throws ExceptionAppli;

}