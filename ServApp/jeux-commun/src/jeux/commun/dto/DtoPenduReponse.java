package jeux.commun.dto;

import java.io.Serializable;


@SuppressWarnings("serial")
public class DtoPenduReponse implements Serializable {

	// Enumératon
	public static enum EnumResponse { EN_COURS, GAGNE, PERDU }

	
	// Champs
	
	private EnumResponse	reponse;
	private String			resultat;
	private int				nbErreursRestantes;
	private String			motMystere;
	private String			lettresJouees;
	
	
	// Getters & Setters

	public EnumResponse getReponse() {
		return reponse;
	}

	public void setReponse(EnumResponse reponse) {
		this.reponse = reponse;
	}

	public String getResultat() {
		return resultat;
	}

	public void setResultat(String resultat) {
		this.resultat = resultat;
	}

	public int getNbErreursRestantes() {
		return nbErreursRestantes;
	}

	public void setNbErreursRestantes(int nbErreurs) {
		this.nbErreursRestantes = nbErreurs;
	}

	public String getMotMystere() {
		return motMystere;
	}

	public void setMotMystere(String motMystere) {
		this.motMystere = motMystere;
	}

	public String getLettresJouees() {
		return lettresJouees;
	}

	public void setLettresJouees(String lettresJouees) {
		this.lettresJouees = lettresJouees;
	}
	
}
