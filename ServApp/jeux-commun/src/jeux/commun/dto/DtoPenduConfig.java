package jeux.commun.dto;

import java.io.Serializable;


@SuppressWarnings("serial")
public class DtoPenduConfig implements Serializable {
	
	
	// Champs
	
	private boolean		flagModeFacile;
	private int			nbErreursMaxi;

	
	// Getters & Setters

	public boolean isFlagModeFacile() {
		return flagModeFacile;
	}

	public void setFlagModeFacile(boolean flagModeFacile) {
		this.flagModeFacile = flagModeFacile;
	}

	public int getNbErreursMaxi() {
		return nbErreursMaxi;
	}

	public void setNbErreursMaxi(int nbErreursMaxi) {
		this.nbErreursMaxi = nbErreursMaxi;
	}
	

}
