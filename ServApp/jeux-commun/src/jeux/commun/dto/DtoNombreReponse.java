package jeux.commun.dto;

import java.io.Serializable;


@SuppressWarnings("serial")
public class DtoNombreReponse implements Serializable {

	// Enumératon
	public static enum EnumResponse { TROP_PETIT, TROP_GRAND, GAGNE, PERDU }

	
	// Champs
	
	private EnumResponse	reponse;
	private int				nbEssaisRestants;
	private int				nombreMystere;
	private int				borneInf;
	private int				borneSup;
	
	
	// Getters & Setters

	public EnumResponse getReponse() {
		return reponse;
	}

	public void setReponse(EnumResponse reponse) {
		this.reponse = reponse;
	}

	public int getNbEssaisRestants() {
		return nbEssaisRestants;
	}

	public void setNbEssaisRestants(int nbEssaisRestants) {
		this.nbEssaisRestants = nbEssaisRestants;
	}

	public int getNombreMystere() {
		return nombreMystere;
	}

	public void setNombreMystere(int nombreMystere) {
		this.nombreMystere = nombreMystere;
	}

	public int getBorneInf() {
		return borneInf;
	}

	public void setBorneInf(int borneInf) {
		this.borneInf = borneInf;
	}

	public int getBorneSup() {
		return borneSup;
	}

	public void setBorneSup(int borneSup) {
		this.borneSup = borneSup;
	}

}



