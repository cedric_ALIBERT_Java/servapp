package jeux.commun.dto;

import java.io.Serializable;


@SuppressWarnings("serial")
public class DtoNombreConfig implements Serializable {

	
	// Champs
	
	private int			valeurMaxi;
	private int			nbEssaisMaxi;

	
	// Getters & Setters

	public final int getValeurMaxi() {
		return valeurMaxi;
	}

	public final void setValeurMaxi(int borneSup) {
		this.valeurMaxi = borneSup;
	}

	public final int getNbEssaisMaxi() {
		return nbEssaisMaxi;
	}

	public final void setNbEssaisMaxi(int nbEssaisMaxi) {
		this.nbEssaisMaxi = nbEssaisMaxi;
	}
	
}
