package jeux.commun.service;

import jeux.commun.util.ExceptionAppli;

public interface IContextService {
	
	<T> T 		getService( Class<T> type ) throws ExceptionAppli;

}