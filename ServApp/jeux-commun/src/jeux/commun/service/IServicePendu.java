package jeux.commun.service;

import jeux.commun.dto.DtoPenduConfig;
import jeux.commun.dto.DtoPenduReponse;
import jeux.commun.util.ExceptionAppli;


public interface IServicePendu {

	DtoPenduConfig retrouverConfig( String idJoueur ) throws ExceptionAppli;

	void enregistrerConfig( String idJoueur, DtoPenduConfig dtoConfig ) throws ExceptionAppli;

	DtoPenduReponse retrouverPartie( String idJoueur ) throws ExceptionAppli;

	DtoPenduReponse nouvellePartie( String idJoueur ) throws ExceptionAppli;

	DtoPenduReponse jouer( String idJoueur, char lettre ) throws ExceptionAppli;

	String tricher( String idJoueur ) throws ExceptionAppli;

}
