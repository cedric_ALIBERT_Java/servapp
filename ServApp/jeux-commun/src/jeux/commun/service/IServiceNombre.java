package jeux.commun.service;

import jeux.commun.dto.DtoNombreConfig;
import jeux.commun.dto.DtoNombreReponse;
import jeux.commun.util.ExceptionAppli;

public interface IServiceNombre {

	DtoNombreConfig retrouverConfig( String idJoueur ) throws ExceptionAppli;

	void enregistrerConfig( String idJoueur, DtoNombreConfig dtoConfig ) throws ExceptionAppli;

	DtoNombreReponse retrouverPartie( String idJoueur ) throws ExceptionAppli;

	DtoNombreReponse nouvellePartie( String idJoueur ) throws ExceptionAppli;

	DtoNombreReponse jouer( String idJoueur, int proposition ) throws ExceptionAppli;

	int tricher( String idJoueur ) throws ExceptionAppli;
	
}
