SELECT `contacts`.`personne`.`Nom`, `contacts`.`personne`.`Prenom`,
  `contacts`.`categorie`.`Libelle`
  FROM
       `contacts`.`categorie` JOIN `contacts`.`personne` ON `contacts`.`categorie`.`IdCategorie` = `contacts`.`personne`.`IdCategorie`
  WHERE `contacts`.`categorie`.`Libelle` LIKE 'E%'
  ORDER BY `contacts`.`categorie`.`Libelle` ASC, `contacts`.`personne`.`Nom` ASC
