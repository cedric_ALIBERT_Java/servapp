select * from personne;

select p.Nom, p.Prenom, c.Libelle
FROM personne p INNER JOIN categorie c
ON p.IdCategorie = c.IdCategorie
WHERE c.Libelle like 'E%'
ORDER BY c.Libelle, p.Nom;