package contacts.emb.dao;

import java.util.List;

import contacts.emb.dom.Personne;
import contacts.emb.dom.Telephone;


public interface IDaoTelephone {

	int 		inserer( Telephone telephone );

	void 		modifier( Telephone telephone );

	void 		supprimer( int idTelephone );

	List<Telephone> listerPourPersonne( Personne personne );

}
