package contacts.emb.dao;

public interface IManagerTransaction {
	
	void transactionBegin();
	
	void transactionCommit();
	
	void transactionRollback();

}
