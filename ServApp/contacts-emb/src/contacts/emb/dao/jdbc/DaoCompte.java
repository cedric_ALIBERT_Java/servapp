package contacts.emb.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import contacts.emb.dao.IDaoCompte;
import contacts.emb.dom.Compte;


public class DaoCompte implements IDaoCompte {

	
	// Champs

	private DataSource		dataSource;

	
	// Injecteurs
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	
	// Actions
	
	@Override
	public int inserer(Compte compte)  {

		Connection			connection	= null;
		PreparedStatement	stmt		= null;
		ResultSet 			rs 			= null;

		try {
			try {
				connection = dataSource.getConnection();
				String sql = "INSERT INTO compte ( Pseudo, MotDePasse, Email ) VALUES ( ?, ?, ? )";
				stmt = connection.prepareStatement(sql,
						new String[] { "ICompte" } );
				stmt.setString(	1, compte.getPseudo() );
				stmt.setString(	2, compte.getMotDePasse() );
				stmt.setString(	3, compte.getEmail() );
				stmt.executeUpdate();
	
				rs = stmt.getGeneratedKeys();
				rs.next();
				int id = rs.getInt(1);
				compte.setId( id );
				return id;
	
			} finally {
				try {
					if (rs != null) rs.close();
				} finally {
					if (stmt != null) stmt.close();
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	
	@Override
	public void modifier(Compte compte)  {

		Connection			connection	= null;
		PreparedStatement	stmt		= null;

		try {

			connection = dataSource.getConnection();

			try {
				String sql = "UPDATE compte SET Pseudo = ?, MotDePasse = ?, Email = ? WHERE IdCompte =  ?";
				stmt = connection.prepareStatement( sql );
				stmt.setString(	1, compte.getPseudo() );
				stmt.setString(	2, compte.getMotDePasse() );
				stmt.setString(	3, compte.getEmail() );
				stmt.setInt(	4, compte.getId() );
				stmt.executeUpdate();
	
			} finally {
				if (stmt != null) stmt.close();
			}

			// Met à jour les liens avec les groupes (table appartenir)
			try {
				String sql = "DELETE FROM role WHERE IdCompte =  ?";
				stmt = connection.prepareStatement( sql );
				stmt.setInt(	1, compte.getId() );
				stmt.executeUpdate();
			} finally {
				if (stmt != null) stmt.close();
			}

			try {
				String sql = "INSERT INTO role (IdCompte, Role) VALUES (?,?)";
				stmt = connection.prepareStatement( sql );
				for( String role : compte.getRoles() ) {
					stmt.setInt(	1, compte.getId() );
					stmt.setString(	2, role );
					stmt.executeUpdate();
				}
			} finally {
					if (stmt != null) stmt.close();
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	
	@Override
	public void supprimer(int id)  {

		Connection			connection	= null;
		PreparedStatement	stmt		= null;

		String 				sql;

		try {
			connection = dataSource.getConnection();
			try {
				sql = "DELETE FROM appartenir  WHERE IdCompte = ? ";
				stmt = connection.prepareStatement(sql);
				stmt.setInt( 1, id );
				stmt.executeUpdate();
			} finally {
				if (stmt != null) stmt.close();
			}

			// Supprime le compte
			try {
				sql = "DELETE FROM compte WHERE IdCompte = ? ";
				stmt = connection.prepareStatement(sql);
				stmt.setInt( 1, id );
				stmt.executeUpdate();
			} finally {
				if (stmt != null) stmt.close();
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	
	@Override
	public Compte retrouver(int id)  {

		Compte 				compte 		= null;

		Connection			connection	= null;
		PreparedStatement	stmt		= null;
		ResultSet 			rs 			= null;

		try {
	        try {
				connection = dataSource.getConnection();
	            String sql = "SELECT * FROM compte WHERE IdCompte = ?";
	            stmt = connection.prepareStatement(sql);
	            stmt.setInt(1, id);
	            rs = stmt.executeQuery();
	
	            if ( rs.next() ) {
	                compte = construireCompte(rs);
	            }
			} finally {
				try {
					if (rs != null) rs.close();
				} finally {
					if (stmt != null) stmt.close();
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

        return compte;
	}

	
	@Override
	public List<Compte> listerTout()   {

		List<Compte> 		comptes 	= new ArrayList<>();
		
		Connection			connection	= null;
		PreparedStatement	stmt		= null;
		ResultSet 			rs 			= null;

		try {
			try {
				connection = dataSource.getConnection();
				String sql = "SELECT * FROM compte ORDER BY IdCompte";
				stmt = connection.prepareStatement(sql);
				rs = stmt.executeQuery();
	
				while (rs.next()) {
					comptes.add( construireCompte(rs) );
				}
			} finally {
				try {
					if (rs != null) rs.close();
				} finally {
					if (stmt != null) stmt.close();
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

		return comptes;
	}


	@Override
	public Compte validerAuthentification(String pseudo, String motDePasse)  {

		Compte 				compte		= null;
		
		Connection			connection	= null;
		PreparedStatement	stmt		= null;
		ResultSet 			rs 			= null;

		try {
			try {
				connection = dataSource.getConnection();
				String sql = "SELECT * FROM compte WHERE pseudo = ? AND MotDePasse = ?";
				stmt = connection.prepareStatement(sql);
				stmt.setString(1, pseudo);
				stmt.setString(2, motDePasse);
				rs = stmt.executeQuery();
	
				if (rs.next()) {
					compte = construireCompte(rs);			
				}
			} finally {
				try {
					if (rs != null) rs.close();
				} finally {
					if (stmt != null) stmt.close();
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

		return compte; 
	}


	@Override
	public boolean verifierUnicitePseudo( String pseudo, int idCompte )   {

		int					nbComptes 	= 0;

		Connection			connection	= null;
		PreparedStatement	stmt		= null;
		ResultSet 			rs 			= null;

		try {
			try {
				connection = dataSource.getConnection();
				String sql = "SELECT COUNT(*) AS nbComptes"
						+ " FROM compte WHERE pseudo = ? AND IdCompte <> ?";
				stmt = connection.prepareStatement(sql);
				stmt.setString(	1, pseudo );
				stmt.setInt(	2, idCompte );
				rs = stmt.executeQuery();
				
				rs.next();
				nbComptes = rs.getInt("nbComptes");
	
			} finally {
				try {
					if (rs != null) rs.close();
				} finally {
					if (stmt != null) stmt.close();
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

		return nbComptes == 0;

	}
	
	
	// Méthodes auxiliaires
	
	private Compte construireCompte( ResultSet rs ) throws SQLException {
		Compte compte = new Compte();
		compte.setId(rs.getInt("IdCompte"));
		compte.setPseudo(rs.getString("Pseudo"));
		compte.setMotDePasse(rs.getString("MotDePasse"));
		compte.setEmail(rs.getString("Email"));
		compte.setRoles( listerRolePourCompte(compte.getId()));
		return compte;
	}

	
	private List<String> listerRolePourCompte( int IdCompte )   {

		List<String> 		roles 		= new ArrayList<>();

		Connection			connection	= null;
		PreparedStatement	stmt		= null;
		ResultSet 			rs 			= null;

		try {
			try {
				connection = dataSource.getConnection();
				String sql = "SELECT * FROM role WHERE IdCompte = ? ORDER BY Role";
				stmt = connection.prepareStatement(sql);
				stmt.setInt( 1, IdCompte );
				rs = stmt.executeQuery();
	
				while (rs.next()) {
					roles.add( rs.getString("Role") );
				}
			} finally {
				try {
					if (rs != null) rs.close();
				} finally {
					if (stmt != null) stmt.close();
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

		return roles;
	}
	
}
