package contacts.emb.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import contacts.emb.dao.IDaoCategorie;
import contacts.emb.dao.IDaoPersonne;
import contacts.emb.dao.IDaoTelephone;
import contacts.emb.dom.Personne;
import contacts.emb.dom.Telephone;


public class DaoPersonne implements IDaoPersonne {

	
	// Champs

	private DataSource			dataSource;
	private IDaoCategorie		daoCategorie;
	private IDaoTelephone		daoTelephone;

	
	// Injecteurs
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public void setDaoCategorie(IDaoCategorie daoCategorie) {
		this.daoCategorie = daoCategorie;
	}
	
	public void setDaoTelephone(IDaoTelephone daoTelephone) {
		this.daoTelephone = daoTelephone;
	}

	
	// Actions

	@Override
	public int inserer(Personne personne) {

		Connection			connection	= null;
		PreparedStatement	stmt		= null;
		ResultSet 			rs 			= null;

		try {
			try {
				connection = dataSource.getConnection();
				String sql = "INSERT INTO personne ( Nom, Prenom, IdCategorie ) VALUES ( ?, ?, ? )";
				stmt = connection.prepareStatement(sql,
						new String[] { "IPersonne" } );
				stmt.setString(	1, personne.getNom() );
				stmt.setString(	2, personne.getPrenom() );
				stmt.setInt(	3, personne.getCategorie().getId() );
				stmt.executeUpdate();
	
				rs = stmt.getGeneratedKeys();
				rs.next();
				int id = rs.getInt(1);
				personne.setId( id );
	
			} finally {
				try {
					if (rs != null) rs.close();
				} finally {
					if (stmt != null) stmt.close();
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
		// Insertion des téléphone
		for( Telephone telephone : personne.getTelephones() ) {
			daoTelephone.inserer(telephone);
		}
		
		return personne.getId();
	}
 
	@Override
	public void modifier(Personne personne) {

		Connection			connection	= null;
		PreparedStatement	stmt		= null;

		try {
			try {
				connection = dataSource.getConnection();
				String sql = "UPDATE personne SET Nom = ?, Prenom = ?, IdCategorie = ? WHERE IdPersonne =  ?";
				stmt = connection.prepareStatement( sql );
				stmt.setString(	1, personne.getNom() );
				stmt.setString(	2, personne.getPrenom() );
				stmt.setInt(	3, personne.getCategorie().getId() );
				stmt.setInt(	4, personne.getId() );
				stmt.executeUpdate();
	
			} finally {
				if (stmt != null) stmt.close();
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	
		// Suppression des téléphones retirés de la liste
		for ( Telephone telephone : daoTelephone.listerPourPersonne(personne) ) {
			if ( ! personne.getTelephones().contains(telephone) ) {
				daoTelephone.supprimer( telephone.getId() );
			}
		}
		
		// Insertion ou modificaiton des téléphones de la liste
		for ( Telephone telephone : personne.getTelephones() ) {
			if ( telephone.getId() == 0 ) {
				daoTelephone.inserer(telephone);
			} else {
				daoTelephone.modifier(telephone);
			}
		}
		
	}

	@Override
	public void supprimer(int idPersonne) {

		Connection			connection	= null;
		PreparedStatement	stmt		= null;

		try {
			
			// Supprime les enregistrements dans la table fille
			Personne personne = retrouver( idPersonne );
			for ( Telephone telephone : personne.getTelephones() ) {
				daoTelephone.supprimer( telephone.getId() );
			}
			
			// Supprime l'enregistrement dans la table principale
			try {
				connection = dataSource.getConnection();
				String sql = "DELETE FROM personne WHERE IdPersonne = ? ";
				stmt = connection.prepareStatement(sql);
				stmt.setInt( 1, idPersonne );
				stmt.executeUpdate();
			} finally {
				if (stmt != null) stmt.close();
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Personne retrouver(int idPersonne) {

		Personne 			personne 	= null;

		Connection			connection	= null;
		PreparedStatement	stmt		= null;
		ResultSet 			rs 			= null;

		try {
	        try {
				connection = dataSource.getConnection();
	            String sql = "SELECT * FROM personne WHERE IdPersonne = ?";
	            stmt = connection.prepareStatement(sql);
	            stmt.setInt(1, idPersonne);
	            rs = stmt.executeQuery();
	
	            if ( rs.next() ) {
	                personne = construirePersonne(rs);
	            }
			} finally {
				try {
					if (rs != null) rs.close();
				} finally {
					if (stmt != null) stmt.close();
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

        return personne;
	}

	@Override
	public List<Personne> listerTout() {

		List<Personne> 		personnes 	= new ArrayList<>();

		Connection			connection	= null;
		PreparedStatement	stmt		= null;
		ResultSet 			rs 			= null;

		try {
			try {
				connection = dataSource.getConnection();
				String sql = "SELECT * FROM personne ORDER BY Nom, Prenom";
				stmt = connection.prepareStatement(sql);
				rs = stmt.executeQuery();
	
				while (rs.next()) {
					personnes.add( construirePersonne(rs) );
				}
			} finally {
				try {
					if (rs != null) rs.close();
				} finally {
					if (stmt != null) stmt.close();
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

		return personnes;
	}

    
    @Override
    public int compterPersonnes(int idCategorie) {

    	int 				nbPersonnes = 0;
    	
		Connection			connection 	= null;
		PreparedStatement	stmt 		= null;
		ResultSet 			rs			= null;

		try {
	        try {
				connection = dataSource.getConnection();
	            String sql = "SELECT COUNT(*) FROM personne WHERE IdCategorie = ?";
	            stmt = connection.prepareStatement( sql );
	            stmt.setInt( 1, idCategorie );
	            rs = stmt.executeQuery();
	
	            if ( rs.next() ) {
	                nbPersonnes = rs.getInt( 1 );
	            }
	        } finally {
	            try {
	                if (rs != null) rs.close();
				} finally {
					if (stmt != null) stmt.close();
	            }
	        }
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

        return nbPersonnes;
    }
	
	
	// Méthodes auxiliaires
	
	private Personne construirePersonne( ResultSet rs ) throws SQLException {
		Personne personne = new Personne();
		personne.setId(rs.getInt("IdPersonne"));
		personne.setNom(rs.getString("Nom"));
		personne.setPrenom(rs.getString("Prenom"));
		personne.setCategorie( daoCategorie.retrouver( rs.getInt("IdCategorie")) );
		personne.setTelephones( daoTelephone.listerPourPersonne(personne));
		return personne;
	}

}
