package contacts.emb.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import contacts.emb.dao.IDaoCategorie;
import contacts.emb.dom.Categorie;


public class DaoCategorie implements IDaoCategorie {

	
	// Champs

	private DataSource		dataSource;

	
	// Injecteurs
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	
	// Actions

	@Override
	public int inserer(Categorie categorie) {

		Connection			connection = null;
		PreparedStatement	stmt = null;
		ResultSet 			rs = null;

		try {
			try {
				connection = dataSource.getConnection();
				String sql = "INSERT INTO categorie ( Libelle ) VALUES ( ? )";
				stmt = connection.prepareStatement(sql,
						new String[] { "ICategorie" } );
				stmt.setString(	1, categorie.getLibelle() );
				stmt.executeUpdate();
	
				rs = stmt.getGeneratedKeys();
				rs.next();
				int id = rs.getInt(1);
				categorie.setId( id );
				return id;
	
			} finally {
				try {
					if (rs != null) rs.close();
				} finally {
					if (stmt != null) stmt.close();
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void modifier(Categorie categorie) {

		Connection			connection = null;
		PreparedStatement	stmt = null;

		try {
			try {
				connection = dataSource.getConnection();
				String sql = "UPDATE categorie SET Libelle = ? WHERE IdCategorie =  ?";
				stmt = connection.prepareStatement( sql );
				stmt.setString(	1, categorie.getLibelle() );
				stmt.setInt(	2, categorie.getId() );
				stmt.executeUpdate();
	
			} finally {
				if (stmt != null) stmt.close();
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void supprimer(int idCategorie) {

		Connection			connection = null;
		PreparedStatement	stmt = null;

		try {
			try {
				connection = dataSource.getConnection();
				String sql = "DELETE FROM categorie WHERE IdCategorie = ? ";
				stmt = connection.prepareStatement(sql);
				stmt.setInt( 1, idCategorie );
				stmt.executeUpdate();
			} finally {
				if (stmt != null) stmt.close();
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Categorie retrouver(int idCategorie) {

		Categorie 			categorie	= null;
		
		Connection			connection 	= null;
		PreparedStatement	stmt 		= null;
		ResultSet 			rs			= null;

		try {
			try {
				connection = dataSource.getConnection();
				String sql = "SELECT * FROM categorie WHERE IdCategorie = ?";
				stmt = connection.prepareStatement(sql);
				stmt.setInt(1, idCategorie);
				rs = stmt.executeQuery();

				if ( rs.next() ) {
					categorie = construireCategorie(rs);
				}
			} finally {
				try {
					if (rs != null) rs.close();
				} finally {
					if (stmt != null) stmt.close();
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

			return categorie;
	}

	@Override
	public List<Categorie> listerTout() {

		List<Categorie> 	categories	= new ArrayList<>();
		
		Connection			connection 	= null;
		PreparedStatement	stmt 		= null;
		ResultSet 			rs			= null;

		try {
			try {
				connection = dataSource.getConnection();
				String sql = "SELECT * FROM categorie ORDER BY Libelle";
				stmt = connection.prepareStatement(sql);
				rs = stmt.executeQuery();
	
				while (rs.next()) {
					categories.add( construireCategorie(rs) );
				}
			} finally {
				try {
					if (rs != null) rs.close();
				} finally {
					if (stmt != null) stmt.close();
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

		return categories;
	}
	
	
	// Méthodes auxiliaires
	
	private Categorie construireCategorie( ResultSet rs ) throws SQLException {
		Categorie categorie = new Categorie();
		categorie.setId(rs.getInt("IdCategorie"));
		categorie.setLibelle(rs.getString("Libelle"));
		return categorie;
	}

}
