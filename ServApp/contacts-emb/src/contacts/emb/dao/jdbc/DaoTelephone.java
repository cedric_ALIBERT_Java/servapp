package contacts.emb.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import contacts.emb.dao.IDaoTelephone;
import contacts.emb.dom.Personne;
import contacts.emb.dom.Telephone;


public class DaoTelephone implements IDaoTelephone {

	
	// Champs

	private DataSource		dataSource;

	
	// Injecteurs
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	
	// Actions

	@Override
	public int inserer(Telephone telephone) {

		Connection			connection	= null;
		PreparedStatement	stmt		= null;
		ResultSet 			rs 			= null;

		try {
			try {
				connection = dataSource.getConnection();
				String sql = "INSERT INTO telephone ( Libelle, Numero, IdPersonne ) VALUES ( ?, ?, ? )";
				stmt = connection.prepareStatement(sql,
						new String[] { "ITelephone" } );
				stmt.setString(	1, telephone.getLibelle() );
				stmt.setString(	2, telephone.getNumero() );
				stmt.setInt(	3, telephone.getPersonne().getId() );
				stmt.executeUpdate();
	
				rs = stmt.getGeneratedKeys();
				rs.next();
				int id = rs.getInt(1);
				telephone.setId( id );
				return id;
	
			} finally {
				try {
					if (rs != null) rs.close();
				} finally {
					if (stmt != null) stmt.close();
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void modifier(Telephone telephone) {

		Connection			connection	= null;
		PreparedStatement	stmt		= null;

		try {
			try {
				connection = dataSource.getConnection();
				String sql = "UPDATE telephone SET Libelle = ?, Numero = ?, IdPersonne = ? WHERE IdTelephone =  ?";
				stmt = connection.prepareStatement( sql );
				stmt.setString(	1, telephone.getLibelle() );
				stmt.setString(	2, telephone.getNumero() );
				stmt.setInt(	3, telephone.getPersonne().getId() );
				stmt.setInt(	4, telephone.getId() );
				stmt.executeUpdate();
	
			} finally {
				if (stmt != null) stmt.close();
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void supprimer(int idTelephone) {

		Connection			connection	= null;
		PreparedStatement	stmt		= null;

		try {
			try {
				connection = dataSource.getConnection();
				String sql = "DELETE FROM telephone WHERE IdTelephone = ? ";
				stmt = connection.prepareStatement(sql);
				stmt.setInt( 1, idTelephone );
				stmt.executeUpdate();
			} finally {
				if (stmt != null) stmt.close();
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	
	@Override
	public List<Telephone> listerPourPersonne( Personne personne ) {

		List<Telephone> 	telephones	= new ArrayList<>();

		Connection			connection	= null;
		PreparedStatement	stmt		= null;
		ResultSet 			rs 			= null;
		
		try {
			try {
				connection = dataSource.getConnection();
				String sql = "SELECT *"
						+ " FROM telephone"
						+ " WHERE IdPersonne = ?"
						+ " ORDER BY Libelle";
				stmt = connection.prepareStatement(sql);
				stmt.setInt( 1, personne.getId() ); 
				rs = stmt.executeQuery();
	
				while (rs.next()) {
					Telephone telephone = new Telephone();
					telephone.setId(rs.getInt("IdTelephone"));
					telephone.setLibelle(rs.getString("Libelle"));
					telephone.setNumero(rs.getString("Numero"));
					telephone.setPersonne(personne);
					telephones.add( telephone );
				}
			} finally {
				try {
					if (rs != null) rs.close();
				} finally {
					if (stmt != null) stmt.close();
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

		return telephones;
		
	}
	
	

}
