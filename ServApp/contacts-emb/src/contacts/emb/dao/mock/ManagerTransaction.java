package contacts.emb.dao.mock;

import java.util.logging.Logger;

import contacts.emb.dao.IManagerTransaction;


public class ManagerTransaction implements IManagerTransaction {
	
	
	// Logger
	private static final Logger logger = Logger.getLogger(ManagerTransaction.class.getName());
	
	
	
	// Actions

	@Override
	public void transactionBegin() {
		logger.finer("Transaction BEGIN");
	}

	@Override
	public void transactionCommit() {
		logger.finer("Transaction COMMIT");
	}

	@Override
	public void transactionRollback() {
		logger.finer("Transaction ROLLBACK");
	}

}
