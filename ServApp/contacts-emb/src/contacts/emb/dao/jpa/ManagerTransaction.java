package contacts.emb.dao.jpa;

import java.util.logging.Logger;

import javax.persistence.EntityManager;

import contacts.emb.dao.IManagerTransaction;


public class ManagerTransaction implements IManagerTransaction {
	
	
	// Logger
	private static final Logger logger = Logger.getLogger(ManagerTransaction.class.getName());
	
	private EntityManager em;
	
	//Injecteurs
	public void setEntityManager( EntityManager em ) {
		this.em = em;
	}
	
	// Actions

	@Override
	public void transactionBegin() {
		em.getTransaction().begin();
		logger.finer("Transaction BEGIN");
		
	}

	@Override
	public void transactionCommit() {
		em.getTransaction().commit();
		logger.finer("Transaction COMMIT");
	}

	@Override
	public void transactionRollback() {
		if( em.getTransaction().isActive() ) {
			em.getTransaction().rollback() ;
			logger.finer("Transaction ROLLBACK");
		}
		
	}

}
