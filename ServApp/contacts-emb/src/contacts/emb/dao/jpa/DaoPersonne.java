package contacts.emb.dao.jpa;



import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import contacts.emb.dao.IDaoPersonne;
import contacts.emb.dom.Personne;



public class DaoPersonne implements IDaoPersonne {
	
	// Champs
	

	private EntityManager em;
	
	// Injecteurs
	
	public void setEntityManager( EntityManager em ) {
		this.em = em;
	}
	


	
	// Actions
	
	@Override
	public int inserer(Personne personne) {
		em.persist( personne ) ;
		em.flush();
		return personne.getId();
	}

	@Override
	public void modifier(Personne personne) {
		em.merge(personne);
	}

	@Override
	public void supprimer(int idPersonne) {
		em.remove(retrouver(idPersonne));
	}

	@Override
	public Personne retrouver(int idPersonne) {
		return em.find( Personne.class, idPersonne );
	}

	@Override
	public List<Personne> listerTout() {
		String jpql="SELECT p FROM Personne p ORDER BY p.nom, p.prenom";
		TypedQuery<Personne> query = em.createQuery( jpql, Personne.class) ;
		List<Personne> personnes = query.getResultList() ;
		return personnes;
	}


	@Override
	public int compterPersonnes(int idCategorie) {
		
		String jpql="SELECT count(p) FROM Personne p WHERE p.categorie.id = :idCategorie";
		TypedQuery<Long> query = em.createQuery( jpql, Long.class) ;
		query.setParameter( "idCategorie", idCategorie ) ;
		return query.getSingleResult().intValue() ;
		
	}
	
	
	
}
