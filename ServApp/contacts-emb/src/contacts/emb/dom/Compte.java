package contacts.emb.dom;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

@Entity
@Table( name="Compte" )
public class Compte  {

	
	// Champs
	@Id
	@GeneratedValue( strategy=GenerationType.IDENTITY )
	@Column( name="IdCompte" )
	private int			id;
	@Column( name="Pseudo" )
	private String		pseudo;
	@Column( name="MotDePasse" )
	private String		motDePasse;
	@Column( name="Email" )
	private String		email;
	@ElementCollection
	@CollectionTable( name="role", joinColumns={ @JoinColumn(name="IdCompte")})
	@Column(name="Role")
	private List<String> roles = new ArrayList<>();	
	
		
	// Getters & setters
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public String getMotDePasse() {
		return motDePasse;
	}

	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	
	
	// Constructeurs
	
	public Compte() {
		super();
	}

	public Compte(int id, String pseudo, String motDePasse, String email) {
		super();
		this.id = id;
		this.pseudo = pseudo;
		this.motDePasse = motDePasse;
		this.email = email;
	}
	
}
