package contacts.emb.util.mapper;

import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import contacts.commun.dto.DtoCategorie;
import contacts.commun.dto.DtoCompte;
import contacts.commun.dto.DtoPersonne;
import contacts.commun.dto.DtoTelephone;
import contacts.emb.dom.Categorie;
import contacts.emb.dom.Compte;
import contacts.emb.dom.Personne;
import contacts.emb.dom.Telephone;

 
public interface IMapperDoDto {  
	
	static final IMapperDoDto INSTANCE = Mappers.getMapper( IMapperDoDto.class );
	
	
	Compte map( DtoCompte source );
	
	DtoCompte map( Compte source );
	
	
	Categorie map( DtoCategorie source );
	
	DtoCategorie map( Categorie source );
	
	
	Personne map( DtoPersonne source );
	
	DtoPersonne map( Personne source );

	@Mapping( target="personne", ignore=true )
	Telephone map( DtoTelephone source );
	
	DtoTelephone map( Telephone source );
	
}
