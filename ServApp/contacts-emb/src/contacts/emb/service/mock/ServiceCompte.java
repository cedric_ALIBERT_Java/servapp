package contacts.emb.service.mock;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import contacts.commun.dto.DtoCompte;
import contacts.commun.service.IServiceCompte;
import contacts.commun.util.ExceptionAnomalie;
import contacts.commun.util.ExceptionAppli;
import contacts.commun.util.ExceptionValidation;
import contacts.emb.util.securite.IManagerSecurite;


public class ServiceCompte implements IServiceCompte {

    
    // Logger
    
    private static final Logger logger = Logger.getLogger(ServiceCompte.class.getName());

	
    // Champs 

    private  Map<Integer, DtoCompte>  mapComptes;
	
	private IManagerSecurite	managerSecurite;
	
	
	// Injecteurs
	
	public void setDonnees( Donnees donnees ) {
		mapComptes = donnees.getMapComptes();
	}
	
	public void setManagerSecurite(IManagerSecurite managerSecurite) {
		this.managerSecurite = managerSecurite;
	}

    
    // Actions 
    
    @Override
    public int inserer(DtoCompte compte) throws ExceptionAppli {

        try {
            managerSecurite.verifierAutorisationAdmin();
            compte.setId(0);
            verifierValiditeDonnees(compte);
            if (mapComptes.isEmpty()) {
                compte.setId(1);
            } else {
                compte.setId(Collections.max(mapComptes.keySet()) + 1);
            }
            mapComptes.put(compte.getId(), compte);
            return compte.getId();
        } catch (RuntimeException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
            throw new ExceptionAnomalie(e);
        }
    }

    @Override
    public void modifier(DtoCompte compte) throws ExceptionAppli {
        try {
            managerSecurite.verifierAutorisationAdmin();
            verifierValiditeDonnees(compte);
            mapComptes.replace(compte.getId(), compte);
        } catch (RuntimeException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
            throw new ExceptionAnomalie(e);
        }
    }

    @Override
    public void supprimer(int idCompte) throws ExceptionAppli {
        try {
            managerSecurite.verifierAutorisationAdmin();
            if (managerSecurite.getIdCompteConnecte() == idCompte) {
                throw new ExceptionValidation("Vous ne pouvez pas supprimer le compte avec lequel vous êtes connecté !");
            }

            mapComptes.remove(idCompte);
        } catch (RuntimeException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
            throw new ExceptionAnomalie(e);
        }
    }

    @Override
    public DtoCompte retrouver(int idCompte) throws ExceptionAppli {
        try {
            managerSecurite.verifierAutorisationAdmin();
            return mapComptes.get(idCompte);
        } catch (RuntimeException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
            throw new ExceptionAnomalie(e);
        }
    }

    @Override
    public List<DtoCompte> listerTout() throws ExceptionAppli {
        try {
            managerSecurite.verifierAutorisationAdmin();
            return trierParPseudo( new ArrayList<>(mapComptes.values()) );
        } catch (RuntimeException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
            throw new ExceptionAnomalie(e);
        }
    }

    
    // Méthodes auxiliaires
    
    private void verifierValiditeDonnees(DtoCompte dto) throws ExceptionAppli {

        StringBuilder message = new StringBuilder();

        if (dto.getPseudo() == null || dto.getPseudo().isEmpty()) {
            message.append("\nLe pseudo est absent.");
        } else if (dto.getPseudo().length() < 3) {
            message.append("\nLe pseudo est trop court.");
        } else if (dto.getPseudo().length() > 25) {
            message.append("\nLe pseudo est trop long.");
        } else {
            boolean pseudoDejaPresent = false;
            for (DtoCompte c : mapComptes.values()) {
                if (c.getPseudo().equals(dto.getPseudo()) && c.getId() != dto.getId()) {
                    pseudoDejaPresent = true;
                    break;
                }
            }
            if (pseudoDejaPresent) {
                message.append("\nLe pseudo " + dto.getPseudo() + " est déjà utilisé.");
            }
        }

        if (dto.getMotDePasse() == null || dto.getMotDePasse().isEmpty()) {
            message.append("\nLe mot de passe est absent.");
        } else if (dto.getMotDePasse().length() < 3) {
            message.append("\nLe mot de passe est trop court.");
        } else if (dto.getMotDePasse().length() > 25) {
            message.append("\nLe mot de passe est trop long.");
        }

        if (dto.getEmail() == null || dto.getEmail().isEmpty()) {
            message.append("\nL'adresse e-mail est absente.");
        }

        if (message.length() > 0) {
            throw new ExceptionValidation(message.toString().substring(1));
        }
    }

    
    private List<DtoCompte> trierParPseudo( List<DtoCompte> liste ) {
		Collections.sort( liste,
	            (Comparator<DtoCompte>) ( item1, item2) -> {
	                return ( item1.getPseudo().toUpperCase().compareTo( item2.getPseudo().toUpperCase() ) );
			});
    	return liste;
    }

}
