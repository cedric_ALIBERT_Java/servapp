package contacts.emb.service.mock;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import contacts.commun.dto.DtoPersonne;
import contacts.commun.dto.DtoTelephone;
import contacts.commun.service.IServicePersonne;
import contacts.commun.util.ExceptionAnomalie;
import contacts.commun.util.ExceptionAppli;
import contacts.commun.util.ExceptionValidation;
import contacts.emb.util.securite.IManagerSecurite;


public class ServicePersonne implements IServicePersonne {


	// Logger
	private static final Logger logger = Logger.getLogger( ServicePersonne.class.getName() );

	
	// Champs 

	private Donnees						donnees;
	private Map<Integer, DtoPersonne>	mapPersonnes;
	
	private IManagerSecurite	managerSecurite;
	
	
	// Injecteurs
	
	public void setDonnees( Donnees donnees ) {
		this.donnees = donnees;
		mapPersonnes = donnees.getMapPersonnes();
	}
	
	public void setManagerSecurite(IManagerSecurite managerSecurite) {
		this.managerSecurite = managerSecurite;
	}
	

	// Actions 

	@Override
	public int inserer(DtoPersonne personne) throws ExceptionAppli {
		try {
			managerSecurite.verifierAutorisationUtilisateur();
			verifierValiditeDonnees( personne );
			if ( mapPersonnes.isEmpty() ) {
				personne.setId( 1 );
			} else {
				personne.setId( Collections.max( mapPersonnes.keySet() ) + 1 );
			}
        	affecterIdTelephones(personne);
			mapPersonnes.put( personne.getId(), personne );
			return personne.getId();
		} catch (RuntimeException e) {
			logger.log( Level.SEVERE, e.getMessage(), e );
			throw new ExceptionAnomalie(e);
		}
	}

	@Override
	public void modifier(DtoPersonne personne) throws ExceptionAppli {
		try {
			managerSecurite.verifierAutorisationUtilisateur();
			verifierValiditeDonnees( personne );
        	affecterIdTelephones(personne);
			mapPersonnes.replace( personne.getId(), personne );
		} catch (RuntimeException e) {
			logger.log( Level.SEVERE, e.getMessage(), e );
			throw new ExceptionAnomalie(e);
		}
	}

	@Override
	public void supprimer(int idPersonne) throws ExceptionAppli  {
		try {
			managerSecurite.verifierAutorisationUtilisateur();
			mapPersonnes.remove( idPersonne );
		} catch (RuntimeException e) {
			logger.log( Level.SEVERE, e.getMessage(), e );
			throw new ExceptionAnomalie(e);
		}
	}

	@Override
	public DtoPersonne retrouver(int idPersonne) throws ExceptionAppli {
		try {
			managerSecurite.verifierAutorisationUtilisateur();
			DtoPersonne personne = mapPersonnes.get( idPersonne );
			return personne;
		} catch (RuntimeException e) {
			logger.log( Level.SEVERE, e.getMessage(), e );
			throw new ExceptionAnomalie(e);
		}
	}

	@Override
	public List<DtoPersonne> listerTout() throws ExceptionAppli {
		try {
			managerSecurite.verifierAutorisationUtilisateur();
			return trierParNom( new ArrayList<>(mapPersonnes.values()) );
		} catch (RuntimeException e) {
			logger.log( Level.SEVERE, e.getMessage(), e );
			throw new ExceptionAnomalie(e);
		}
	}
	
	
	// Méthodes auxiliaires
	

	private void verifierValiditeDonnees( DtoPersonne personne ) throws ExceptionAppli {
		
		StringBuilder message = new StringBuilder();
		
		if ( personne.getNom() == null || personne.getNom().isEmpty() ) {
			message.append( "\nLe nom est absent." );
		} else 	if ( personne.getNom().length() > 25 ) {
			message.append( "\nLe nom est trop long." );
		}

		if ( personne.getPrenom() == null || personne.getPrenom().isEmpty() ) {
			message.append( "\nLe prénom est absent." );
		} else 	if ( personne.getPrenom().length() > 25 ) {
			message.append( "\nLe prénom est trop long." );
		}
		
		for( DtoTelephone telephoe : personne.getTelephones() ) {
			if ( telephoe.getLibelle() == null || telephoe.getLibelle().isEmpty() ) {
				message.append( "\nLlibellé absent pour le téléphone : " + telephoe.getNumero() );
			} else 	if ( telephoe.getLibelle().length() > 25 ) {
				message.append( "\nLe libellé du téléphone est trop lon : " + telephoe.getLibelle() );
			}
			
			if ( telephoe.getNumero() == null || telephoe.getNumero().isEmpty() ) {
				message.append( "\nNuméro absent pour le téléphone : " + telephoe.getLibelle() );
			} else 	if ( telephoe.getNumero().length() > 25 ) {
				message.append( "\nLe numéro du téléphone est trop lon : " + telephoe.getNumero() );
			}
		}
		
		if ( message.length() > 0 ) {
			throw new ExceptionValidation( message.toString().substring(1) );
		}
	}
    
	private void affecterIdTelephones( DtoPersonne personne ) {
		for( DtoTelephone t : personne.getTelephones() ) {
			if ( t.getId() == 0 ) {
				t.setId( donnees.getProchainIdTelephone() );
			}
		}
	}
    
    private List<DtoPersonne> trierParNom( List<DtoPersonne> liste ) {
		Collections.sort( liste,
	            (Comparator<DtoPersonne>) ( item1, item2) -> {
	            	int resultat = item1.getNom().toUpperCase().compareTo( item2.getNom().toUpperCase() );
	            	if ( resultat  != 0 ) {
	            		return resultat;
	            	} else {
		                return ( item1.getPrenom().toUpperCase().compareTo( item2.getPrenom().toUpperCase() ) );
	            	}
		});
    	return liste;
    }
	
}
