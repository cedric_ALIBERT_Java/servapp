package contacts.ejb.service.standard;

import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import contacts.commun.dto.DtoCompte;
import contacts.commun.service.IServiceAuthentification;
import contacts.commun.util.ExceptionAnomalie;
import contacts.commun.util.ExceptionAppli;
import contacts.ejb.dao.IDaoCompte;
import contacts.ejb.util.mapper.IMapperDoDto;


@Stateless(mappedName="ejb/contacts/ServiceAuthentification")
@Remote(IServiceAuthentification.class)
public class ServiceAuthentification implements IServiceAuthentification {

	
	// Logger
//	private static final Logger	logger = Logger.getLogger(ServiceAuthentification.class.getName());

	
	// Champs 

	private IMapperDoDto		mapper = IMapperDoDto.INSTANCE;
	@EJB
	private IDaoCompte			daoCompte;
	
	
	// Injecteurs
	
//	public void setMapper( IMapperDoDto mapper ) {
//		this.mapper = mapper = IMapperDoDto.INSTANCE;
//	}

//	public void setDaoCompte(IDaoCompte daoCompte) {
//		this.daoCompte = daoCompte;
//	}

	
	// Actions
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public DtoCompte verifierAuthentification(String pseudo, String motDePasse) throws ExceptionAppli {
		
		DtoCompte	compteConnecte = null;

//		try {
			compteConnecte = mapper.map( daoCompte.validerAuthentification(pseudo, motDePasse) );
			return compteConnecte;
//		} catch ( RuntimeException e) {
//			logger.log( Level.SEVERE, e.getMessage(), e );
//			throw new ExceptionAnomalie(e);
//		}
	}

}
