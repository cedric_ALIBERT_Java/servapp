package contacts.ejb.service.standard;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import contacts.commun.dto.DtoCompte;
import contacts.commun.service.IServiceCategorie;
import contacts.commun.service.IServiceCompte;
import contacts.commun.util.ExceptionAnomalie;
import contacts.commun.util.ExceptionAppli;
import contacts.commun.util.ExceptionValidation;
import contacts.ejb.dao.IDaoCompte;
//import contacts.ejb.dao.IManagerTransaction;
import contacts.ejb.dom.Compte;
import contacts.ejb.util.mapper.IMapperDoDto;
//import contacts.ejb.util.securite.IManagerSecurite;

@Stateless(mappedName="ejb/contacts/ServiceCompte")
@Remote(IServiceCompte.class)
public class ServiceCompte implements IServiceCompte {

	
	// Logger
//	private static final Logger	logger = Logger.getLogger(ServiceCompte.class.getName());

	
	// Champs 

//	private IManagerSecurite		managerSecurite;
//    private	IManagerTransaction		managerTransaction;
	private IMapperDoDto			mapper = IMapperDoDto.INSTANCE;
	@EJB
	private IDaoCompte				daoCompte;
	
	
	// Injecteurs
	
//	public void setManagerSecurite(IManagerSecurite managerSecurite) {
//		this.managerSecurite = managerSecurite;
//	}
	
//	public void setManagerTransaction(IManagerTransaction managerTransaction) {
//		this.managerTransaction = managerTransaction;
//	}
	
//	public void setMapper( IMapperDoDto mapper ) {
//		this.mapper = mapper = IMapperDoDto.INSTANCE;
//	}

//	public void setDaoCompte(IDaoCompte daoCompte) {
//		this.daoCompte = daoCompte;
//	}

	
	// Actions 

	@Override
	public int inserer(DtoCompte dto) throws ExceptionAppli {
		
//		try {
			
//--->//--->	managerSecurite.verifierAutorisationAdmin();
			dto.setId(0);
			verifierValiditeDonnees( dto );

//			managerTransaction.transactionBegin();
//			try {
				Compte compte = mapper.map( dto );
				int id = daoCompte.inserer( compte );
//				managerTransaction.transactionCommit();
				return id;
//			} catch (Exception e) {
//				managerTransaction.transactionRollback();
//				throw e;
//			}

//		} catch (RuntimeException e) {
//			logger.log( Level.SEVERE, e.getMessage(), e );
//			throw new ExceptionAnomalie(e);
//		}
	}

	@Override
	public void modifier(DtoCompte dto) throws ExceptionAppli { 
//		try {

//--->//--->	managerSecurite.verifierAutorisationAdmin();
			verifierValiditeDonnees( dto );

//			managerTransaction.transactionBegin();
//			try {
				Compte compte = mapper.map( dto );
				daoCompte.modifier( compte );
//				managerTransaction.transactionCommit();
//			} catch (Exception e) {
//				managerTransaction.transactionRollback();
//				throw e;
//			}

//		} catch (RuntimeException e) {
//			logger.log( Level.SEVERE, e.getMessage(), e );
//			throw new ExceptionAnomalie(e);
//		}
		
	}

	@Override
	public void supprimer(int idCompte) throws ExceptionAppli  {
//		try {
			
//--->//--->	managerSecurite.verifierAutorisationAdmin();
//--->//--->	if ( managerSecurite.getIdCompteConnecte() == idCompte ) {
//--->//--->		throw new ExceptionValidation("Vous ne pouvez pas supprimer le compte avec lequel vous êtes connecté !");
//--->//--->	}

//			managerTransaction.transactionBegin();
//			try {
				daoCompte.supprimer(idCompte);
//				managerTransaction.transactionCommit();
//			} catch (Exception e) {
//				managerTransaction.transactionRollback();
//				throw e;
//			}

//		} catch (RuntimeException e) {
//			logger.log( Level.SEVERE, e.getMessage(), e );
//			throw new ExceptionAnomalie(e);
//		}
	}

	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public DtoCompte retrouver(int idCompte) throws ExceptionAppli {
//		try {
			
//--->//--->	managerSecurite.verifierAutorisationAdmin();
			return mapper.map( daoCompte.retrouver(idCompte) );

//		} catch (RuntimeException e) {
//			logger.log( Level.SEVERE, e.getMessage(), e );
//			throw new ExceptionAnomalie(e);
//		}
	}

	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<DtoCompte> listerTout() throws ExceptionAppli {
//		try {

//--->//--->	managerSecurite.verifierAutorisationAdmin();
			List<DtoCompte> liste = new ArrayList<>();
			for( Compte compte : daoCompte.listerTout() ) {
				liste.add( mapper.map( compte ) );
			}
			return liste;

//		} catch (RuntimeException e) {
//			logger.log( Level.SEVERE, e.getMessage(), e );
//			throw new ExceptionAnomalie(e);
//		}
	}
	
	
	// Méthodes auxiliaires
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	private void verifierValiditeDonnees( DtoCompte dto ) throws ExceptionAppli {
		
		StringBuilder message = new StringBuilder();
		
		if ( dto.getPseudo() == null || dto.getPseudo().isEmpty() ) {
			message.append( "\nLe pseudo est absent." );
		} else 	if ( dto.getPseudo().length() < 3 ) {
			message.append( "\nLe pseudo est trop court." );
		} else 	if ( dto.getPseudo().length() > 25 ) {
			message.append( "\nLe pseudo est trop long." );
		} else 	if ( ! daoCompte.verifierUnicitePseudo( dto.getPseudo(), dto.getId() ) ) {
			message.append( "\nLe pseudo " + dto.getPseudo() + " est déjà utilisé." );
		}
		
		if ( dto.getMotDePasse() == null || dto.getMotDePasse().isEmpty() ) {
			message.append( "\nLe mot de passe est absent." );
		} else 	if ( dto.getMotDePasse().length() < 3 ) {
			message.append( "\nLe mot de passe est trop court." );
		} else 	if ( dto.getMotDePasse().length() > 25 ) {
			message.append( "\nLe mot de passe est trop long." );
		}
		
		if ( dto.getEmail() == null || dto.getEmail().isEmpty() ) {
			message.append( "\nL'adresse e-mail est absente." );
		}
		
		if ( message.length() > 0 ) {
			throw new ExceptionValidation( message.toString().substring(1) );
		}
	}
	
}
