package contacts.ejb.service.standard;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import contacts.commun.dto.DtoCategorie;
import contacts.commun.service.IServiceAuthentification;
import contacts.commun.service.IServiceCategorie;
import contacts.commun.util.ExceptionAnomalie;
import contacts.commun.util.ExceptionAppli;
import contacts.commun.util.ExceptionValidation;
import contacts.ejb.dao.IDaoCategorie;
import contacts.ejb.dao.IDaoPersonne;
//import contacts.ejb.dao.IManagerTransaction;
import contacts.ejb.dom.Categorie;
import contacts.ejb.util.mapper.IMapperDoDto;
//import contacts.ejb.util.securite.IManagerSecurite;

@Stateless(mappedName="ejb/contacts/ServiceCategorie")
@Remote(IServiceCategorie.class)
public class ServiceCategorie implements IServiceCategorie {


	// Logger
//	private static final Logger logger = Logger.getLogger( ServiceCategorie.class.getName() );

	
	// Champs 

//	private IManagerSecurite		managerSecurite;
//    private	IManagerTransaction		managerTransaction;
	
	private IMapperDoDto			mapper = IMapperDoDto.INSTANCE;
	@EJB
	private IDaoCategorie			daoCategorie;
	@EJB
	private IDaoPersonne			daoPersonne;
	
	
	// Injecteurs
	
//	public void setManagerSecurite(IManagerSecurite managerSecurite) {
//		this.managerSecurite = managerSecurite;
//	}
	
//	public void setManagerTransaction(IManagerTransaction managerTransaction) {
//		this.managerTransaction = managerTransaction;
//	}
	
//	public void setMapper( IMapperDoDto mapper ) {
//		this.mapper = mapper = IMapperDoDto.INSTANCE;
//	}
	
//	public void setDaoCategorie(IDaoCategorie daoCategorie) {
//		this.daoCategorie = daoCategorie;
//	}
	
//	public void setDaoPersonne(IDaoPersonne daoPersonne) {
//		this.daoPersonne = daoPersonne;
//	}
	

	// Actions 

	@Override
	public int inserer(DtoCategorie dto) throws ExceptionAppli {
//		try {
			
//--->//--->	managerSecurite.verifierAutorisationAdmin();
			verifierValiditeDonnees( dto );
	
//			managerTransaction.transactionBegin();
//			try {
				Categorie categorie = mapper.map( dto );
				int id = daoCategorie.inserer( categorie );
//				managerTransaction.transactionCommit();
				return id;
//			} catch (Exception e) {
//				managerTransaction.transactionRollback();
//				throw e;
//			}
	
//		} catch (RuntimeException e) {
//			logger.log( Level.SEVERE, e.getMessage(), e );
//			throw new ExceptionAnomalie(e);
//		}
	}

	@Override
	public void modifier(DtoCategorie dto) throws ExceptionAppli {
//		try  {
		
//--->//--->	managerSecurite.verifierAutorisationAdmin();
			verifierValiditeDonnees( dto );
	
//			managerTransaction.transactionBegin();
//			try {
				daoCategorie.modifier( mapper.map( dto ) );
//				managerTransaction.transactionCommit();
//			} catch (Exception e) {
//				managerTransaction.transactionRollback();
//				throw e;
//			}
	
//		} catch (RuntimeException e) {
//			logger.log( Level.SEVERE, e.getMessage(), e );
//			throw new ExceptionAnomalie(e);
//		}
	}

	@Override
	public void supprimer(int idCategorie) throws ExceptionAppli {
//		try {
			
//--->//--->	managerSecurite.verifierAutorisationAdmin();

            if ( daoPersonne.compterPersonnes(idCategorie) != 0 ) {
                throw new ExceptionValidation( "La catégorie n'est pas vide" );
            }

//			managerTransaction.transactionBegin();
//			try {
				daoCategorie.supprimer(idCategorie);
//				managerTransaction.transactionCommit();
//			} catch (Exception e) {
//				managerTransaction.transactionRollback();
//				throw e;
//			}

//		} catch (RuntimeException e) {
//			logger.log( Level.SEVERE, e.getMessage(), e );
//			throw new ExceptionAnomalie(e);
//		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public DtoCategorie retrouver(int idCategorie) throws ExceptionAppli {
//		try {
			
//--->//--->	managerSecurite.verifierAutorisationUtilisateur();
			return mapper.map( daoCategorie.retrouver(idCategorie) );

//		} catch (RuntimeException e) {
//			logger.log( Level.SEVERE, e.getMessage(), e );
//			throw new ExceptionAnomalie(e);
//		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<DtoCategorie> listerTout() throws ExceptionAppli {
//		try {

//--->//--->	managerSecurite.verifierAutorisationUtilisateur();
			List<DtoCategorie> liste = new ArrayList<>();
			for( Categorie categorie : daoCategorie.listerTout() ) {
				liste.add( mapper.map( categorie ) );
			}
			return liste;

//		} catch (RuntimeException e) {
//			logger.log( Level.SEVERE, e.getMessage(), e );
//			throw new ExceptionAnomalie(e);
//		}
	}

	
	
	// Méthodes auxiliaires
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	private void verifierValiditeDonnees( DtoCategorie dto ) throws ExceptionAppli {
		
		StringBuilder message = new StringBuilder();
		
		if ( dto.getLibelle() == null || dto.getLibelle().isEmpty() ) {
			message.append( "\nLe libellé est absent." );
		} else 	if ( dto.getLibelle().length() < 3 ) {
			message.append( "\nLe libellé est trop court." );
		} else 	if ( dto.getLibelle().length() > 25 ) {
			message.append( "\nLe libellé est trop long." );
		}
		
		if ( message.length() > 0 ) {
			throw new ExceptionValidation( message.toString().substring(1) );
		}
	}

}
