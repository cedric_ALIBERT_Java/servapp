package contacts.ejb.dao;

import java.util.List;

import contacts.ejb.dom.Personne;
import contacts.ejb.dom.Telephone;


public interface IDaoTelephone {

	int 		inserer( Telephone telephone );

	void 		modifier( Telephone telephone );

	void 		supprimer( int idTelephone );

	List<Telephone> listerPourPersonne( Personne personne );

}
