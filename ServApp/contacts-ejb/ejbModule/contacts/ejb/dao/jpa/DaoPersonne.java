package contacts.ejb.dao.jpa;



import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import contacts.ejb.dao.IDaoCompte;
import contacts.ejb.dao.IDaoPersonne;
import contacts.ejb.dom.Personne;


@Stateless
@Local(IDaoPersonne.class)
public class DaoPersonne implements IDaoPersonne {
	
	// Champs
	
	@PersistenceContext(unitName="contacts")
	private EntityManager em;
	
	// Injecteurs
	
	public void setEntityManager( EntityManager em ) {
		this.em = em;
	}
	


	
	// Actions
	
	@Override
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public int inserer(Personne personne) {
		em.persist( personne ) ;
		em.flush();
		return personne.getId();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public void modifier(Personne personne) {
		em.merge(personne);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public void supprimer(int idPersonne) {
		em.remove(retrouver(idPersonne));
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Personne retrouver(int idPersonne) {
		return em.find( Personne.class, idPersonne );
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Personne> listerTout() {
		String jpql="SELECT p FROM Personne p ORDER BY p.nom, p.prenom";
		TypedQuery<Personne> query = em.createQuery( jpql, Personne.class) ;
		List<Personne> personnes = query.getResultList() ;
		return personnes;
	}


	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public int compterPersonnes(int idCategorie) {
		
		String jpql="SELECT count(p) FROM Personne p WHERE p.categorie.id = :idCategorie";
		TypedQuery<Long> query = em.createQuery( jpql, Long.class) ;
		query.setParameter( "idCategorie", idCategorie ) ;
		return query.getSingleResult().intValue() ;
		
	}
	
	
	
}
