package contacts.ejb.dao.jpa;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import contacts.ejb.dao.IDaoCategorie;
import contacts.ejb.dao.IDaoCompte;
import contacts.ejb.dom.Categorie;
import contacts.ejb.dom.Compte;

@Stateless
@Local(IDaoCompte.class)
public class DaoCompte implements IDaoCompte {

	
	// Champs
	
	private Map<Integer, Compte>		mapComptes;
	@PersistenceContext(unitName="contacts")
	private EntityManager em;
	
	// Injecteurs
	
	public void setDonnees( Donnees donnees ) {
		mapComptes = donnees.getMapComptes();
	}
	
	public void setEntityManager( EntityManager em ) {
		this.em = em;
	}

	
	// Actions
	
	@Override
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public int inserer(Compte compte) {
        if (mapComptes.isEmpty()) {
            compte.setId(1);
        } else {
            compte.setId(Collections.max(mapComptes.keySet()) + 1);
        }
		mapComptes.put( compte.getId(), compte );
		return compte.getId();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public void modifier(Compte compte) {
		mapComptes.replace( compte.getId(), compte );
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public void supprimer(int idCompte) {
		mapComptes.remove( idCompte );
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Compte retrouver(int idCompte) {
		return mapComptes.get( idCompte );
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Compte> listerTout() {
		String jpql="SELECT c FROM Compte c ORDER BY c.pseudo";
		TypedQuery<Compte> query = em.createQuery( jpql, Compte.class) ;
		List<Compte> comptes = query.getResultList() ;
		return comptes;
	}


	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Compte validerAuthentification( String pseudo, String motDePasse )  {

		for ( Compte compte : mapComptes.values() ) {
			if ( compte.getPseudo().equals(pseudo) ) {
				if ( compte.getMotDePasse().equals(motDePasse) ) {
					return compte;
				}
				break;
			}
		}
		return null;
	}


	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean verifierUnicitePseudo( String pseudo, int idCompte )  {
		
		for ( Compte compte : mapComptes.values() ) {
			if ( compte.getPseudo().equals(pseudo) ) {
				if ( compte.getId() != idCompte  ) {
					return false;
				}
			}
		}
		return true;
	}
    
    private List<Compte> trierParPseudo( List<Compte> liste ) {
		Collections.sort( liste,
	            (Comparator<Compte>) ( item1, item2) -> {
	                return ( item1.getPseudo().toUpperCase().compareTo( item2.getPseudo().toUpperCase() ) );
			});
    	return liste;
    }
	
	
}
