package contacts.ejb.dom;

import java.util.ArrayList;
import java.util.List;


import javax.persistence.*;

@Entity
@Table( name="personne" )
public class Personne {
	
	
	// Champs
	@Id
	@GeneratedValue( strategy=GenerationType.IDENTITY )
	@Column( name="IdPersonne" )
	private int				id;
	@Column( name="Nom" )
	private String			nom;
	@Column( name="Prenom" )
	private String			prenom;
	@ManyToOne
	@JoinColumn( name="IdCategorie" )
	private Categorie		categorie;
	@OneToMany( mappedBy="personne", cascade=CascadeType.ALL, orphanRemoval = true)
	@OrderBy("Libelle")
	private List<Telephone>	telephones = new ArrayList<>();
	
	
	
	// Getters & setters

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	public List<Telephone> getTelephones() {
		return telephones;
	}

	public void setTelephones(List<Telephone> telephones) {
		this.telephones = telephones;
	}

	
	// Actions
	
	public void ajouterTelephone( Telephone telephone ) {
		telephones.add( telephone );
	}
	
	public void retirerTelephone( Telephone telephone ) {
		telephones.remove(telephone);
	}
	
	
	// Constructeurs
	
	public Personne() {
	}

	public Personne(int id, String nom, String prenom, Categorie categorie ) {
		super();
		setId(id);
		setNom(nom);
		setPrenom(prenom);
		setCategorie(categorie);
	}

	
	
	// hashcode() + equals()
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Personne other = (Personne) obj;
		if (id != other.id)
			return false;
		return true;
	}
	

}
