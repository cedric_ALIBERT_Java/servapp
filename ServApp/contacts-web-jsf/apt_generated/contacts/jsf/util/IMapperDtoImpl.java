package contacts.jsf.util;

import contacts.commun.dto.DtoCategorie;
import contacts.commun.dto.DtoCompte;
import contacts.commun.dto.DtoPersonne;
import contacts.commun.dto.DtoTelephone;
import java.util.ArrayList;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2016-12-07T11:50:31+0100",
    comments = "version: 1.0.0.Final, compiler: Eclipse JDT (IDE) 1.2.100.v20160418-1457, environment: Java 1.8.0_101 (Oracle Corporation)"
)
public class IMapperDtoImpl implements IMapperDto {

    @Override
    public DtoCompte duplicate(DtoCompte source) {
        if ( source == null ) {
            return null;
        }

        DtoCompte dtoCompte = new DtoCompte();

        dtoCompte.setEmail( source.getEmail() );
        dtoCompte.setId( source.getId() );
        dtoCompte.setMotDePasse( source.getMotDePasse() );
        dtoCompte.setPseudo( source.getPseudo() );
        if ( source.getRoles() != null ) {
            dtoCompte.setRoles( new ArrayList<String>( source.getRoles() ) );
        }

        return dtoCompte;
    }

    @Override
    public void update(DtoCompte source, DtoCompte target) {
        if ( source == null ) {
            return;
        }

        target.setEmail( source.getEmail() );
        target.setId( source.getId() );
        target.setMotDePasse( source.getMotDePasse() );
        target.setPseudo( source.getPseudo() );
        if ( source.getRoles() != null ) {
            if ( target.getRoles() != null ) {
                target.getRoles().clear();
                target.getRoles().addAll( source.getRoles() );
            }
            else {
                target.setRoles( new ArrayList<String>( source.getRoles() ) );
            }
        }
    }

    @Override
    public DtoCategorie duplicate(DtoCategorie source) {
        if ( source == null ) {
            return null;
        }

        DtoCategorie dtoCategorie = new DtoCategorie();

        dtoCategorie.setId( source.getId() );
        dtoCategorie.setLibelle( source.getLibelle() );

        return dtoCategorie;
    }

    @Override
    public void update(DtoCategorie source, DtoCategorie target) {
        if ( source == null ) {
            return;
        }

        target.setId( source.getId() );
        target.setLibelle( source.getLibelle() );
    }

    @Override
    public DtoPersonne duplicate(DtoPersonne source) {
        if ( source == null ) {
            return null;
        }

        DtoPersonne dtoPersonne = new DtoPersonne();

        dtoPersonne.setCategorie( duplicate( source.getCategorie() ) );
        dtoPersonne.setId( source.getId() );
        dtoPersonne.setNom( source.getNom() );
        dtoPersonne.setPrenom( source.getPrenom() );
        if ( source.getTelephones() != null ) {
            dtoPersonne.setTelephones( new ArrayList<DtoTelephone>( source.getTelephones() ) );
        }

        return dtoPersonne;
    }

    @Override
    public void update(DtoPersonne source, DtoPersonne target) {
        if ( source == null ) {
            return;
        }

        target.setId( source.getId() );
        target.setNom( source.getNom() );
        target.setPrenom( source.getPrenom() );
        if ( source.getTelephones() != null ) {
            if ( target.getTelephones() != null ) {
                target.getTelephones().clear();
                target.getTelephones().addAll( source.getTelephones() );
            }
            else {
                target.setTelephones( new ArrayList<DtoTelephone>( source.getTelephones() ) );
            }
        }

        target.setCategorie( source.getCategorie() );
    }

    @Override
    public DtoTelephone duplicate(DtoTelephone source) {
        if ( source == null ) {
            return null;
        }

        DtoTelephone dtoTelephone = new DtoTelephone();

        dtoTelephone.setId( source.getId() );
        dtoTelephone.setLibelle( source.getLibelle() );
        dtoTelephone.setNumero( source.getNumero() );

        return dtoTelephone;
    }

    @Override
    public void update(DtoTelephone source, DtoTelephone target) {
        if ( source == null ) {
            return;
        }

        target.setId( source.getId() );
        target.setLibelle( source.getLibelle() );
        target.setNumero( source.getNumero() );
    }
}
