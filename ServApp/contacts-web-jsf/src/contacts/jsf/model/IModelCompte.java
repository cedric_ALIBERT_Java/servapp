package contacts.jsf.model;

import java.util.Collection;
import java.util.List;

import contacts.commun.dto.DtoCompte;
import contacts.commun.util.Roles.Role;

public interface IModelCompte {

	Collection<DtoCompte>	getComptes();

	DtoCompte 		getCompteVue();

	List<String>	getRolesChoisis();

	Role[] 			getRoles();

	void 			setRolesChoisis(List<String> rolesChoisis);

	String 			actualiserListe();

	String 			preparerAjouter();

	String 			preparerModifier(DtoCompte compte);

	String 			validerMiseAJour();

	String 			supprimer(DtoCompte compte);

}