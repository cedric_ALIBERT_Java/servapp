package contacts.jsf.model;

import contacts.commun.dto.DtoCompte;

public interface IModelConnexion {

	String getPseudo();

	void setPseudo(String pseudo);

	String getMotDePasse();

	void setMotDePasse(String motDePasse);

	DtoCompte getCompteConnecte();

	String connect();

	String disconnect();

}