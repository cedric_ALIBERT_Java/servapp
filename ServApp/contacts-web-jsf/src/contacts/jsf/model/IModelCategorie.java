package contacts.jsf.model;

import java.util.List;

import contacts.commun.dto.DtoCategorie;

public interface IModelCategorie {

	List<DtoCategorie> getCategories();

	DtoCategorie getCategorieVue();

	String actualiserListe();

	String preparerAjouter();

	String preparerModifier( DtoCategorie categorie );

	String validerMiseAJour();

	String supprimer( DtoCategorie categorie );

}