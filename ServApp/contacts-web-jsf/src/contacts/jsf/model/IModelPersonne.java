package contacts.jsf.model;

import java.util.List;

import contacts.commun.dto.DtoPersonne;
import contacts.commun.dto.DtoTelephone;

public interface IModelPersonne {

	List<DtoPersonne> getPersonnes();

	DtoPersonne	getPersonneVue();

	String		actualiserListe();

	String		preparerAjouter();

	String		preparerModifier(DtoPersonne personne);

	String		validerMiseAJour();

	String		supprimer(DtoPersonne personne);

	String		ajouterTelephone();

	String		supprimerTelephone(DtoTelephone telephone);

}