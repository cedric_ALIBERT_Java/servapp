package contacts.jsf.model;

public interface IModelInfo {

	String getTitre();

	void setTitre(String titre);

	String getTexte();

	void setTexte(String texte);

	void preRender();

}