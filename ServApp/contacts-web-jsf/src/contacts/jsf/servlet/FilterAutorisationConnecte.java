package contacts.jsf.servlet;

import java.io.IOException;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import contacts.jsf.model.IModelConnexion;


@WebFilter(dispatcherTypes = {
				DispatcherType.REQUEST, 
				DispatcherType.FORWARD
		}
					, urlPatterns = { "/jsf/accueil/info.jsf" })
public class FilterAutorisationConnecte implements Filter {


	@Override
	public void destroy() {
	}


	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		HttpSession		session;
		IModelConnexion	modelConnexion	= null;
		boolean			flagOk	= false;
		
		session = ( (HttpServletRequest) request).getSession();
		modelConnexion = (IModelConnexion) session.getAttribute( "modelConnexion" );
		
		if( modelConnexion != null ) {
			if ( modelConnexion.getCompteConnecte() != null ) {
				flagOk = true;
			}
		}
		
		// Si un utilisateur est connecté
		if( flagOk ) {
			// on traite l'URL normalement
			chain.doFilter(request, response);
		} else {
			// sinon on affiche la page d'accueil
			request.setAttribute( "message", "Vous devez être connecté pour effectuer l'action demandée." );
			request.getRequestDispatcher( "/" ).forward(request, response);
		}
	}


	@Override
	public void init(FilterConfig fConfig) throws ServletException {
	}

}
