package contacts.jsf.servlet;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import contacts.commun.util.Roles;
import contacts.jsf.model.IModelConnexion;


@WebFilter({ "/jsf/compte/*", "/jsf/groupe/*", "/jsf/categorie/*" })
public class FilterAutorisationAdministrateur implements Filter {


	@Override
	public void destroy() {
	}


	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		HttpSession		session;
		IModelConnexion	modelConnexion	= null;
		boolean			flagOk	= false;
		
		session = ( (HttpServletRequest) request).getSession();
		modelConnexion = (IModelConnexion) session.getAttribute( "modelConnexion" );
		
		if( modelConnexion != null ) {
			if ( modelConnexion.getCompteConnecte() != null ) {
				flagOk = modelConnexion.getCompteConnecte().isInRole( Roles.ADMINISTRATEUR );
			}
		}
		
		// Si un utilisateur est connecté
		if( flagOk ) {
			// on traite l'URL normalement
			chain.doFilter(request, response);
		} else {
			// sinon on affiche la page d'accueil
			request.setAttribute( "message", "Vous n'êtes pas autorisé à effectuer l'action demandée." );
			request.getRequestDispatcher( "/jsf/accueil/info.jsf" ).forward(request, response);
		}
	}


	@Override
	public void init(FilterConfig fConfig) throws ServletException {
	}

}
