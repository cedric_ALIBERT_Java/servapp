package contacts.jsf.util;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import contacts.commun.dto.DtoCategorie;
import contacts.jsf.model.IModelCategorie;

@ManagedBean
@RequestScoped
public class ConvertisseurCategorie implements Converter {
	
	// Champs
	
	@ManagedProperty( "#{modelCategorie}")
	private IModelCategorie	modelCategorie;

	
	// Injecteurs
	
	public void setModelCategorie( IModelCategorie modelCategorie ) {
		this.modelCategorie = modelCategorie;
	}
	
	
	
	// Actions	
	
	@Override
	public Object getAsObject( FacesContext fc, UIComponent uic, String value ) {
	     if (value == null || value.isEmpty()) {
	            return null;
	        }

	        try {
	        	int id = Integer.parseInt(value);
	        	for( DtoCategorie categorie : modelCategorie.getCategories()) {
	        		if ( categorie.getId() == id ) {
	        			return categorie;
	        		}
	        	}
	        	return null;
	        } catch (NumberFormatException e) {
	            throw new ConverterException(new FacesMessage(String.format("%s n'est pas un identifiant de catégorie valide", value)), e);
	        }
	}

	@Override
	public String getAsString( FacesContext fc, UIComponent uic, Object value ) {
        if (value == null) {
            return "";
        }

        if ( value instanceof DtoCategorie ) {
            return String.valueOf(((DtoCategorie) value).getId());
        } else {
            throw new ConverterException( new FacesMessage( String.format("%s n'est pas une catégorie valide", value)) );
        }
	}

}
