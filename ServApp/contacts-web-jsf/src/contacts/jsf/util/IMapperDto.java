package contacts.jsf.util;

import java.io.Serializable;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import contacts.commun.dto.DtoCategorie;
import contacts.commun.dto.DtoCompte;
import contacts.commun.dto.DtoPersonne;
import contacts.commun.dto.DtoTelephone;


@Mapper
public interface IMapperDto extends Serializable {
	
	IMapperDto INSTANCE = Mappers.getMapper( IMapperDto.class );
	

	// Compte
	
	DtoCompte duplicate( DtoCompte source );
	
	void update( DtoCompte source, @MappingTarget DtoCompte target );
	
	
	// Categorie
	
	DtoCategorie duplicate( DtoCategorie source );
	
	void update( DtoCategorie source, @MappingTarget DtoCategorie target );
	
	
	// Personne
	
	DtoPersonne duplicate( DtoPersonne source );
	
    @Mapping( target="categorie", expression="java( source.getCategorie() )" )
    void update( DtoPersonne source, @MappingTarget DtoPersonne target );
	
	
	// Telephone
	
	DtoTelephone duplicate( DtoTelephone source );
	
	void update( DtoTelephone source, @MappingTarget DtoTelephone target );
	
	
}
	
	
	
