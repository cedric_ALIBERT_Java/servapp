package contacts.jsf.util;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import contacts.commun.util.ExceptionAnomalie;
import contacts.commun.util.ExceptionAutorisation;
import contacts.commun.util.ExceptionValidation;


public class UtilJsf {

	// Logger
	private static final Logger logger = Logger.getLogger( UtilJsf.class.getName() );

	
	// Messages d'erreur
	
	public static void genererMessageErreur( String message ) {
		genererMessageErreur( message, null );
	}

	public static void genererMessageErreur( Exception exception) {
		genererMessageErreur( null, exception );
	}

	public static void genererMessageErreur( String message, Exception exception) {
		String messageDefaut = null;
//		if ( exception != null ) {
//			
//			if ( exception.getClass().getName().equals( "javax.ejb.EJBException") 
//					|| exception.getClass().getName().equals( "javax.ejb.EJBTransactionRolledbackException") ) {
//				messageDefaut = "EJB : Echec du traitement demandé";
//				logger.log(Level.FINE, exception.getMessage(), exception );
//			} else if ( exception.getClass().getName().equals( "javax.ejb.EJBAccessException") ) {
//				messageDefaut = "EJB : Action non autoriésé !";
//				logger.log(Level.FINEST, exception.getMessage(), exception );
//			} else if ( exception.getClass().getName().equals( "contacts.exception.ExceptionAutorisation") ) {
//				messageDefaut = "Action non autoriésé !";
//				logger.log(Level.FINEST, exception.getMessage(), exception );
//			} else if ( !  exception.getClass().getName().equals( "contacts.exception.ExceptionAppli") 
//					&& ! exception.getClass().getSuperclass().getName().equals( "contacts.exception.ExceptionAppli" ) ) {
//				logger.log(Level.SEVERE, exception.getMessage(), exception );
//			} 
//			if (message == null ) {
//				if ( messageDefaut != null ) {
//					message = messageDefaut;
//				} else 	if ( ( exception.getClass().getName().equals( "contacts.exception.ExceptionAppli")
//					 && exception.getCause() != null )
//						|| exception.getMessage() == null )  {
//					message = "Ecec du traitement demandé.";
//				} else {
//					message = exception.getMessage();
//				}
//			}
//			if (message == null ) {
//				message = exception.getClass().getName();
//			}
//		}

		
		if ( exception != null ) {

			if ( exception instanceof ExceptionValidation ) {
				messageDefaut = exception.getMessage();
			} else if ( exception instanceof ExceptionAutorisation ) {
				messageDefaut = "Action non autoriésé !";
				logger.log(Level.FINEST, exception.getMessage(), exception );
			} else if ( exception.getClass().getName().equals( "javax.ejb.EJBAccessException") ) {
				messageDefaut = "EJB : Action non autoriésé !";
				logger.log(Level.FINEST, exception.getMessage(), exception );
			} else if ( exception instanceof ExceptionAnomalie ) {
				messageDefaut = "Echec du traitement demandé";
				logger.log(Level.FINEST, exception.getMessage(), exception );
			} else if ( exception.getClass().getName().equals( "javax.ejb.EJBException") 
					|| exception.getClass().getName().equals( "javax.ejb.EJBTransactionRolledbackException") ) {
				messageDefaut = "EJB : Echec du traitement demandé";
				logger.log(Level.FINEST, exception.getMessage(), exception );
			} else if ( exception instanceof RuntimeException ) {
				messageDefaut = "Echec du traitement demandé";
				logger.log(Level.SEVERE, exception.getMessage(), exception );
			} else {
				messageDefaut = exception.getMessage();
				logger.log(Level.SEVERE, exception.getMessage(), exception );
			}
		
			if (message == null ) {
				if ( messageDefaut != null ) {

					message = messageDefaut;

					// Ajoute le message de la cause
					Throwable cause = exception.getCause();
					if( cause != null ) {
						while( cause.getCause() != null ) {
							cause = cause.getCause();
						}
						if ( cause.getMessage() != null ) {
							message = message + "\n" +cause.getMessage();
						}
					}
					
				} else {
					message = "Ecec du traitement demandé.";
				}
			}
		}

		FacesContext fc = FacesContext.getCurrentInstance();
		String[] lignes = message.split("\\n");
	    for ( String ligne : lignes ) {
	 		fc.addMessage( null, new FacesMessage( FacesMessage.SEVERITY_ERROR, ligne, null ) );
	    }
	}
	
	
	public static void traiterMessageErreur() {
		String message = UtilJsf.getRequestAttribute( "messageErreur" );
		if( message != null ) {
			UtilJsf.genererMessageErreur(message);
		}
	}
	
	// Session
	
	@SuppressWarnings("unchecked")
	public static <T> T getSessionAttribute(String name) {
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
	  	return (T) ec.getSessionMap().get( name );
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T removeSessionAttribute(String name) {
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
	  	return (T) ec.getSessionMap().remove( name );
	}
	
	public static void sessionInvalidate() {
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		HttpSession session = (HttpSession) ec.getSession(false);
		if( session != null ) {
			session.invalidate();
		}
		
	}

	
	// Requête
	
	@SuppressWarnings("unchecked")
	public static <T> T getRequestAttribute(String name) {
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
	  	return (T) ec.getRequestMap().get( name );
	}
	
	public static void forward(String uri) throws IOException {
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		ec.dispatch(uri);
	}
	
	
	
	
	
	// Constructeur privé pour empêcher l'instanciaiton de la classe
	private UtilJsf() {
	}
}
