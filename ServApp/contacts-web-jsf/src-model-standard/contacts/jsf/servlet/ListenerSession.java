package contacts.jsf.servlet;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionActivationListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import contacts.jsf.model.standard.ModelConnexion;
import contacts.commun.service.IContextService;
import contacts.commun.service.IServiceConnexion;


@SuppressWarnings("serial")
@WebListener
public class ListenerSession implements HttpSessionListener, HttpSessionActivationListener, Serializable {
	
	
	// LOgger
	
	private static final Logger logger = Logger.getLogger( ListenerSession.class.getName() );

	private static final boolean flagDebug = false;
	
	
	// Actions
	
	@Override
	public void sessionCreated( HttpSessionEvent event ) {
		if ( flagDebug ) {
			System.out.println( "sessionCreated" );
		}
		HttpSession session = event.getSession();
		session.setAttribute( "listenerSessionActivation", this );
		initComposants( event );
	}

	
	@Override
	public void sessionDestroyed( HttpSessionEvent event ) {
		if ( flagDebug ) {
			System.out.println( "sessionDestroyed()" );
		}
		libererRessources(event);
	}
	
	
	@Override
	public void sessionDidActivate( HttpSessionEvent event ) {
		if ( flagDebug ) {
			System.out.println( "sessionDidActivate()" );
		}
		initComposants( event );
	}

	@Override
	public void sessionWillPassivate( HttpSessionEvent event ) {
		if ( flagDebug ) {
			System.out.println( "sessionWillPassivate()" );
		}
		libererRessources(event);
	}
	
    

	
	// Méthodes auxiliaires
	
	private void initComposants( HttpSessionEvent event ){
		
		if( flagDebug ) {
			System.out.println( "initComposants()" );
		}
		
		HttpSession session = event.getSession();
		ServletContext application = session.getServletContext();

		try {

			session.setAttribute( "verrouSession", new ReentrantLock() );

			IContextService contextService = (IContextService) application.getAttribute("contextService");
			
			ModelConnexion beanConnexion = (ModelConnexion) session.getAttribute( "modelConnexion" );
			if ( beanConnexion != null && beanConnexion.getCompteConnecte() != null ) {
				contextService.getService( IServiceConnexion.class ).sessionUtilisateurOuvrir( beanConnexion.getCompteConnecte().getPseudo(), beanConnexion.getCompteConnecte().getMotDePasse() );
			}
 
			Enumeration<String> noms = session.getAttributeNames();
			while( noms.hasMoreElements() ) {
				String nom = noms.nextElement();
				if( flagDebug ) {
					System.out.println( "   " + nom  );
				}
				Object attribut = session.getAttribute(nom);
					Method methode;
					try {
						methode = attribut.getClass().getMethod( "setContextService", new Class[] { IContextService.class} );
						methode.invoke( attribut, contextService );
						if( flagDebug ) {
							System.out.println( "   " + nom + " : " + methode.getName() );
						}
					} catch (NoSuchMethodException | SecurityException e1) {
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						logger.log( Level.SEVERE, e.getMessage(), e );
					}
			}

		} catch (Exception e) {
			logger.log( Level.SEVERE, e.getMessage(), e );
		}

	}
	
	
	private void libererRessources( HttpSessionEvent event ) {

//		HttpSession session = event.getSession();
//		ServletContext application = session.getServletContext();
//
//		IProxyEntityManager proxyEM = (IProxyEntityManager) application.getAttribute( "proxyEntityManager" );
//		if ( proxyEM != null ) {
//			proxyEM.closeEntityManager(session);;;
//		}
	}
    
}
