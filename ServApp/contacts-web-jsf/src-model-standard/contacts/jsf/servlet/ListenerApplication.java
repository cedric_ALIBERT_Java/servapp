package contacts.jsf.servlet;

import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import contacts.emb.dao.IContextDao;
import contacts.emb.util.securite.IManagerSecurite;
import contacts.commun.service.IContextService;
import contacts.web.util.jdbc.FactoryProxyDataSource;
import contacts.web.util.jdbc.IProxyDataSource;


@WebListener
public class ListenerApplication implements ServletContextListener {
	
	
	// LOgger
	
	private static final Logger logger = Logger.getLogger( ListenerApplication.class.getName() );
	
	
	// Actions

    public void contextInitialized(ServletContextEvent event)  {
		
    	// Composants de l'application
		ServletContext 				application = event.getServletContext();
    	IProxyDataSource 			proxyDS;
//    	IProxyEntityManager 		proxyEM;
    	IContextDao					contextDao;
    	IManagerSecurite			managerSecurite; 
    	IContextService				contextService;
		

		try {

			// Configuration de la trace
			configurerLogging();

			
			// JDBC
			
			InputStream isProps =  contacts.emb.dao.jdbc.ContextDao.class.getResourceAsStream( "/META-INF/jdbc.properties" );
			Properties props = new Properties();
			props.load(isProps);
			isProps.close();

			com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource ds =
					new com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource();
			ds.setUrl( props.getProperty( "jdbc.url" ) );
			ds.setUser( props.getProperty( "jdbc.user" ) );
			ds.setPassword( props.getProperty( "jdbc.password" ) );
				
			proxyDS =
//					null;
					FactoryProxyDataSource.createProxy( ds );

			
			// JPA
			
//			EntityManagerFactory emf =
//					Persistence.createEntityManagerFactory( "contacts" );
//			proxyEM =
//					FactoryProxyEntityManager.createProxy( emf );

			
			// Composants de l'application

			contextDao = 
//					null;
//					new contacts.emb.dao.mock.ContextDao();
					new contacts.emb.dao.jdbc.ContextDao( proxyDS );
//					new contacts.emb.dao.jpa.ContextDao( proxyEM );

			managerSecurite =
//					null;
					new contacts.web.util.securite.ManagerSecuriteServlet();

			contextService = 
//					null;
//					new contacts.emb.service.mock.ContextService( managerSecurite );
					new contacts.emb.service.standard.ContextService( contextDao, managerSecurite );
				  

			// Enregistre les composants comme attributs de l'application

			application.setAttribute( "proxyDataSource", proxyDS );
//			application.setAttribute( "proxyEntityManager", proxyEM );
			application.setAttribute( "managerSecurite", managerSecurite);
			application.setAttribute( "contextService", contextService);
			
		    	
	    	// Trace
	    	
	    	StringBuilder sbMessage = new StringBuilder();
	    	if ( contextDao != null ) {
	    		sbMessage.append( "\n    contextDao     : " ).append( contextDao.getClass().getName() );
	    	}
	    	if ( contextService != null ) {
	    		sbMessage.append( "\n    contextService : " ).append( contextService.getClass().getName() );
	    	}
			logger.log(Level.CONFIG, sbMessage.toString() );

			
		} catch (Exception e) {
			logger.log( Level.SEVERE, e.getMessage(), e );
		}

    }


    public void contextDestroyed(ServletContextEvent event)  { 
		
		logger.log(Level.FINE, "\n    Arrêt de l'application web" );
		
		ServletContext application = event.getServletContext();

		IProxyDataSource ds = (IProxyDataSource) application.getAttribute( "proxyDataSource" );
		if ( ds != null ) {
			ds.close();
		}

//		IProxyEntityManager proxyEM = (IProxyEntityManager) application.getAttribute( "proxyEntityManager" );
//		if ( proxyEM != null ) {
//			proxyEM.getEntityManagerFactory().close();
//		}
    }
	
    
    private void configurerLogging() {

		// Configuraiton du logging
    	
    	try {
        	InputStream in = this.getClass().getResourceAsStream("/META-INF/logging.properties");
        	LogManager logManager = LogManager.getLogManager();
			logManager.readConfiguration( in );
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    }
    
}
