package contacts.jsf.servlet;

import java.io.IOException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import contacts.web.util.jdbc.IProxyDataSource;
import contacts.web.util.securite.IManagerSecuriteServlet;


@WebFilter( dispatcherTypes = {DispatcherType.REQUEST },
			servletNames = { "Faces Servlet" })
public class FilterThreadJsf implements Filter {
	
	

	@Override
	public void destroy() {
	}


	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		Lock					verrou = null;

		String 					message = null;
		String 					cheminVue = null;
		IManagerSecuriteServlet	managerSecurite = null;
		IProxyDataSource		proxyDS = null;
//		IProxyEntityManager		proxyEM = null;
		
		HttpServletRequest		httpRequest = (HttpServletRequest) request;
		HttpSession 			session 	= httpRequest.getSession();
		ServletContext 			application = httpRequest.getServletContext();
		
		try {
			
			// Traitements avant l'exécution de la requête web
			
			String chemin = httpRequest.getRequestURI();
			chemin = chemin.substring( httpRequest.getContextPath().length() );
			
			if ( ! chemin.startsWith( "/javax.faces" ) ) {
				
				// Verrouille la session
				verrou = (Lock) session.getAttribute( "verrouSession" );
				if ( verrou == null ) {
					verrou = new ReentrantLock();
					session.setAttribute( "verrouSession", verrou );
				}
				verrou.lock();;
	
				// Manager Securite
				managerSecurite = (IManagerSecuriteServlet) application.getAttribute( "managerSecurite" );
				if ( managerSecurite == null ) {
					cheminVue = "/";
					message = "Panne de l'application.\nContactez l'administrateur.";
				} else {
					managerSecurite.threadAfterBein(session);
				}
	
				// Proxy Entity Manager
//				proxyEM = (IProxyEntityManager) application.getAttribute( "proxyEntityManager" );
//				if ( proxyEM != null ) {
//					proxyEM.threadAfterBein(session);
//				}
	 			
			} 
			
			
			// Exécution de la requête web
			
			if (cheminVue == null ) {
				// Si pas de problème exécution de la requête
				chain.doFilter(request, response);
			} else {
				// Sinon, redireciton pour afficher un message d'erreur
				httpRequest.setAttribute("message", message );
				request.getRequestDispatcher(cheminVue).forward(request, response);
			}
	
			
			// Traitements après l'exécution de la requête web
	
			// Manager Securite
			if ( ! chemin.startsWith( "/javax.faces" ) ) {
				if ( managerSecurite != null ) {
					managerSecurite.threadBeforeEnd(session);
				} else {
					session.invalidate();
				} 
	
				// Proxy DataSource
				proxyDS = (IProxyDataSource) application.getAttribute( "proxyDataSource" );
				if ( proxyDS != null ) {
					proxyDS.closeConnection();
				}
	
				// Proxy Entity Manager
//				if ( proxyEM != null ) {
//					proxyEM.threadBeforeEnd(session);
//				}
				
			}

		} finally {
			// Déverrouile la session
			if ( verrou != null ) {
				verrou.unlock();
			}
		}
	}


	@Override
	public void init(FilterConfig fConfig) throws ServletException {
	}

}
