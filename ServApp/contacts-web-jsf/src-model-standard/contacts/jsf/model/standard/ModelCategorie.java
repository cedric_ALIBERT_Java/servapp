package contacts.jsf.model.standard;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import contacts.commun.dto.DtoCategorie;
import contacts.commun.service.IContextService;
import contacts.commun.service.IServiceCategorie;
import contacts.commun.util.ExceptionAppli;
import contacts.jsf.model.IModelCategorie;
import contacts.jsf.util.IMapperDto;
import contacts.jsf.util.UtilJsf;

@SuppressWarnings("serial")
@ManagedBean
@SessionScoped
public class ModelCategorie implements Serializable, IModelCategorie {
	
	
	// Données des vues
	private List<DtoCategorie>		categories;
	private DtoCategorie			categorieVue;
	
	// Objet courant
	private DtoCategorie			categorieCourant;
	
	// Autres champs

	@ManagedProperty( "#{contextService}")
	private transient IContextService	contextService;
	
	private transient IServiceCategorie	serviceCategorie;
	
	private final IMapperDto				mapper = IMapperDto.INSTANCE;
	
	
	// Getters & setters
	
	@Override
	public List<DtoCategorie> getCategories() {
		return categories;
	}
	
	@Override
	public DtoCategorie getCategorieVue() {
		return categorieVue;
	}

	
	// Injecteur
	
	public void setContextService( IContextService contextService) {
		this.contextService = contextService;
		serviceCategorie = contextService.getService( IServiceCategorie.class );
		actualiserListe();
	}
	
	
	// Actions

	@Override
	public String actualiserListe() {
		try {
			categories = serviceCategorie.listerTout();
			trierListe();
		} catch (ExceptionAppli e) {
			UtilJsf.genererMessageErreur(e);
		}
		return null;
	}

	@Override
	public String preparerAjouter() {
		categorieCourant = null;
		categorieVue = new DtoCategorie();
		return "formulaire";
	}

	@Override
	public String preparerModifier( DtoCategorie categorie ) {
		categorieCourant = categorie;
		categorieVue = mapper.duplicate( categorie );
		return "formulaire";
	}

	@Override
	public String validerMiseAJour() {
		try {
			if( categorieCourant == null ) {
				// Ajout
				int id = serviceCategorie.inserer(categorieVue);
				categorieVue.setId(id);
				categories.add(categorieVue);
			} else {
				// Modification
				serviceCategorie.modifier(categorieVue);
				mapper.update( categorieVue, categorieCourant );
			}
			trierListe();
			return "categories";
		} catch (ExceptionAppli e) {
			UtilJsf.genererMessageErreur(e);
			return null; 
		}
	}

	@Override
	public String supprimer( DtoCategorie categorie ) {
		try {
			serviceCategorie.supprimer( categorie.getId() );
			categories.remove(categorie);
		} catch (ExceptionAppli e) {
			UtilJsf.genererMessageErreur(e);
		}
		return null;
	}
    
    
    // Méthodes auxiliaires
    
    private void trierListe() {
		Collections.sort( categories,
            (Comparator<DtoCategorie>) ( item1, item2 ) -> {
                return ( item1.getLibelle().toUpperCase().compareTo(item2.getLibelle().toUpperCase()));
		});
    }
	
}
