package contacts.jsf.model.standard;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import contacts.jsf.model.IModelInfo;
import contacts.jsf.util.UtilJsf;


@SuppressWarnings("serial")
@ManagedBean
@SessionScoped
public class ModelInfo implements Serializable, IModelInfo {
	
	// Données des vues
	
	private String	titre;
	
	private String	texte;
	
	
	
	// Getters & setters

	@Override
	public String getTitre() {
		return titre;
	}

	@Override
	public void setTitre(String titre) {
		this.titre = titre;
	}

	@Override
	public String getTexte() {
		return texte;
	}

	@Override
	public void setTexte(String texte) {
		this.texte = texte;
	}
	
	
	// Actions
	
	@Override
	public void preRender() {
		
		String message = UtilJsf.getRequestAttribute( "message" );
		if ( message != null ) {
			titre = "Erreur";
			UtilJsf.genererMessageErreur(message);
		}
		
	}
	
	

}
