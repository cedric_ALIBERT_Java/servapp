package contacts.jsf.model.standard;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import contacts.commun.dto.DtoCompte;
import contacts.commun.service.IContextService;
import contacts.commun.service.IServiceConnexion;
import contacts.commun.util.ExceptionAppli;
import contacts.jsf.model.IModelConnexion;
import contacts.jsf.model.IModelInfo;
import contacts.jsf.util.UtilJsf;


@SuppressWarnings("serial")
@ManagedBean
@SessionScoped
public class ModelConnexion implements Serializable, IModelConnexion {
	
	
	// Données de la vue
	
	private String	pseudo;
	private String	motDePasse;
	
	
	// Autres champs
	
	private DtoCompte	compteConnecte;
	
	@ManagedProperty( "#{modelInfo}")
	private IModelInfo	modelInfo;
	
	@ManagedProperty( "#{contextService}")
	private transient  IContextService	contextService;
	
	private transient IServiceConnexion	serviceConnexion;

	
	// Getters & setters
	
	@Override
	public String getPseudo() {
		return pseudo;
	}

	@Override
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	@Override
	public String getMotDePasse() {
		return motDePasse;
	}

	@Override
	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}
	
	@Override
	public DtoCompte getCompteConnecte() {
		return compteConnecte;
	}

	
	// Injecteurs
	
	public void setModelInfo( final IModelInfo modelInfo) {
		this.modelInfo = modelInfo;
	}
	
	public void setContextService(IContextService contextService) {
		this.contextService = contextService;
		serviceConnexion = contextService.getService( IServiceConnexion.class );
	}
	
	
	// Actions
	
	@Override
	public String connect() {
		try {
			compteConnecte = serviceConnexion.sessionUtilisateurOuvrir(pseudo, motDePasse);
		} catch (Exception e) {
			UtilJsf.genererMessageErreur(e);
			return null;
		}
		
		if ( compteConnecte != null ) {
			modelInfo.setTitre("Bienvenue");
			modelInfo.setTexte( "Vous êtes connecté en tant que '" + pseudo + "'.");
			return "info";
		} else {
			UtilJsf.genererMessageErreur( "Pseudo ou mot de passe invalide." );
			return null;
		}
		
	}
	
	
	@Override
	public String disconnect() {
		try {
			serviceConnexion.sessionUtilisateurFermer();
		} catch (ExceptionAppli e) {
			UtilJsf.genererMessageErreur(e);
		}
//		pseudo = null;
//		motDePasse = null;
//		compteConnecte = null;
		UtilJsf.sessionInvalidate();
		return "connexion";
	}
	

}
