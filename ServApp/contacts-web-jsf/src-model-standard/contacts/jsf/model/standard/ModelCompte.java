package contacts.jsf.model.standard;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import contacts.commun.dto.DtoCompte;
import contacts.commun.service.IContextService;
import contacts.commun.service.IServiceCompte;
import contacts.commun.util.ExceptionAppli;
import contacts.commun.util.Roles;
import contacts.commun.util.Roles.Role;
import contacts.jsf.model.IModelCompte;
import contacts.jsf.util.IMapperDto;
import contacts.jsf.util.UtilJsf;


@SuppressWarnings("serial")
@ManagedBean
@SessionScoped
public class ModelCompte implements Serializable, IModelCompte {

	// Données des vues
	private List<DtoCompte>		comptes;
	private DtoCompte			compteVue;
	private List<String>		rolsChoisis;
	
	// Objet courant
	private DtoCompte			compteCourant;
	
	// Autres champs

	@ManagedProperty( "#{contextService}")
	private transient IContextService		contextService;

	private transient IServiceCompte		serviceCompte;
	
	private final IMapperDto				mapper = IMapperDto.INSTANCE;
	
	
	// Getters & setters
	
	@Override
	public Collection<DtoCompte> getComptes() {
		return comptes;
	}


	@Override
	public DtoCompte getCompteVue() {
		return compteVue;
	}

	@Override
	public List<String> getRolesChoisis() {
		return rolsChoisis;
	}

	@Override
	public void setRolesChoisis(List<String> rolesChoisis) {
		this.rolsChoisis = rolesChoisis;
	}
	
	@Override
	public Role[] getRoles() {
		return Roles.getRoles();
	}

	
	// Injecteurs
	
	public void setContextService( IContextService contextService) {
		this.contextService = contextService;
		serviceCompte = contextService.getService( IServiceCompte.class );
		actualiserListe();
	}
	
	
	// Actions

	
	// Actions

	@Override
	public String actualiserListe() {
		try {
			comptes = serviceCompte.listerTout();
			trierListe();
		} catch (ExceptionAppli e) {
			UtilJsf.genererMessageErreur(e);
		}
		return null;
	}

	@Override
	public String preparerAjouter() {
		compteCourant = null;
		compteVue = new DtoCompte();
		rolsChoisis = new ArrayList<>();
		return "formulaire";
	}

	@Override
	public String preparerModifier( DtoCompte compte ) {
		compteCourant = compte;
		compteVue = mapper.duplicate( compte );
		rolsChoisis = new ArrayList<>();
		for ( String role : compteVue.getRoles() ) {
			rolsChoisis.add( role );
		}
		return "formulaire";
	}

	@Override
	public String validerMiseAJour() {
		try {

			compteVue.getRoles().clear();
			for( String id : rolsChoisis ) {
				for( String role : Roles.getIds() ) {
					if( role.equals( id ) ) {
						compteVue.getRoles().add( role );
					}
				}
			}
			
			if( compteCourant == null ) {
				// Ajout
				int id = serviceCompte.inserer(compteVue);
				compteVue.setId(id);
				comptes.add( compteVue );
			} else {
				// Modification
				serviceCompte.modifier(compteVue);
				mapper.update( compteVue, compteCourant );
			}
			trierListe();
			return "comptes";
			
		} catch (ExceptionAppli e) {
			UtilJsf.genererMessageErreur(e);
			return null;
		}
	}

	@Override
	public String supprimer( DtoCompte compte ) {
		try {
			serviceCompte.supprimer( compte.getId() );
			comptes.remove( compte );
		} catch (ExceptionAppli e) {
			UtilJsf.genererMessageErreur(e);
		}
		return null;
	}
    
    
    // Méthodes auxiliaires
    
    private void trierListe() {
		Collections.sort( comptes,
            (Comparator<DtoCompte>) ( item1, item2 ) -> {
                return ( item1.getPseudo().toUpperCase().compareTo(item2.getPseudo().toUpperCase()));
		});
    }

}
