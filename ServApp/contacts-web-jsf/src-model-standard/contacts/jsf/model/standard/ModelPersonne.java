package contacts.jsf.model.standard;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import contacts.commun.dto.DtoPersonne;
import contacts.commun.dto.DtoTelephone;
import contacts.commun.service.IContextService;
import contacts.commun.service.IServicePersonne;
import contacts.commun.util.ExceptionAppli;
import contacts.jsf.model.IModelPersonne;
import contacts.jsf.util.IMapperDto;
import contacts.jsf.util.UtilJsf;

@SuppressWarnings("serial")
@ManagedBean
@SessionScoped
public class ModelPersonne implements Serializable, IModelPersonne {
	
	// Données des vues
	
	private List<DtoPersonne> 	personnes;
	
	private DtoPersonne			personneVue;
	
	private DtoPersonne			personneCourant;

	
	// Autre champs
	
	@ManagedProperty("#{contextService}")
	private transient IContextService	contextService;
	
	private final IMapperDto				mapper = IMapperDto.INSTANCE;
	
	private transient IServicePersonne	servicePersonne;
	
	
	
	// Getters & setters
	
	@Override
	public List<DtoPersonne> getPersonnes() {
		return personnes;
	}
	
	@Override
	public DtoPersonne getPersonneVue() {
		return personneVue;
	}

	
	// Injecteurs
	
	public void setContextService( IContextService contextService) {
		this.contextService = contextService;
		servicePersonne = contextService.getService( IServicePersonne.class );
		actualiserListe();
	}
	
	
	// Actions
	
	@Override
	public String actualiserListe() {
		
		try {
			personnes = servicePersonne.listerTout();
		} catch (ExceptionAppli e) {
			UtilJsf.genererMessageErreur(e);
		}
		
		return null;
	}
	
	
	@Override
	public String preparerAjouter() {
		personneCourant = null;
		personneVue = new DtoPersonne();
		return "formulaire";
	}
	
	@Override
	public String preparerModifier( DtoPersonne personne ) {
		personneCourant = personne;
		personneVue = mapper.duplicate( personne );
		return "formulaire";
	}

	
	@Override
	public String validerMiseAJour() {
		
		try {
			if( personneCourant == null ) {
				// Ajout
				int id = servicePersonne.inserer(personneVue);
				// Récupère les id des téléphones
				personneVue = servicePersonne.retrouver(id);
				personnes.add(personneVue);
			} else {
				// Modification
				servicePersonne.modifier(personneVue);
				// Récupère les id des téléphones
				personneVue = servicePersonne.retrouver(personneVue.getId());
				mapper.update( personneVue, personneCourant );
			}
			return "liste";
		} catch (ExceptionAppli e) {
			UtilJsf.genererMessageErreur(e);
			return null; 
		}
	}
	
	@Override
	public String supprimer( DtoPersonne personne ) {
		
		try {
			servicePersonne.supprimer( personne.getId() );
			personnes.remove(personne);
		} catch (ExceptionAppli e) {
			UtilJsf.genererMessageErreur(e);
		}
		return null;
	}
	
	
	
	@Override
	public String ajouterTelephone() {
		personneVue.getTelephones().add( new DtoTelephone() );
		return null;
	}
	
	
	@Override
	public String supprimerTelephone( DtoTelephone telephone ) {

		for ( int i = personneVue.getTelephones().size() - 1; i >= 0; i-- ) {
			if ( telephone == personneVue.getTelephones().get(i) ) {
				personneVue.getTelephones().remove(i);
				break;
			}
		}
		return null;
	}
	
	
}
