package contacts.web.util.jdbc;

import javax.sql.DataSource;

public interface IProxyDataSource extends DataSource  {
	
	public void closeConnection();
	
	public void close();

}
