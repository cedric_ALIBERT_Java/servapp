package contacts.web.util.jdbc;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.sql.SQLException;

import javax.sql.DataSource;

import contacts.emb.util.jdbc.FactoryProxyConnection;
import contacts.emb.util.jdbc.IProxyConnection;


public class FactoryProxyDataSource {
	
	
	public static IProxyDataSource createProxy( DataSource dataSource ) {

		return (IProxyDataSource) Proxy.newProxyInstance(
				IProxyDataSource.class.getClassLoader(),
				new Class[] {IProxyDataSource.class },
				new Handler( dataSource )
				);		
	}
	
	
	
	private static class Handler implements InvocationHandler {
		

		// Champs
		
		private final ThreadLocal<IProxyConnection> tlConnection;
		
		private final DataSource	dataSource;
		
		private boolean		flagDebug = false;
		
		
		// Constructeur
		
		public Handler( DataSource ds ) {
			super();

			if ( ds == null ) {
				throw new NullPointerException();
			}
			this.dataSource = ds;
			
			tlConnection = new ThreadLocal<IProxyConnection>() {
				@Override
				protected IProxyConnection initialValue() {
					try {
						return FactoryProxyConnection.createProxy( dataSource.getConnection() );
					} catch (SQLException e) {
						throw new RuntimeException(e);
					}
				}
			};
		}

		
		// Actions
		
	    @Override
		public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
	    	
	    	switch ( method.getName() ) {
	    		
	    	case "getConnection" :
	    		return tlConnection.get();
	    		
	    	case "closeConnection" :
	    		if ( flagDebug ) {
		    		System.out.println( this.getClass().getName() + " " + method.getName() + "()" );
	    		}
	    		tlConnection.get().closeConnection();;;
	    		tlConnection.remove();
	    		return null ;
	    		
	    	case "close" :
	    		if ( flagDebug ) {
		    		System.out.println( this.getClass().getName() + " " + method.getName() + "()" );
	    		}
	    		try {
					method = dataSource.getClass().getMethod( "close" );
					method.invoke(dataSource);
				} catch (Exception e) {
				}
	    		return null;
	    		
	    	default :
		    	return method.invoke( dataSource, args);
	    	}
	    	
		}
	    	
	}

}
