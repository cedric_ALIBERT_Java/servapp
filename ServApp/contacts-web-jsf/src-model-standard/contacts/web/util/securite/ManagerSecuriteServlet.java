package contacts.web.util.securite;

import javax.servlet.http.HttpSession;

import contacts.commun.dto.DtoCompte;
import contacts.emb.util.securite.ManagerSecurite;


public class ManagerSecuriteServlet extends ManagerSecurite implements IManagerSecuriteServlet {
	
	private final static String nomAtribut = ManagerSecuriteServlet.class.getName() + ".compteConnecte"; 

	@Override
	public void threadAfterBein( HttpSession session ) {
		tlCompteConnecte.set( (DtoCompte) session.getAttribute(nomAtribut));
	}

	@Override
	public void threadBeforeEnd( HttpSession session ) {
		try {
			session.setAttribute( nomAtribut, tlCompteConnecte.get());
		} catch (Exception e) {
		}
		tlCompteConnecte.remove();
	}
	
}
