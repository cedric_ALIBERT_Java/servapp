package contacts.web.util.securite;

import javax.servlet.http.HttpSession;

import contacts.emb.util.securite.IManagerSecurite;


public interface IManagerSecuriteServlet extends IManagerSecurite {

	void threadAfterBein( HttpSession session );

	void threadBeforeEnd( HttpSession session );

}
