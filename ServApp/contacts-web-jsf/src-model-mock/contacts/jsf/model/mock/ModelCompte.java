package contacts.jsf.model.mock;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import contacts.commun.dto.DtoCompte;
import contacts.commun.util.Roles;
import contacts.commun.util.Roles.Role;
import contacts.jsf.model.IModelCompte;
import contacts.jsf.util.IMapperDto;


@SuppressWarnings("serial")
@ManagedBean
@SessionScoped
public class ModelCompte implements Serializable, IModelCompte {

	// Données des vues
	private List<DtoCompte>		comptes;
	private DtoCompte			compteVue;
	private List<String>		rolesChoisis;
	
	// Objet courant
	private DtoCompte			compteCourant;
	
	// Autres champs
	
	private final IMapperDto	mapper = IMapperDto.INSTANCE;
	
	
	// Getters & setters
	
	@Override
	public Collection<DtoCompte> getComptes() {
		return comptes;
	}

	@Override
	public DtoCompte getCompteVue() {
		return compteVue;
	}

	@Override
	public List<String> getRolesChoisis() {
		return rolesChoisis;
	}

	@Override
	public void setRolesChoisis(List<String> rolesChoisis) {
		this.rolesChoisis = rolesChoisis;
	}

	@Override
	public Role[] getRoles() {
		return Roles.getRoles();
	}

	
	// Actions

	@Override
	public String actualiserListe() {
		trierListe();
		return null;
	}

	@Override
	public String preparerAjouter() {
		compteCourant = null;
		compteVue = new DtoCompte();
		rolesChoisis = new ArrayList<>();
		return "formulaire";
	}

	@Override
	public String preparerModifier( DtoCompte compte ) {
		compteCourant = compte;
		compteVue = mapper.duplicate( compte );
		rolesChoisis = new ArrayList<>();
		for ( String role : compteVue.getRoles() ) {
			rolesChoisis.add( role );
		}
		return "formulaire";
	}

	@Override
	public String validerMiseAJour() {
		compteVue.getRoles().clear();
		for( String id : rolesChoisis ) {
			for( String role : Roles.getIds() ) {
				if( role.equals(id) ) {
					compteVue.getRoles().add( role );
				}
			}
		}
		
		if( compteCourant == null ) {
			// Ajout
			compteVue.setId(getIdSuivant());
			comptes.add( compteVue );
		} else {
			// Modification
			mapper.update( compteVue, compteCourant );
		}
		trierListe();
		return "comptes";
	}

	@Override
	public String supprimer( DtoCompte compte ) {
		comptes.remove( compte );
		return null;
	}
    
    
    // Méthodes auxiliaires
    
    private void trierListe() {
		Collections.sort( comptes,
            (Comparator<DtoCompte>) ( item1, item2 ) -> {
                return ( item1.getPseudo().toUpperCase().compareTo(item2.getPseudo().toUpperCase()));
		});
    }
    
    
    @PostConstruct
	public void initialiserDonnees() {
    	
    	comptes = new ArrayList<>();
		
		DtoCompte compte;
		compte = new DtoCompte( 1, "geek", "geek", "geek@3il.fr" );
		compte.getRoles().add( Roles.ADMINISTRATEUR  );
		compte.getRoles().add( Roles.UTILISATEUR  );
		comptes.add( compte );
		
		compte = new DtoCompte( 2, "chef", "chef", "chef@3il.fr" );
		compte.getRoles().add( Roles.UTILISATEUR  );
		comptes.add( compte );
		
		compte = new DtoCompte( 3, "job", "job", "job@3il.fr" );
		compte.getRoles().add( Roles.UTILISATEUR  );
		comptes.add( compte );
		
		trierListe();
	}

    
    private int getIdSuivant() {
    	DtoCompte itemMax = Collections.max( comptes,
	            (Comparator<DtoCompte>) ( item1, item2) -> {
	                return ( item1.getId() - item2.getId() );
			});
		if ( itemMax != null ) {
			return itemMax.getId() + 1;
		} else {
			return 1;
		}
    }

}
