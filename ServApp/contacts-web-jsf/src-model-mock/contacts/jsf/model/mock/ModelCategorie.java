package contacts.jsf.model.mock;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import contacts.commun.dto.DtoCategorie;
import contacts.jsf.model.IModelCategorie;
import contacts.jsf.util.IMapperDto;

@SuppressWarnings("serial")
@ManagedBean
@SessionScoped
public class ModelCategorie implements Serializable, IModelCategorie {
	
	
	// Données des vues
	private List<DtoCategorie>		categories;
	private DtoCategorie			categorieVue;
	
	// Objet courant
	private DtoCategorie			categorieCourant;
	
	// Autres champs
	
	private final IMapperDto		mapper = IMapperDto.INSTANCE;
	
	
	// Getters & setters
	
	@Override
	public List<DtoCategorie> getCategories() {
		return categories;
	}
	
	@Override
	public DtoCategorie getCategorieVue() {
		return categorieVue;
	}
	
	
	// Actions

	@Override
	public String actualiserListe() {
		trierListe();
		return null;
	}

	@Override
	public String preparerAjouter() {
		categorieCourant = null;
		categorieVue = new DtoCategorie();
		return "formulaire";
	}

	@Override
	public String preparerModifier( DtoCategorie categorie ) {
		categorieCourant = categorie;
		categorieVue = mapper.duplicate( categorie );
		return "formulaire";
	}

	@Override
	public String validerMiseAJour() {
		if( categorieCourant == null ) {
			// Ajout
			categorieVue.setId(getIdSuivant());
			categories.add(categorieVue);
		} else {
			// Modification
			mapper.update( categorieVue, categorieCourant );
		}
		return "categories";
	}

	@Override
	public String supprimer( DtoCategorie categorie ) {
		categories.remove(categorie);
		return null;
	}
    
    
    // Méthodes auxiliaires
    
    private void trierListe() {
		Collections.sort( categories,
            (Comparator<DtoCategorie>) ( item1, item2 ) -> {
                return ( item1.getLibelle().toUpperCase().compareTo(item2.getLibelle().toUpperCase()));
		});
    }
    
    
    @PostConstruct
	public void initialiserDonnees() {
		categories = new ArrayList<>();
		categories.add( new DtoCategorie( 1, "Ecrivains" ) );
    	categories.add( new DtoCategorie( 2, "Peintres" ) );
		trierListe();
	}

    
    private int getIdSuivant() {
    	DtoCategorie itemMax = Collections.max( categories,
	            (Comparator<DtoCategorie>) ( item1, item2) -> {
	                return ( item1.getId() - item2.getId() );
			});
		if ( itemMax != null ) {
			return itemMax.getId() + 1;
		} else {
			return 1;
		}
    }
	
}
