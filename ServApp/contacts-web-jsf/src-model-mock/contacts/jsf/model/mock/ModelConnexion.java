package contacts.jsf.model.mock;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import contacts.commun.dto.DtoCompte;
import contacts.jsf.model.IModelCompte;
import contacts.jsf.model.IModelConnexion;
import contacts.jsf.model.IModelInfo;
import contacts.jsf.util.UtilJsf;


@SuppressWarnings("serial")
@ManagedBean
@SessionScoped
public class ModelConnexion implements Serializable, IModelConnexion {
	
	
	// Données de la vue
	
	private String	pseudo;
	private String	motDePasse;
	
	
	// Autres champs
	
	private DtoCompte	compteConnecte;
	
	@ManagedProperty( "#{modelInfo}")
	private IModelInfo	modelInfo;
	
	@ManagedProperty( "#{modelCompte}")
	private IModelCompte	modelCompte;

	
	// Getters & setters
	
	@Override
	public String getPseudo() {
		return pseudo;
	}

	@Override
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	@Override
	public String getMotDePasse() {
		return motDePasse;
	}

	@Override
	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}
	
	@Override
	public DtoCompte getCompteConnecte() {
		return compteConnecte;
	}

	
	// Injecteurs
	
	public void setModelInfo( final IModelInfo modelInfo) {
		this.modelInfo = modelInfo;
	}

	public void setModelCompte( final IModelCompte modelCompte ) {
		this.modelCompte = modelCompte;
	}
	
	
	// Actions
	
	@Override
	public String connect() {

		compteConnecte =  null ;
		for ( DtoCompte compte : modelCompte.getComptes() ) {
			if ( compte.getPseudo().equals( pseudo )
					&& compte.getMotDePasse().equals( motDePasse) ) {
				compteConnecte = compte ;
			}
		}
		
		if ( compteConnecte != null ) {
			modelInfo.setTitre("Bienvenue");
			modelInfo.setTexte( "Vous êtes connecté en tant que '" + pseudo + "'.");
			return "info";
		} else {
			UtilJsf.genererMessageErreur( "Pseudo ou mot de passe invalide." );
			return null;
		}
		
	}
	
	
	@Override
	public String disconnect() {
		UtilJsf.sessionInvalidate();
		return "connexion";
	}
	

}
