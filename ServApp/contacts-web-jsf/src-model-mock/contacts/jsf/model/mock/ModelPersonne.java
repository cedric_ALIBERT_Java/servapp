package contacts.jsf.model.mock;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import contacts.commun.dto.DtoCategorie;
import contacts.commun.dto.DtoPersonne;
import contacts.commun.dto.DtoTelephone;
import contacts.jsf.model.IModelCategorie;
import contacts.jsf.model.IModelPersonne;
import contacts.jsf.util.IMapperDto;


@SuppressWarnings("serial")
@ManagedBean
@SessionScoped
public class ModelPersonne implements Serializable, IModelPersonne {

	
	// Données des vues
	
	private List<DtoPersonne> 	personnes;
	
	private DtoPersonne			personneVue;
	
	private DtoPersonne			personneCourant;

	
	// Autre champs
	
	private final IMapperDto	mapper = IMapperDto.INSTANCE;

	@ManagedProperty(value="#{modelCategorie}")
	private IModelCategorie				modelCategorie;

	private int dernierIdTelephone;
	
	
	
	// Getters & setters
	
	@Override
	public List<DtoPersonne> getPersonnes() {
		return personnes;
	}
	
	@Override
	public DtoPersonne getPersonneVue() {
		return personneVue;
	}

	
	// Injecteurs
	
	public void setModelCategorie(IModelCategorie modelCategorie) {
		this.modelCategorie = modelCategorie;
	}
	
	
	// Actions
	
	@Override
	public String actualiserListe() {
		trierListe();
		return null;
	}
	
	
	@Override
	public String preparerAjouter() {
		personneCourant = null;
		personneVue = new DtoPersonne();
		return "formulaire";
	}
	
	@Override
	public String preparerModifier( DtoPersonne personne ) {
		personneCourant = personne;
		personneVue = mapper.duplicate( personne );
		return "formulaire";
	}

	
	@Override
	public String validerMiseAJour() {
		if( personneCourant == null ) {
        	personneVue.setId(getIdSuivant());
        	affecterIdTelephones(personneVue);
			personnes.add(personneVue);
		} else {
        	affecterIdTelephones(personneVue);
			mapper.update( personneVue, personneCourant );
		}
		return "liste";
	}
	
	@Override
	public String supprimer( DtoPersonne personne ) {
		personnes.remove(personne);
		return null;
	}
	
	
	
	@Override
	public String ajouterTelephone() {
		personneVue.getTelephones().add( new DtoTelephone() );
		return null;
	}
	
	
	@Override
	public String supprimerTelephone( DtoTelephone telephone ) {

		for ( int i = personneVue.getTelephones().size() - 1; i >= 0; i-- ) {
			if ( telephone == personneVue.getTelephones().get(i) ) {
				personneVue.getTelephones().remove(i);
				break;
			}
		}
		return null;
	}
    
    
    // Méthodes auxiliaires
    
    private void trierListe() {
		Collections.sort( personnes,
            (Comparator<DtoPersonne>) ( item1, item2 ) -> {
                int resultat = item1.getNom().toUpperCase().compareTo(item2.getNom().toUpperCase());
                return (resultat != 0 ? resultat : item1.getPrenom().toUpperCase().compareTo(item2.getPrenom().toUpperCase()));
		});
    }
    
    
    @PostConstruct
	private void initialiserDonnees() {
    	
    	personnes = new ArrayList<>();
		
		DtoCategorie categorie1 = modelCategorie.getCategories().get(0);
		DtoCategorie categorie2 = modelCategorie.getCategories().get(1);

    	DtoPersonne personne; 
		
		personne = new DtoPersonne( 1, "VERLAINE", "Paul", categorie1 );
		personne.getTelephones().add(new DtoTelephone( 11, "Portable", "06 11 11 11 11" ) );
		personne.getTelephones().add(new DtoTelephone( 12, "Domicile", "05 55 11 11 11" ) );
		personne.getTelephones().add(new DtoTelephone( 13, "Travail", "05 55 99 11 11" ) );
		personnes.add( personne );
		
		personne = new DtoPersonne( 2, "MONET", "Claude", categorie2 );
		personne.getTelephones().add(new DtoTelephone( 21, "Portable", "06 22 22 22 22" ) );
		personne.getTelephones().add(new DtoTelephone( 22, "Domicile", "05 55 22 22 22" ) );
		personne.getTelephones().add(new DtoTelephone( 23, "Travail", "05 55 99 22 22" ) );
		personnes.add( personne );
		
		personne = new DtoPersonne( 3, "HUGO", "Victor", categorie1 );
		personne.getTelephones().add(new DtoTelephone( 31, "Portable", "06 33 33 33 33" ) );
		personne.getTelephones().add(new DtoTelephone( 32, "Domicile", "05 55 33 33 33" ) );
		personne.getTelephones().add(new DtoTelephone( 33, "Travail", "05 55 99 33 33" ) );
		personnes.add( personne );

		trierListe();
		dernierIdTelephone = 100;
	}

    
    private int getIdSuivant() {
    	DtoPersonne itemMax = Collections.max( personnes,
	            (Comparator<DtoPersonne>) ( item1, item2) -> {
	                return ( item1.getId() - item2.getId() );
			});
		if ( itemMax != null ) {
			return itemMax.getId() + 1;
		} else {
			return 1;
		}
    }

    
	private void affecterIdTelephones( DtoPersonne personne ) {
		for( DtoTelephone t : personne.getTelephones() ) {
			if ( t.getId() == 0 ) {
				t.setId( ++dernierIdTelephone );
			}
		}
	}
	
	
}
