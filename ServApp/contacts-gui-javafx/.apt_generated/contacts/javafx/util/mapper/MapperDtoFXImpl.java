package contacts.javafx.util.mapper;

import contacts.commun.dto.DtoCategorie;
import contacts.commun.dto.DtoCompte;
import contacts.commun.dto.DtoPersonne;
import contacts.commun.dto.DtoTelephone;
import contacts.javafx.fxb.FXCategorie;
import contacts.javafx.fxb.FXCompte;
import contacts.javafx.fxb.FXPersonne;
import contacts.javafx.fxb.FXTelephone;
import contacts.javafx.util.mapper.MapperDtoFX.FactoryObsservableList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javafx.collections.ObservableList;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2016-12-07T11:15:54+0100",
    comments = "version: 1.0.0.Final, compiler: Eclipse JDT (IDE) 1.2.100.v20160418-1457, environment: Java 1.8.0_101 (Oracle Corporation)"
)
public class MapperDtoFXImpl implements MapperDtoFX {

    private final FactoryObsservableList factoryObsservableList = new FactoryObsservableList();

    @Override
    public FXCompte duplicate(FXCompte source) {
        if ( source == null ) {
            return null;
        }

        FXCompte fXCompte = new FXCompte();

        fXCompte.setEmail( source.getEmail() );
        fXCompte.setId( source.getId() );
        fXCompte.setMotDePasse( source.getMotDePasse() );
        fXCompte.setPseudo( source.getPseudo() );
        if ( source.getRoles() != null ) {
            if ( fXCompte.getRoles() != null ) {
                Collection<String> targetCollection = source.getRoles();
                if ( targetCollection != null ) {
                    fXCompte.getRoles().addAll( targetCollection );
                }
            }
        }

        return fXCompte;
    }

    @Override
    public void update(FXCompte source, FXCompte target) {
        if ( source == null ) {
            return;
        }

        target.setEmail( source.getEmail() );
        target.setId( source.getId() );
        target.setMotDePasse( source.getMotDePasse() );
        target.setPseudo( source.getPseudo() );
        if ( source.getRoles() != null ) {
            if ( target.getRoles() != null ) {
                target.getRoles().clear();
                Collection<String> targetCollection = source.getRoles();
                if ( targetCollection != null ) {
                    target.getRoles().addAll( targetCollection );
                }
            }
        }
    }

    @Override
    public FXCompte map(DtoCompte source) {
        if ( source == null ) {
            return null;
        }

        FXCompte fXCompte = new FXCompte();

        fXCompte.setEmail( source.getEmail() );
        fXCompte.setId( source.getId() );
        fXCompte.setMotDePasse( source.getMotDePasse() );
        fXCompte.setPseudo( source.getPseudo() );
        if ( fXCompte.getRoles() != null ) {
            Collection<String> targetCollection = stringListToStringObservableList( source.getRoles() );
            if ( targetCollection != null ) {
                fXCompte.getRoles().addAll( targetCollection );
            }
        }

        return fXCompte;
    }

    @Override
    public DtoCompte map(FXCompte source) {
        if ( source == null ) {
            return null;
        }

        DtoCompte dtoCompte = new DtoCompte();

        dtoCompte.setEmail( source.getEmail() );
        dtoCompte.setId( source.getId() );
        dtoCompte.setMotDePasse( source.getMotDePasse() );
        dtoCompte.setPseudo( source.getPseudo() );
        if ( source.getRoles() != null ) {
            dtoCompte.setRoles( new ArrayList<String>( source.getRoles() ) );
        }

        return dtoCompte;
    }

    @Override
    public FXCategorie duplicate(FXCategorie source) {
        if ( source == null ) {
            return null;
        }

        FXCategorie fXCategorie = new FXCategorie();

        fXCategorie.setId( source.getId() );
        fXCategorie.setLibelle( source.getLibelle() );

        return fXCategorie;
    }

    @Override
    public void update(FXCategorie source, FXCategorie target) {
        if ( source == null ) {
            return;
        }

        target.setId( source.getId() );
        target.setLibelle( source.getLibelle() );
    }

    @Override
    public FXCategorie map(DtoCategorie source) {
        if ( source == null ) {
            return null;
        }

        FXCategorie fXCategorie = new FXCategorie();

        fXCategorie.setId( source.getId() );
        fXCategorie.setLibelle( source.getLibelle() );

        return fXCategorie;
    }

    @Override
    public DtoCategorie map(FXCategorie source) {
        if ( source == null ) {
            return null;
        }

        DtoCategorie dtoCategorie = new DtoCategorie();

        if ( source.getId() != null ) {
            dtoCategorie.setId( source.getId() );
        }
        dtoCategorie.setLibelle( source.getLibelle() );

        return dtoCategorie;
    }

    @Override
    public FXPersonne duplicate(FXPersonne source) {
        if ( source == null ) {
            return null;
        }

        FXPersonne fXPersonne = new FXPersonne();

        fXPersonne.setCategorie( duplicate( source.getCategorie() ) );
        fXPersonne.setId( source.getId() );
        fXPersonne.setNom( source.getNom() );
        fXPersonne.setPrenom( source.getPrenom() );
        if ( source.getTelephones() != null ) {
            if ( fXPersonne.getTelephones() != null ) {
                Collection<FXTelephone> targetCollection = source.getTelephones();
                if ( targetCollection != null ) {
                    fXPersonne.getTelephones().addAll( targetCollection );
                }
            }
        }

        return fXPersonne;
    }

    @Override
    public void update(FXPersonne source, FXPersonne target) {
        if ( source == null ) {
            return;
        }

        target.setId( source.getId() );
        target.setNom( source.getNom() );
        target.setPrenom( source.getPrenom() );
        if ( source.getTelephones() != null ) {
            if ( target.getTelephones() != null ) {
                target.getTelephones().clear();
                Collection<FXTelephone> targetCollection = source.getTelephones();
                if ( targetCollection != null ) {
                    target.getTelephones().addAll( targetCollection );
                }
            }
        }

        target.setCategorie( source.getCategorie() );
    }

    @Override
    public FXPersonne map(DtoPersonne source) {
        if ( source == null ) {
            return null;
        }

        FXPersonne fXPersonne = new FXPersonne();

        fXPersonne.setCategorie( map( source.getCategorie() ) );
        fXPersonne.setId( source.getId() );
        fXPersonne.setNom( source.getNom() );
        fXPersonne.setPrenom( source.getPrenom() );
        if ( fXPersonne.getTelephones() != null ) {
            Collection<FXTelephone> targetCollection = dtoTelephoneListToFXTelephoneObservableList( source.getTelephones() );
            if ( targetCollection != null ) {
                fXPersonne.getTelephones().addAll( targetCollection );
            }
        }

        return fXPersonne;
    }

    @Override
    public DtoPersonne map(FXPersonne source) {
        if ( source == null ) {
            return null;
        }

        DtoPersonne dtoPersonne = new DtoPersonne();

        dtoPersonne.setCategorie( map( source.getCategorie() ) );
        dtoPersonne.setId( source.getId() );
        dtoPersonne.setNom( source.getNom() );
        dtoPersonne.setPrenom( source.getPrenom() );
        dtoPersonne.setTelephones( fXTelephoneObservableListToDtoTelephoneList( source.getTelephones() ) );

        return dtoPersonne;
    }

    @Override
    public void update(DtoPersonne source, FXPersonne target) {
        if ( source == null ) {
            return;
        }

        target.setCategorie( map( source.getCategorie() ) );
        target.setId( source.getId() );
        target.setNom( source.getNom() );
        target.setPrenom( source.getPrenom() );
        if ( target.getTelephones() != null ) {
            target.getTelephones().clear();
            Collection<FXTelephone> targetCollection = dtoTelephoneListToFXTelephoneObservableList( source.getTelephones() );
            if ( targetCollection != null ) {
                target.getTelephones().addAll( targetCollection );
            }
        }
    }

    @Override
    public FXTelephone duplicate(FXTelephone source) {
        if ( source == null ) {
            return null;
        }

        FXTelephone fXTelephone = new FXTelephone();

        fXTelephone.setId( source.getId() );
        fXTelephone.setLibelle( source.getLibelle() );
        fXTelephone.setNumero( source.getNumero() );

        return fXTelephone;
    }

    @Override
    public void update(FXTelephone source, FXTelephone target) {
        if ( source == null ) {
            return;
        }

        target.setId( source.getId() );
        target.setLibelle( source.getLibelle() );
        target.setNumero( source.getNumero() );
    }

    @Override
    public FXTelephone map(DtoTelephone source) {
        if ( source == null ) {
            return null;
        }

        FXTelephone fXTelephone__ = new FXTelephone();

        fXTelephone__.setId( source.getId() );
        fXTelephone__.setLibelle( source.getLibelle() );
        fXTelephone__.setNumero( source.getNumero() );

        return fXTelephone__;
    }

    @Override
    public DtoTelephone map(FXTelephone source) {
        if ( source == null ) {
            return null;
        }

        DtoTelephone dtoTelephone_ = new DtoTelephone();

        dtoTelephone_.setId( source.getId() );
        dtoTelephone_.setLibelle( source.getLibelle() );
        dtoTelephone_.setNumero( source.getNumero() );

        return dtoTelephone_;
    }

    protected ObservableList<String> stringListToStringObservableList(List<String> list) {
        if ( list == null ) {
            return null;
        }

        ObservableList<String> observableList = factoryObsservableList.createObsList();
        for ( String string : list ) {
            observableList.add( string );
        }

        return observableList;
    }

    protected ObservableList<FXTelephone> dtoTelephoneListToFXTelephoneObservableList(List<DtoTelephone> list) {
        if ( list == null ) {
            return null;
        }

        ObservableList<FXTelephone> observableList = factoryObsservableList.createObsList();
        for ( DtoTelephone dtoTelephone : list ) {
            observableList.add( map( dtoTelephone ) );
        }

        return observableList;
    }

    protected List<DtoTelephone> fXTelephoneObservableListToDtoTelephoneList(ObservableList<FXTelephone> observableList) {
        if ( observableList == null ) {
            return null;
        }

        List<DtoTelephone> list = factoryObsservableList.createObsList();
        for ( FXTelephone fXTelephone : observableList ) {
            list.add( map( fXTelephone ) );
        }

        return list;
    }
}
