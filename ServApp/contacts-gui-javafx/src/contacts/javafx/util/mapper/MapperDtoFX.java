package contacts.javafx.util.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import contacts.commun.dto.DtoCategorie;
import contacts.commun.dto.DtoCompte;
import contacts.commun.dto.DtoPersonne;
import contacts.commun.dto.DtoTelephone;
import contacts.javafx.fxb.FXCategorie;
import contacts.javafx.fxb.FXCompte;
import contacts.javafx.fxb.FXPersonne;
import contacts.javafx.fxb.FXTelephone;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
   

@Mapper( uses=MapperDtoFX.FactoryObsservableList.class  )
public interface MapperDtoFX { 
	
	MapperDtoFX INSTANCE = Mappers.getMapper( MapperDtoFX.class );
	
	
	// Compte
	
	FXCompte duplicate( FXCompte source );
	
	void update( FXCompte source, @MappingTarget FXCompte target );
	
	FXCompte map( DtoCompte source );
	
	DtoCompte map( FXCompte source );
	
	
	// Categorie
	
	FXCategorie duplicate( FXCategorie source );
	
	void update( FXCategorie source, @MappingTarget FXCategorie target );
	
	FXCategorie map( DtoCategorie source );
	
	DtoCategorie map( FXCategorie source );
	
	
	// Personne
	
	FXPersonne duplicate( FXPersonne source );
	
    @Mapping( target="categorie", expression="java( source.getCategorie() )" )
    void update( FXPersonne source, @MappingTarget FXPersonne target );
	
    FXPersonne map( DtoPersonne source );
	
	DtoPersonne map( FXPersonne source );

	void update( DtoPersonne source, @MappingTarget FXPersonne target );
	
	
	// Telephone
	
	FXTelephone duplicate( FXTelephone source );
	
	void update( FXTelephone source, @MappingTarget FXTelephone target );
	
    FXTelephone map( DtoTelephone source );
	
    DtoTelephone map( FXTelephone source );
	
	
    // Classe auxiliaire
    
    public static class FactoryObsservableList {
    	
    	<T> ObservableList<T> createObsList() {
    		return FXCollections.observableArrayList();
    	}

    }
    
    
    
}
	
	
	
