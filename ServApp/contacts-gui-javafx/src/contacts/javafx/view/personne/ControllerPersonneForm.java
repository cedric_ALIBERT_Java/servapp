package contacts.javafx.view.personne;

import contacts.javafx.fxb.FXCategorie;
import contacts.javafx.fxb.FXPersonne;
import contacts.javafx.fxb.FXTelephone;
import contacts.javafx.model.IModelCategorie;
import contacts.javafx.model.IModelPersonne;
import contacts.javafx.view.EnumView;
import contacts.javafx.view.IController;
import contacts.javafx.view.IManagerGui;
import contacts.javafx.view.util.EditingCell;
import contacts.javafx.view.util.StringBindingId;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;


public class ControllerPersonneForm implements IController {
	
	
	// Composants de la vue
	
	@FXML
	private TextField			textFieldId;
	@FXML
	private TextField			textFieldNom;
	@FXML	
	private TextField			textFieldPrenom;
    @FXML
    private ComboBox<FXCategorie>	comboBoxCategorie;
	@FXML
	private TableView<FXTelephone>	tableViewTelphones;
	@FXML
	private TableColumn<FXTelephone, Number> columnId;
	@FXML
	private TableColumn<FXTelephone, String> columnLibelle;
	@FXML
	private TableColumn<FXTelephone, String> columnNumero;
	

	
	// Champs
	private IManagerGui			managerGui;
	private IModelPersonne		modelPersonne;
    private IModelCategorie 	modelCategorie;
	
	
	// Actions
	
	@FXML
	private void doValider() {
		try {
			modelPersonne.ValiderMiseAJour();
			managerGui.showView( EnumView.PersonneListe );
		} catch (Exception e) {
			managerGui.afficherErreur( e );
		}
	}
	
	@FXML
	private void doAnnuler() {
		managerGui.showView( EnumView.PersonneListe );
	}
	
	@FXML
	private void doAjouterTelephone() {
		modelPersonne.ajouterTelephone();
	}
	
	
	@FXML
	private void doiSupprimerTelephone() {
		FXTelephone telephone = tableViewTelphones.getSelectionModel().getSelectedItem();
		try {
			modelPersonne.supprimerTelephone(telephone);
		} catch (Exception e) {
			managerGui.afficherErreur(e);
		}
	}
	
	
	// Initialisations
	
	@Override
	public void setManagerGui(IManagerGui managerGui) throws Exception {
		
		// Injection de dépendances
		this.managerGui = managerGui;
		modelPersonne = managerGui.getModel( IModelPersonne.class );
		modelCategorie = managerGui.getModel( IModelCategorie.class );
		
		// Champs simples
		FXPersonne personneVue = modelPersonne.getPersonneVue();
		textFieldId.textProperty().bind( new StringBindingId( personneVue.idProperty() ));
		textFieldNom.textProperty().bindBidirectional( personneVue.nomProperty() );
		textFieldPrenom.textProperty().bindBidirectional( personneVue.prenomProperty() );

        
		// Configuration de la combo box

		// Data binding
        comboBoxCategorie.valueProperty().bindBidirectional( personneVue.categorieProperty() );
		comboBoxCategorie.setItems( modelCategorie.getCategories() );
 		
		
		// Configuration du TableView

		// Data binding
		tableViewTelphones.setItems(  modelPersonne.getPersonneVue().getTelephones() );
		
//		columnId.setCellValueFactory( new PropertyValueFactory<FxPersonne, Integer>("id"));
		columnId.setCellValueFactory( t -> t.getValue().idProperty() );
		columnLibelle.setCellValueFactory( t -> t.getValue().libelleProperty() );
		columnNumero.setCellValueFactory( t -> t.getValue().numeroProperty() );

		columnLibelle.setCellFactory(  p -> new EditingCell<>() );
		columnNumero.setCellFactory(  p -> new EditingCell<>() );		
	
	}
    
}
