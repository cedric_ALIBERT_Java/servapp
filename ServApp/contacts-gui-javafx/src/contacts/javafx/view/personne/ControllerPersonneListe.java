package contacts.javafx.view.personne;

import contacts.javafx.fxb.FXPersonne;
import contacts.javafx.model.IModelPersonne;
import contacts.javafx.view.EnumView;
import contacts.javafx.view.IController;
import contacts.javafx.view.IManagerGui;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;


public class ControllerPersonneListe implements IController {
	
	
	// Composants de la vue
	
	@FXML
	private ListView<FXPersonne> listView;
	@FXML
	private Button				buttonModfier;
	@FXML
	private Button				buttonSupprimer;
	
	
	// Autres champs
	
	private IManagerGui			managerGui;
	private IModelPersonne		modelPersonne;
	
	
	// Actions
	
	@FXML
	private void doActualiser() {
		try {
			modelPersonne.actualiserListe();
			listView.getSelectionModel().clearSelection();;
		} catch (Exception e) {
			managerGui.afficherErreur(e);
		}
	}
	
	@FXML
	private void doAjouter() {
		managerGui.showView( EnumView.PersonneForm );
		modelPersonne.preparerAjouter();
	}
	
	@FXML
	private void doModifier() {
		managerGui.showView( EnumView.PersonneForm );
		FXPersonne p = listView.getSelectionModel().getSelectedItem();
		modelPersonne.preparerModifier(p);
	}
	
	@FXML
	private void doSupprimer() {
		
		if ( managerGui.demanderConfirmation("Etes-vous sûr de voulir supprimer cette personne ?" ) ) {
			FXPersonne p = listView.getSelectionModel().getSelectedItem();
			try {
				modelPersonne.supprimer(p);
			} catch (Exception e) {
				managerGui.afficherErreur(e);
			}
		}
	}
	
	
	// Gestion des évènements

	// Clic sur la liste
	@FXML
	private void gererClicSurListe( MouseEvent event ) {
		if (event.getButton().equals(MouseButton.PRIMARY)) {
			if (event.getClickCount() == 2) {
				doModifier();
			}
		}
	}
	
	
	// Initialisations

	@Override
	public void setManagerGui(IManagerGui managerGui) throws Exception {
		
		// Injection de dépendances
		this.managerGui = managerGui;
		modelPersonne = managerGui.getModel( IModelPersonne.class );
		
		// Data binding
		listView.setItems( modelPersonne.getPersonnes() );
		
		// Configuraiton des boutons
		listView.getSelectionModel().getSelectedItems().addListener( 
		        (ListChangeListener<FXPersonne>) (c) -> {
		        	configurerBoutons();
		});
    	configurerBoutons();
		
		// Comportement si changement du contenu
		listView.getItems().addListener( (ListChangeListener<FXPersonne>) (c) -> {
			c.next();
			// Après insertion d'un élément, le sélectionne
			// Après suppression d'un élément, sélectionne le suivant
			if ( c.wasAdded() || c.wasRemoved() ) {
				listView.getSelectionModel().clearSelection();
				listView.getSelectionModel().select( c.getFrom());
				listView.getFocusModel().focus(c.getFrom());
				listView.scrollTo( c.getFrom());
				listView.requestFocus();
			}
		});
	}

	
	// Méthodes auxiliaires
	
	private void configurerBoutons() {
    	if( listView.getSelectionModel().getSelectedItem() == null ) {
    		buttonModfier.setDisable(true);
    		buttonSupprimer.setDisable(true);
    	} else {
    		buttonModfier.setDisable(false);
    		buttonSupprimer.setDisable(false);
    	}
	}
	
}
