package contacts.javafx.view.personne;

import contacts.javafx.fxb.FXCategorie;
import contacts.javafx.model.IModelCategorie;
import contacts.javafx.view.EnumView;
import contacts.javafx.view.IController;
import contacts.javafx.view.IManagerGui;
import contacts.javafx.view.util.StringBindingId;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;


public class ControllerCategorieForm implements IController {

	
	// Composants de la vue
	
	@FXML
	private TextField			textFieldId;
	@FXML
	private TextField			textFieldLibelle;


	// Autres champs
	
	private IManagerGui			managerGui;
	private IModelCategorie		modelCategorie;
	
	
	
	// Actions
	
	@FXML
	private void doAnnuler() {
		managerGui.showView( EnumView.CategorieListe );
	}
	
	@FXML
	private void doValider()  {
		try {
			modelCategorie.validerMiseAJour();
			managerGui.showView( EnumView.CategorieListe );
		} catch (Exception e) {
			managerGui.afficherErreur( e );
		}
	}
	


	// Initialisation du Controller
	
	public void setManagerGui( IManagerGui managerGui) throws Exception {

		// Injection  de dépendances
		this.managerGui = managerGui;
		modelCategorie = managerGui.getModel( IModelCategorie.class );

		// Data binding
		
		FXCategorie categorieVue = modelCategorie.getCategorieVue();
		textFieldId.textProperty().bind( new StringBindingId( categorieVue.idProperty() ) );
		textFieldLibelle.textProperty().bindBidirectional( categorieVue.libelleProperty()  );
		
	}

}
