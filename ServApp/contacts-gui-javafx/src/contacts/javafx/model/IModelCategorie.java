package contacts.javafx.model;

import contacts.javafx.fxb.FXCategorie;
import javafx.collections.ObservableList;

public interface IModelCategorie {

	ObservableList<FXCategorie> getCategories();

	FXCategorie getCategorieVue();

	void actualiserListe() throws Exception;

	void preparerAjouter();

	void preparerModifier(FXCategorie categorie);

	void validerMiseAJour() throws Exception;

	void supprimer(FXCategorie categorie) throws Exception;

}