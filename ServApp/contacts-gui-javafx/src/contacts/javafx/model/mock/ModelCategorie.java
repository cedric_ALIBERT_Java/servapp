package contacts.javafx.model.mock;

import static contacts.javafx.model.EnumModeVue.CREER;
import static contacts.javafx.model.EnumModeVue.MODIFIER;

import java.util.Collections;
import java.util.Comparator;

import contacts.javafx.fxb.FXCategorie;
import contacts.javafx.model.EnumModeVue;
import contacts.javafx.model.IModelCategorie;
import contacts.javafx.util.mapper.MapperDtoFX;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


public class ModelCategorie implements IModelCategorie  {
	
	
	// Données observables 
	
	private final ObservableList<FXCategorie> categories = FXCollections.observableArrayList( 
			g ->  new Observable[] { g.libelleProperty()  } 
		);
	
	private final FXCategorie	categorieVue = new FXCategorie();
	
	
	// Objet courant

	private FXCategorie			categorieCourant;
    private EnumModeVue			modeVue;

	
	// Autres champs
	private MapperDtoFX		mapper;
	
	
	// Getters 
	
	@Override
	public ObservableList<FXCategorie> getCategories() {
		return categories;
	}
	
	@Override
	public FXCategorie getCategorieVue() {
		return categorieVue;
	}
	
	
	// Injecteurs
	
	public void setMapper(MapperDtoFX mapper) {
		this.mapper = mapper;
	}
	
	
	// Initialisation
	
	public ModelCategorie() {
		initialiserDonnees();
	}
	
	
	// Actualisations
	
	
	// Actualise la liste des CAT2GORIES
	@Override
	public void actualiserListe() throws Exception  {
        trierListe();
 	}


	// Actions
	
	@Override
	public void preparerAjouter() {
        modeVue = CREER;
		mapper.update( new FXCategorie(), categorieVue );		
	}
	
	@Override
	public void preparerModifier( FXCategorie categorie ) {
        modeVue = MODIFIER;
		categorieCourant = categorie;
		mapper.update( categorie, categorieVue );		
	}
	
	
	@Override
	public void validerMiseAJour() throws Exception  {
		
        if ( modeVue == CREER ) {
			categorieVue.setId( getIdSuivant() );
			categorieCourant = mapper.duplicate( categorieVue );
			categories.add(categorieCourant);

        } else if ( modeVue == MODIFIER ) {
			mapper.update( categorieVue, categorieCourant );		
		}
		
		trierListe();
	}
	
	
	@Override
	public void supprimer( FXCategorie categorie ) throws Exception {
		categories.remove(categorie);
	}

	
	// Méthodes auxiliaires
    
    private void trierListe() {
		FXCollections.sort( categories,
	            (Comparator<FXCategorie>) ( item1, item2) -> {
	                return ( item1.getLibelle().toUpperCase().compareTo( item2.getLibelle().toUpperCase() ) );
			});
    }
    
    private int getIdSuivant() {
    	if ( categories.isEmpty() ) {
    		return 1;
    	} else {
        	FXCategorie itemMax = Collections.max( categories,
	            (Comparator<FXCategorie>) ( item1, item2) -> {
	                return ( item1.getId() - item2.getId()  );
    		});
   			return itemMax.getId() + 1;
    	}
    }
	
	private void initialiserDonnees() {
		FXCategorie categorie;
    	categorie =  new FXCategorie( 1, "Ecrivains" );
    	categories.add( categorie );
    	categorie =  new FXCategorie( 2, "Peintres" );
    	categories.add( categorie );
		trierListe();
	}
	
}
