package contacts.javafx.model.mock;


import static contacts.javafx.model.EnumModeVue.CREER;
import static contacts.javafx.model.EnumModeVue.MODIFIER;

import java.util.Collections;
import java.util.Comparator;

import contacts.commun.util.Roles;
import contacts.javafx.fxb.FXCompte;
import contacts.javafx.model.EnumModeVue;
import contacts.javafx.model.IModelCompte;
import contacts.javafx.util.mapper.MapperDtoFX;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


public class ModelCompte implements IModelCompte {
	
	
	// Données pour les vues 
	
	private final ObservableList<FXCompte> comptes = FXCollections.observableArrayList( 
			c ->  new Observable[] { c.pseudoProperty()  } 
		);
	
	private final FXCompte         compteVue = new FXCompte();
	
	
	// Objet courant

	private FXCompte               compteCourant;
    private EnumModeVue            modeVue;
	
	
	// Autres champs
	private MapperDtoFX			mapper;
	
	
	// Getters 
	
	@Override
	public ObservableList<FXCompte> getComptes() {
		return comptes;
	}

	@Override
	public FXCompte getCompteVue() {
		return compteVue;
	}
	
	
	// Injecteurs
	
	public void setMapper(MapperDtoFX mapper) {
		this.mapper = mapper;
	}
	
	
	// Constructeur
	
	public ModelCompte() {
		initialiserDonnees();
	}
	
	
	// Actualisations
	
	// Actualise la liste 
	@Override
	public void actualiserListe() throws Exception {
        trierListe();
 	}
	
	
	// Actions
	
	@Override
	public void preparerAjouter() {
        modeVue = CREER;
		mapper.update( new FXCompte(), compteVue );		
	}
	
	@Override
	public void preparerModifier( FXCompte compte ) {
        modeVue = MODIFIER;
		compteCourant = compte;
		mapper.update( compte, compteVue );	
	}
	
	
	@Override
	public void validerMiseAJour() throws Exception  {
		
		// Effectue la mise à jour
        if ( modeVue == CREER ) {
        	compteVue.setId( getIdSuivant() );
            compteCourant = mapper.duplicate( compteVue );
			comptes.add( compteCourant );
		}
        if ( modeVue == MODIFIER ) {
			mapper.update( compteVue, compteCourant );		
		}

        // Trie la liste
        trierListe();
	}
	
	
	@Override
	public void supprimer( FXCompte compte ) throws Exception  {
		comptes.remove( compte );
	}
    
    
    // Méthodes auxiliaires
    
    private void trierListe() {
		FXCollections.sort( comptes,
            (Comparator<FXCompte>) ( item1, item2) -> {
                return ( item1.pseudoProperty().get().toUpperCase().compareTo(item2.pseudoProperty().get().toUpperCase()));
		});
    }
    
    
    private int getIdSuivant() {
    	if ( comptes.isEmpty() ) {
    		return 1;
    	} else {
    		FXCompte itemMax = Collections.max( comptes,
	            (Comparator<FXCompte>) ( item1, item2) -> {
	                return ( item1.getId() - item2.getId() );
			});
   			return itemMax.getId() + 1;
    	}
    }

	
	private void initialiserDonnees() {
		
		FXCompte compte;
		compte = new FXCompte( 1, "geek", "geek", "geek@3il.fr" );
		compte.getRoles().add( Roles.ADMINISTRATEUR  );
		compte.getRoles().add( Roles.UTILISATEUR  );
		comptes.add( compte );

		compte = new FXCompte(2, "chef", "chef", "chef@3il.fr");
		compte.getRoles().add( Roles.UTILISATEUR  );
		comptes.add( compte );
		
		compte = new FXCompte( 3, "job", "job", "job@3il.fr" );
		compte.getRoles().add( Roles.UTILISATEUR  );
		comptes.add( compte );

		compte = new FXCompte(4, "_" + this.getClass().getPackage().getName(), "xxx", "xxx@3il.fr");
		compte.getRoles().add( Roles.UTILISATEUR  );
		comptes.add( compte );
		
		trierListe();
	}
    
}
