package contacts.javafx.model.mock;

import static contacts.javafx.model.EnumModeVue.CREER;
import static contacts.javafx.model.EnumModeVue.MODIFIER;

import java.util.Collections;
import java.util.Comparator;

import contacts.commun.util.ExceptionValidation;
import contacts.javafx.fxb.FXCategorie;
import contacts.javafx.fxb.FXPersonne;
import contacts.javafx.fxb.FXTelephone;
import contacts.javafx.model.EnumModeVue;
import contacts.javafx.model.IModelCategorie;
import contacts.javafx.model.IModelPersonne;
import contacts.javafx.util.mapper.MapperDtoFX;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


public class ModelPersonne implements IModelPersonne {
	
	
	// Données observables 
	
	private final ObservableList<FXPersonne> personnes = FXCollections.observableArrayList( 
			p ->  new Observable[] { p.nomProperty(), p.prenomProperty() } 
		);
	
	private final FXPersonne	personneVue = new FXPersonne();
	
	
	// Objet courant
	
	private FXPersonne     		personneCourant;
    private EnumModeVue         modeVue;
	
	
	// Autres champs
	
	private MapperDtoFX		mapper;
	private IModelCategorie		modelCategorie;

	private int dernierIdTelephone;
	
	
	// Getters
	
	@Override
	public ObservableList<FXPersonne> getPersonnes() {
		return personnes;
	}
	
	@Override
	public FXPersonne getPersonneVue() {
		return personneVue;
	}
	
	
	// Injecteurs
	
	public void setMapper(MapperDtoFX mapper) {
		this.mapper = mapper;
	}
	
	public void setModelCategorie(IModelCategorie modelCategorie) {
		this.modelCategorie = modelCategorie;
		initialiserDonnees();
	}

	
	// Actualisations
	
	@Override
	public void actualiserListe() throws Exception  {
        trierListe();
	}

	
	// Actions
	
	@Override
	public void preparerAjouter() {
        modeVue = CREER;
		personneCourant = new FXPersonne();
		mapper.update( personneCourant, personneVue );	
	}
	
	@Override
	public void preparerModifier( FXPersonne personne ) {
        modeVue = MODIFIER;
		personneCourant = personne;
		mapper.update( personneCourant, personneVue );	
	}
	
	@Override
	public void ValiderMiseAJour() throws Exception {
		
		String nom = personneVue.nomProperty().get();
		String prenom = personneVue.prenomProperty().get();
		
		StringBuilder message = new StringBuilder();
		if( nom == null || nom.isEmpty() ) {
			message.append( "\nLe nom ne doit pas être vide." );
		} else  if ( nom.length()> 25 ) {
			message.append( "\nLe nom est trop long." );
		}
		if( prenom == null || prenom.isEmpty() ) {
			message.append( "\nLe prénom ne doit pas être vide." );
		} else if ( prenom.length()> 25 ) {
			message.append( "\nLe prénom est trop long." );
		}
		
		if ( message.length() > 0 ) {
			throw new ExceptionValidation( message.toString().substring(1) );
		}
		
		// Test si c'est un ajout ou une modificaiton
        if ( modeVue == CREER ) {
        	personneVue.setId(getIdSuivant());
        	affecterIdTelephones(personneVue);
        	mapper.update(personneVue, personneCourant);
			personnes.add(personneCourant);
		}
        if ( modeVue == MODIFIER ) {
        	affecterIdTelephones(personneVue);
			mapper.update( personneVue, personneCourant );		
		}

        // Trie la liste
        trierListe();
	}
	
	@Override
	public void supprimer( FXPersonne personne ) throws Exception {
		personnes.remove(personne);
	}
	
	@Override
	public void ajouterTelephone() {
		personneVue.getTelephones().add( new FXTelephone() );
	}
	
	@Override
	public void supprimerTelephone( FXTelephone telephone )  {
		personneVue.getTelephones().remove( telephone );
	}
    
    
    // Méthodes auxiliaires
    
    private void trierListe() {
		FXCollections.sort( personnes,
            (Comparator<FXPersonne>) ( item1, item2) -> {
                int resultat = item1.getNom().toUpperCase().compareTo(item2.getNom().toUpperCase());
                return (resultat != 0 ? resultat : item1.getPrenom().toUpperCase().compareTo(item2.getPrenom().toUpperCase()));
		});
    }
    
    private int getIdSuivant() {
    	if ( personnes.isEmpty() ) {
    		return 1;
    	} else {
        	FXPersonne itemMax = Collections.max( personnes,
	            (Comparator<FXPersonne>) ( item1, item2) -> {
	                return ( item1.getId() - item2.getId()  );
    		});
    		return itemMax.getId() + 1;
    	}
    }
	
	private void affecterIdTelephones( FXPersonne personne ) {
		for( FXTelephone t : personne.getTelephones() ) {
			if ( t.getId() == 0 ) {
				t.setId( ++dernierIdTelephone );
			}
		}
	}

	
	public void initialiserDonnees() {
		
		FXCategorie categorie1 = modelCategorie.getCategories().get(0);
		FXCategorie categorie2 = modelCategorie.getCategories().get(1);

    	FXPersonne personne; 
		
		personne = new FXPersonne( 1, "VERLAINE", "Paul", categorie1 );
		personne.getTelephones().add(new FXTelephone( 11, "Portable", "06 11 11 11 11" ) );
		personne.getTelephones().add(new FXTelephone( 12, "Domicile", "05 55 11 11 11" ) );
		personne.getTelephones().add(new FXTelephone( 13, "Travail", "05 55 99 11 11" ) );
		personnes.add( personne );
		
		personne = new FXPersonne( 2, "MONET", "Claude", categorie2 );
		personne.getTelephones().add(new FXTelephone( 21, "Portable", "06 22 22 22 22" ) );
		personne.getTelephones().add(new FXTelephone( 22, "Domicile", "05 55 22 22 22" ) );
		personne.getTelephones().add(new FXTelephone( 23, "Travail", "05 55 99 22 22" ) );
		personnes.add( personne );
		
		personne = new FXPersonne( 3, "HUGO", "Victor", categorie1 );
		personne.getTelephones().add(new FXTelephone( 31, "Portable", "06 33 33 33 33" ) );
		personne.getTelephones().add(new FXTelephone( 32, "Domicile", "05 55 33 33 33" ) );
		personne.getTelephones().add(new FXTelephone( 33, "Travail", "05 55 99 33 33" ) );
		personnes.add( personne );

		trierListe();
		dernierIdTelephone = 100;
	}
}
