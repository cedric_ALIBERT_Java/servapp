package contacts.javafx.model.standard;

import static contacts.javafx.model.EnumModeVue.CREER;
import static contacts.javafx.model.EnumModeVue.MODIFIER;

import java.util.Comparator;

import contacts.commun.dto.DtoCategorie;
import contacts.commun.service.IServiceCategorie;
import contacts.javafx.fxb.FXCategorie;
import contacts.javafx.model.EnumModeVue;
import contacts.javafx.model.IModelCategorie;
import contacts.javafx.util.mapper.MapperDtoFX;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


public class ModelCategorie implements IModelCategorie  {
	
	
	// Données observables 
	
	private final ObservableList<FXCategorie> categories = FXCollections.observableArrayList( 
			g ->  new Observable[] { g.libelleProperty()  } 
		);
	
	private final FXCategorie	categorieVue = new FXCategorie();
	
	
	// Objet courant

	private FXCategorie			categorieCourant;
    private EnumModeVue			modeVue;

	
	// Autres champs
	private MapperDtoFX		mapper;
	private IServiceCategorie	serviceCategorie;
	
	
	// Getters 
	
	@Override
	public ObservableList<FXCategorie> getCategories() {
		return categories;
	}
	
	@Override
	public FXCategorie getCategorieVue() {
		return categorieVue;
	}
	
	
	// Injecteurs
	
	public void setMapper(MapperDtoFX mapper) {
		this.mapper = mapper;
	}
	
	public void setServiceCategorie(IServiceCategorie serviceCategorie) {
		this.serviceCategorie = serviceCategorie;
	}
	
	
	// Actualisations
	
	@Override
	public void actualiserListe() throws Exception  {
		
		// Prépare la récupération de l'objet courant
		int idCourant = 0;
		if( categorieCourant != null ) {
			idCourant = categorieCourant.getId();
		}

		// Actualise la liste
		categories.clear();
		for( DtoCategorie dto : serviceCategorie.listerTout() ) {
			FXCategorie categorie = mapper.map( dto );
			categories.add( categorie );
			if( categorie.getId() == idCourant ) {
				categorieCourant = categorie;
			}
		}
 	}


	// Actions
	
	@Override
	public void preparerAjouter() {
        modeVue = CREER;
		mapper.update( new FXCategorie(), categorieVue );		
	}
	
	@Override
	public void preparerModifier( FXCategorie categorie ) {
        modeVue = MODIFIER;
		categorieCourant = categorie;
		mapper.update( categorie, categorieVue );		
	}
	
	
	@Override
	public void validerMiseAJour() throws Exception  {
		
		// Crée un objet contenant le données pour la mise à jour
		DtoCategorie dto = mapper.map( categorieVue );
		
		// Effectue la mise à jour
		switch( modeVue) {
		case CREER :
			int id = serviceCategorie.inserer( dto );
			categorieVue.setId(id);
			categorieCourant = mapper.duplicate(categorieVue);
			categories.add(categorieCourant);
			break;
		case MODIFIER :
			serviceCategorie.modifier( dto );
			mapper.update( categorieVue, categorieCourant );		
			break;
		default:
			break;
		}
		trierListe();
	}
	
	
	@Override
	public void supprimer( FXCategorie categorie ) throws Exception {
		serviceCategorie.supprimer( categorie.getId() );
		categories.remove(categorie);
	}
	
	
	// Initialisaiton
	
	public void refresh() throws Exception {
		actualiserListe();
	}

	
	// Méthodes auxiliaires
    
    private void trierListe() {
		FXCollections.sort( categories,
	            (Comparator<FXCategorie>) ( item1, item2) -> {
	                return ( item1.getLibelle().toUpperCase().compareTo( item2.getLibelle().toUpperCase() ) );
			});
    }
}
