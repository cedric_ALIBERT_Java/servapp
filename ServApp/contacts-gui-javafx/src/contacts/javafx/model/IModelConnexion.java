package contacts.javafx.model;

import contacts.javafx.fxb.FXCompte;
import javafx.beans.property.ObjectProperty;

public interface IModelConnexion {

	FXCompte getCompteVue();

	ObjectProperty<FXCompte> compteConnecteProperty();

	FXCompte getCompteConnecte();

	void ouvrirSessionUtilisateur() throws Exception;

	void fermerSessionUtilisateur() throws Exception;

}