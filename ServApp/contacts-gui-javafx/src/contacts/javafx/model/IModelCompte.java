package contacts.javafx.model;

import contacts.javafx.fxb.FXCompte;
import javafx.collections.ObservableList;

public interface IModelCompte {

	ObservableList<FXCompte> getComptes();

	FXCompte getCompteVue();
	
	void actualiserListe() throws Exception;

	void preparerAjouter();

	void preparerModifier(FXCompte compte);

	void validerMiseAJour() throws Exception;

	void supprimer(FXCompte compte) throws Exception;

}