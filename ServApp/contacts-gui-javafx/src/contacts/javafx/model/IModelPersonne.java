package contacts.javafx.model;

import contacts.javafx.fxb.FXPersonne;
import contacts.javafx.fxb.FXTelephone;
import javafx.collections.ObservableList;

public interface IModelPersonne {

	ObservableList<FXPersonne> getPersonnes();

	FXPersonne getPersonneVue();

	void actualiserListe() throws Exception;

	void preparerAjouter();

	void preparerModifier(FXPersonne personne);

	void ValiderMiseAJour() throws Exception;

	void supprimer(FXPersonne personne) throws Exception;

	void ajouterTelephone();

	void supprimerTelephone(FXTelephone telephone);

}