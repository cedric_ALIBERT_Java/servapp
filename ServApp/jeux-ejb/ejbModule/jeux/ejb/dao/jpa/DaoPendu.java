package jeux.ejb.dao.jpa;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


import jeux.ejb.dao.IDaoPendu;

import jeux.ejb.dom.DataPendu;

@Stateless
@Local(IDaoPendu.class)
public class DaoPendu implements IDaoPendu {

	@PersistenceContext(unitName="jeux")
	private EntityManager entityManager;
	
	@Override
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public void inserer(DataPendu dataPendu) {
		entityManager.persist(dataPendu);
		entityManager.flush();

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public void modifier(DataPendu dataPendu) {
		entityManager.merge(dataPendu);

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public DataPendu retrouver(String idJoueur) {
		
		return entityManager.find(DataPendu.class, idJoueur);
	}

}
