package jeux.ejb.dao.jpa;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import jeux.ejb.dao.IDaoNombre;
import jeux.ejb.dom.DataNombre;

@Stateless
@Local(IDaoNombre.class)
public class DaoNombre implements IDaoNombre {

	@PersistenceContext(unitName="jeux")
	private EntityManager entityManager;
	
	@Override
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public void inserer(DataNombre dataNombre) {
		entityManager.persist(dataNombre);
		entityManager.flush();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public void modifier(DataNombre dataNombre) {
		entityManager.merge(dataNombre);

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public DataNombre retrouver(String idJoueur) {
		
		return entityManager.find(DataNombre.class, idJoueur);
	}

}
