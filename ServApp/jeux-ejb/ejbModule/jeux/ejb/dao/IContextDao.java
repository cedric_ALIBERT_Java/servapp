package jeux.ejb.dao;


public interface IContextDao {
	
	<T> T 	getDao(Class<T> type);

}
