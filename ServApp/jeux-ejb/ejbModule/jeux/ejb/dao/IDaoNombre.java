package jeux.ejb.dao;

import jeux.ejb.dom.DataNombre;

public interface IDaoNombre {

	void		inserer( DataNombre dataNombre );

	void 		modifier( DataNombre dataNombre );

	DataNombre 	retrouver( String idJoueur );

}
