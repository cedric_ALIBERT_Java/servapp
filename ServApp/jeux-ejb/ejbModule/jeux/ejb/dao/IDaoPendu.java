package jeux.ejb.dao;

import jeux.ejb.dom.DataPendu;

public interface IDaoPendu {

	void		inserer( DataPendu dataPendu );

	void 		modifier( DataPendu dataPendu );

	DataPendu 	retrouver( String idJoueur );

}
